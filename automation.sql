/*
Navicat MySQL Data Transfer

Source Server         : 39.106.53.5
Source Server Version : 50505
Source Host           : 39.106.53.5:3306
Source Database       : aotu_bushu

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-06-17 22:21:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `job`
-- ----------------------------
DROP TABLE IF EXISTS `job`;
CREATE TABLE `job` (
  `job_id` varchar(50) NOT NULL,
  `job_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `environment` varchar(255) DEFAULT NULL COMMENT '环境',
  `label` varchar(255) DEFAULT NULL COMMENT '标签',
  `group_id` int(11) DEFAULT NULL COMMENT '流水线分组id',
  `template_id` int(11) DEFAULT NULL COMMENT '模板id',
  `pipeline` varchar(255) DEFAULT NULL COMMENT 'pipeline',
  `display_name` varchar(255) DEFAULT NULL COMMENT '任务简称',
  `cread_time` timestamp NULL DEFAULT NULL COMMENT '任务创建时间',
  `creader` varchar(255) DEFAULT NULL COMMENT '创建用户',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `iscollect` varchar(255) DEFAULT NULL COMMENT '是否收藏1收藏 0没有收藏',
  `status` varchar(255) DEFAULT NULL COMMENT '最近执行状态',
  `setting_id` varchar(255) DEFAULT NULL COMMENT '最近执行状态',
  `timestamp` timestamp NULL DEFAULT NULL COMMENT '最近执行时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job
-- ----------------------------
INSERT INTO `job` VALUES ('098c67fc-5c14-4a5c-950c-cb354cec2459', '111', 'ios', '', '0', '1', '', '111', '2022-06-17 08:03:55', 'admin', '111_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', 'aotu_bushu06160009', 'linux', '', '0', '3', '', 'aotu_bushu06160009', '2022-06-15 16:10:24', 'admin', 'aotu_bushu06160009_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('2d633f15-8f8c-4d30-ad15-b65176220478', 'auto_bushu_0616', 'linux', '', '0', '3', '', 'auto_bushu_0616', '2022-06-15 16:08:38', 'admin', 'auto_bushu_0616_project ', null, '未构建', '', null);
INSERT INTO `job` VALUES ('3df8fcda-2580-4268-8a95-e664d1d1af2f', '全自动化测试部署', 'linux', '11', '0', '3', '', '全自动化测试部署', '2022-06-15 15:17:54', 'admin', '全自动化测试部署_project ', null, '未构建', '', null);
INSERT INTO `job` VALUES ('3f0de026-8235-46d1-a9d9-642de6bd199e', '全自动化测试', 'linux', '', '0', '3', '', '全自动化测试', '2022-06-15 15:24:40', 'admin', '全自动化测试_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('4e128f10-27df-41b3-9849-7a2a782d2192', '11', 'android', '12', '0', '2', '', '11', '2022-06-09 13:20:11', 'zhangtao', '11_project ', null, '未构建', '69c4a676-0d48-44fb-bf64-e9ae6ec044b3', null);
INSERT INTO `job` VALUES ('4e52f1bb-aafe-4dfc-864b-c660d4e86bf6', 'aotu_bushu06-17-5', 'linux', '', '0', '3', '', 'aotu_bushu06-17-5', '2022-06-17 09:26:27', 'admin', 'aotu_bushu06-17-5_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '测试', 'windows', '1，2', '0', '1', '', '测试', '2022-06-14 10:18:12', 'zhangtao', '测试_project ', null, 'ABORTED', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', '2022-06-14 15:27:27');
INSERT INTO `job` VALUES ('98a7ebf4-5f49-453b-88bd-6292e7b7d851', '测试0615', 'windows', '1，2，3', '0', '1', '', '测试0615', '2022-06-15 11:37:30', 'admin', '测试0615_project ', null, 'FAILURE', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', '2022-06-15 11:38:24');
INSERT INTO `job` VALUES ('9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '测试0614', 'windows', '1，2，3', '0', '1', '', '测试0614', '2022-06-14 15:45:01', 'zhangtao', '测试0614_project ', null, 'ABORTED', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', '2022-06-14 15:49:48');
INSERT INTO `job` VALUES ('9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '三国杀', '', '', '0', '1', '', '三国杀', '2022-06-17 02:35:34', 'admin', '三国杀_project ', '1', '未构建', '', null);
INSERT INTO `job` VALUES ('9f3a1315-9a45-45ce-acc4-989f33caa552', '测试0615-2', 'windows', '1，2', '0', '1', '', '测试0615-2', '2022-06-15 11:42:57', 'admin', '测试0615-2_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '的', '', '', '0', '1', '', '的', '2022-06-17 08:20:37', 'admin', '的_project ', null, '未构建', '', null);
INSERT INTO `job` VALUES ('bbe37332-22de-4864-acb7-f91aa1c5e64a', 'lxl11', 'linux', '1，2', '0', '1', '', 'lxl', '2022-06-09 13:49:09', 'zhangtao', 'lxl_project ', null, '未构建', '69c4a676-0d48-44fb-bf64-e9ae6ec044b3', null);
INSERT INTO `job` VALUES ('dd7cf0c9-0936-4bab-9bf2-6f86014b74a6', 'aotu_bushu06-17', 'linux', '', '0', '3', '', 'aotu_bushu06-17', '2022-06-17 09:25:31', 'admin', 'aotu_bushu06-17_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '1', 'ios', '1', '0', '2', '', '1', '2022-06-09 12:55:40', 'zhangtao', '1_project ', null, '未构建', null, null);
INSERT INTO `job` VALUES ('e3803930-baeb-40e8-8637-4839b3307434', '111111', 'linux', '2,1', '0', '2', '', '111111', '2022-06-09 11:58:47', 'zhangtao', '111111_project ', null, '未构建', null, null);
INSERT INTO `job` VALUES ('f1bd23c6-d6fe-4f72-ae0f-0bdcaba3f4b8', '112', 'ios', '', '0', '1', '', '112', '2022-06-17 09:29:44', 'admin', '112_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);
INSERT INTO `job` VALUES ('fd37a92e-c5cb-4f1d-952a-3188338ffb6f', 'aotu_bushu06160011', 'linux', '', '0', '3', '', 'aotu_bushu06160010', '2022-06-17 09:20:16', 'admin', 'aotu_bushu06160010_project ', null, '未构建', 'ee50396c-e10b-4eaf-829e-b0c2c3775003', null);

-- ----------------------------
-- Table structure for `job_build`
-- ----------------------------
DROP TABLE IF EXISTS `job_build`;
CREATE TABLE `job_build` (
  `build_id` varchar(50) NOT NULL,
  `status` varchar(255) DEFAULT NULL COMMENT '构建状态',
  `job_id` varchar(255) DEFAULT NULL COMMENT '任务id',
  `keeptime` int(11) DEFAULT NULL COMMENT '持续时间',
  `timestamp` timestamp NULL DEFAULT NULL COMMENT '构建时间',
  `remake` varchar(255) DEFAULT NULL COMMENT '构建备注',
  `log` varchar(255) DEFAULT NULL COMMENT '构建日志',
  `number` varchar(255) DEFAULT NULL COMMENT '构建编号',
  `creader` varchar(255) DEFAULT NULL COMMENT '构建用户',
  `log_format` varchar(255) DEFAULT NULL COMMENT '日志类型 xml&text',
  `job_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `triggers` varchar(255) DEFAULT NULL COMMENT '触发信息',
  PRIMARY KEY (`build_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_build
-- ----------------------------
INSERT INTO `job_build` VALUES ('0123c74f-29ef-400e-a8fd-0a9562c98f3e', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:40:45', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('016252d2-646b-4ab0-9d17-128bca08eb27', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('01876cea-f7ca-4ac2-bbe4-3c6703e48797', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('021bce41-e04c-4690-9dc5-f95bbfc6f9d3', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('02f056f4-bf9f-4691-b32a-741b911fd7a6', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:10:27', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('040083ab-20f6-47cd-8978-e1c49880a3fb', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:00:16', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('041a41f9-f954-470c-a33e-d674ea8c57ad', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:09:47', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('04ef5a95-3537-4482-9780-fb54bc3e4bae', 'NOT_BUILT', 'fd37a92e-c5cb-4f1d-952a-3188338ffb6f', '0', '2022-06-17 09:20:16', '[]', null, '0', 'admin', 'text', 'aotu_bushu06160010', 'admin自动触发');
INSERT INTO `job_build` VALUES ('04f5ee9f-0708-4dc4-8d3c-d0361e010ada', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:20:37', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('06efa850-2a9d-428f-a78d-430d5fecd4bf', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('07357aa1-c266-4d32-89e2-a7aa7a71e490', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('075bc9f6-73f2-4e4a-8252-064e7c0109c3', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('07905fb6-9400-4ce2-bbaa-46006623df98', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:13:07', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('07e219a6-73da-4f29-b818-c1c64a14718c', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:08:13', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('08e771e9-1b40-4ddc-a131-1e62c4e10411', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('095049a3-12ef-45a7-9525-6cb065f24672', 'FAILURE', '9f3a1315-9a45-45ce-acc4-989f33caa552', '149', '2022-06-15 11:43:14', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615-2', 'admin自动触发');
INSERT INTO `job_build` VALUES ('099c010e-b08a-4007-b829-68d1a1a27d54', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:21:19', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('0aefc15e-9d34-480a-9cf3-d83c45c81f1b', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:18:49', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('0ba8e5f3-2ac5-4a2f-8fd8-b3c220bca89d', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:24:27', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('0c0e4a91-93a3-4b6c-b328-259f5bc4e5af', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:21:27', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('0ca863c3-2c14-4cca-b9cd-3615fde5ed7b', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:08:23', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('0cfac719-1ef8-4ea7-bc7a-79c2591bb080', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('0d395cf0-581a-4462-9665-cc34df39d973', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:19:24', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('0dee21fe-e1c0-4ce8-8415-d1c0c689f18f', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:42:32', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('105ac261-4709-4aa1-8839-bcd215430f8c', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('105f27b4-389e-433c-a74c-0bda13ccd989', 'FAILURE', '3f0de026-8235-46d1-a9d9-642de6bd199e', '68', '2022-06-15 15:40:34', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '全自动化测试', 'admin自动触发');
INSERT INTO `job_build` VALUES ('10e38390-77e7-4e2e-9ade-6a51f36c2b56', 'NOT_BUILT', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '0', '2022-06-14 15:45:59', '[]', null, '0', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('114039d3-e7e3-4df7-bb26-d4224709ce51', 'FAILURE', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '46', '2022-06-15 15:11:39', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('120ccde0-f8a6-4655-b2e9-975945f86e63', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('16cc2e68-2456-4b01-92c5-1058aa05a246', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:09:00', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('17075658-293b-4cb3-b5dc-69b47123945e', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1aeeaf09-0020-4662-a6f3-3114942aeef6', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1bf4b82d-82c9-49fe-bfe2-30690cbaf82c', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('1cb01ae3-38a8-413f-8402-eb2db1e7d107', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:32:08', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1cd080e9-a4eb-4aa0-823c-bc56a4a134a4', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1dec089c-f987-4fe4-85d3-8b2cd5124695', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:21:54', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1e43d168-8538-425d-ac5c-b2eef18b18d7', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('1e849b63-286a-4edc-8147-1a33ca0a0add', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:17:21', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('1ea6c6c9-a0a2-4f45-b0d0-d86316c46165', 'FAILURE', '9f3a1315-9a45-45ce-acc4-989f33caa552', '149', '2022-06-15 11:43:14', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615-2', 'admin自动触发');
INSERT INTO `job_build` VALUES ('1efc0bec-63f6-4d89-a217-5f69d32edffd', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('204cb285-b17d-422a-b9f8-4817d8074ee8', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-09 13:20:12', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('211eb2f1-2085-45df-bbff-7e68a8b5b228', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('21ab57e2-3de1-445b-8666-28d1f94872a2', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('21b728d9-bd7c-45e4-9334-38f725ccd72c', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:09:38', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('21d84681-942b-41c6-b1f0-4bf03335ed64', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:00:39', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('22793881-c0d1-4b96-80d2-4199b9c7f51f', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('23a1f608-cc31-4dc4-a724-75670e431e99', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:11:50', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('24ab94de-faa4-4474-843b-5295151dd7ab', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:02:17', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('24e4fb6f-8c21-4eb6-999d-f0cd56e4a1e6', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:21:01', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('250317e9-54de-49c9-911d-a6b9d7a509f1', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('254403f1-9ab8-43e0-bcd3-33f8c9da23c5', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('275ae390-d053-4651-bae2-2c2dde8518ee', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:37:00', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('27f0e5a8-e10e-4444-aacb-5df3c33982d3', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('28d62971-e9f8-4d18-ac50-39d109fa5511', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:49:31', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('2b149835-cc79-489b-96dd-8c494f61a458', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:46:02', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('2b8cbd44-82eb-4175-b812-302519663b7d', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('2c5e670f-bdf3-4a7b-a5f8-e57d3ac98849', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:42:32', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('2e420cd4-4022-4da9-9e8f-ec03bd73eb40', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('2e8d1ce8-70d8-479d-9e7c-fe60a42c9921', 'NOT_BUILT', '2d633f15-8f8c-4d30-ad15-b65176220478', '0', '2022-06-15 16:09:08', '[]', null, '0', 'admin', 'text', 'auto_bushu_0616', 'admin自动触发');
INSERT INTO `job_build` VALUES ('2f7ae156-7171-4619-9863-d8b4e65a40e8', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('30031ef8-a287-4168-ae09-fcbae7976106', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('30152ce2-274a-4284-b31f-91761a295e19', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('305d3a31-539d-45d4-b0b5-307407e7d029', 'FAILURE', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '176', '2022-06-15 11:38:24', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', null, 'admin页面手动触发');
INSERT INTO `job_build` VALUES ('308e677f-9aae-4f98-b670-3d6ad5ba717b', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('32924242-b60f-411c-92d6-4ee328947258', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '517', '2022-06-14 10:23:15', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '5', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('3333c0f3-a381-43ac-8ee1-e1b9d662c5bc', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:38:25', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('3405123b-3225-4b13-8e03-991aaa28ba2f', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('343093fd-ed27-459d-9998-7e58e32e2b2a', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('356b0215-9248-4ed6-a527-82ba53c2444a', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('384d059b-f9ea-4189-b77e-a2c6b5487c2b', 'NOT_BUILT', '3df8fcda-2580-4268-8a95-e664d1d1af2f', '0', '2022-06-15 15:17:54', '[]', null, '0', 'admin', 'text', '全自动化测试部署', 'admin自动触发');
INSERT INTO `job_build` VALUES ('39737d94-2de4-4d26-b247-5ebaf68fd053', 'NOT_BUILT', 'fd37a92e-c5cb-4f1d-952a-3188338ffb6f', '0', '2022-06-17 09:25:16', '[]', null, '0', 'admin', 'text', 'aotu_bushu06160011', 'admin自动触发');
INSERT INTO `job_build` VALUES ('39faa80c-5b81-4486-be9e-0c50a02b906a', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('3b0896bd-5a05-4f6f-b61d-9fd837800173', 'FAILURE', '9f3a1315-9a45-45ce-acc4-989f33caa552', '149', '2022-06-15 11:43:14', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615-2', 'admin自动触发');
INSERT INTO `job_build` VALUES ('3c37dd5d-84eb-435a-aec5-fec82d0571e9', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:36:24', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('3c8da8bc-be2f-4b8f-ab8b-b2ba6e491069', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('3da961cc-c4e0-462e-afea-e0b283e3cc70', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:33:36', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('3faeb5fd-9ebd-451f-b169-ff568f4ecc14', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:58:37', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('42871ab4-8425-4a7f-b6e7-6607ce21cd21', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('437dde47-d3cd-4a69-a4d5-66871b8f2be2', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4482e88a-ae55-4960-bc0b-5877e0234b25', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '407', '2022-06-14 11:21:06', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '21', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('4516c408-de4d-4b04-bea7-a39715c5098b', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:35:21', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('45d53be5-3626-4276-a479-a223a523b0f8', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('45d5b46b-7041-4780-8eac-24bbfc55afb5', 'NOT_BUILT', 'bbe37332-22de-4864-acb7-f91aa1c5e64a', '0', '2022-06-09 13:49:10', '[]', null, '0', 'zhangtao', 'text', 'lxl', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('45ea91a1-e7c6-4244-af83-375a17fc066f', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '533', '2022-06-14 10:24:40', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '6', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('464876bd-acaf-476c-b54e-e558d0bf2ab1', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('46e47931-8a2b-49e1-b90c-2376b3afdb61', 'FAILURE', '3f0de026-8235-46d1-a9d9-642de6bd199e', '60064', '2022-06-17 03:13:10', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '全自动化测试', 'admin自动触发');
INSERT INTO `job_build` VALUES ('4ab11bac-fd4d-4889-b308-b3938cfd2dfc', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:32:08', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4b10dc88-e591-465b-b3bf-6b6f714ee660', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4b19295b-ad5f-4fee-b59a-d2f8de65711e', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '5475', '2022-06-14 10:18:40', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('4b4a2b71-36fd-487f-9b8d-4de4d352493d', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4b6bff81-f8be-4152-9a52-6b94203e2a08', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:32:08', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4b781dee-4e7c-4aff-b846-2b5de277c108', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4cb49e17-515b-49b7-8af4-8ca4caa69dfa', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:19:19', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4d098714-e20a-45a8-a87f-4a324278a9d2', 'FAILURE', '3f0de026-8235-46d1-a9d9-642de6bd199e', '60064', '2022-06-17 03:13:10', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '全自动化测试', 'admin自动触发');
INSERT INTO `job_build` VALUES ('4db5f343-cbab-4058-bbe3-2a3fae415606', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4eef5db7-0c94-4c52-9b07-10c6523e5a31', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('4f6e2f82-6646-423d-9450-d2c1d048a43b', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '421', '2022-06-14 10:56:11', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '14', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('50852f10-9279-4150-ab2c-208e979bdb85', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:35:34', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('51b8feea-1b9c-4a7a-ab63-e989cac665ae', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('5356be9b-8a42-40e9-a8af-9d72f32ec50e', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('5579ebd1-66f1-4ff6-89de-ac6bd246b113', 'NOT_BUILT', '098c67fc-5c14-4a5c-950c-cb354cec2459', '0', '2022-06-17 08:10:20', '[]', null, '0', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('56abe6e5-b219-45ce-baa2-ffdeca7341c5', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '311', '2022-06-14 10:59:06', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '17', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('57cd4f32-7ce5-4aa4-bb47-bdfd4b7cb41a', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('57f8720b-c2c8-405f-ab77-bcec7767a2e6', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '378', '2022-06-14 11:12:36', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '19', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('590d28bb-2501-406f-be1d-a59dbe1c6b5a', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 05:37:03', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('59c25abd-12f0-4bc7-94fc-6bad0b742ab7', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('59d30bc4-383f-4773-b816-6bce947229f9', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:04:00', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('5b91df24-90ee-4004-92ba-f35576c1b1b0', 'NOT_BUILT', '9f3a1315-9a45-45ce-acc4-989f33caa552', '0', '2022-06-15 11:42:57', '[]', null, '0', 'admin', 'text', '测试0615-2', 'admin自动触发');
INSERT INTO `job_build` VALUES ('5e2bc4ee-de7e-4c63-9ed2-07bda5a1dce4', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '474', '2022-06-14 10:19:45', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('5ef4f0fd-c20e-469f-bfd4-62fc296f7dd5', 'NOT_BUILT', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '0', '2022-06-15 11:37:31', '[]', null, '0', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('5ef5848d-789d-4759-ae9a-ee2bb72bff91', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:24:27', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('5f893b4f-1146-44f9-b665-d2cbb8d0c8ce', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:38:25', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('5f9f4974-6cec-45c9-998d-7b6bce2c7856', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('60a7c636-3dfe-4af2-a532-f40d88ad9a7e', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('629e1c0a-6315-43ed-aa95-a8a72d07218d', 'NOT_BUILT', '098c67fc-5c14-4a5c-950c-cb354cec2459', '0', '2022-06-17 08:08:46', '[]', null, '0', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('62d0a7e0-2916-4adf-91ba-dacc2fa75371', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '5475', '2022-06-14 10:18:40', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('64cf3b14-04ea-4a6d-bbd8-b2bf6f03fe5c', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:40:37', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('64f7578b-0c4f-4396-8844-a79763d51688', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:38:25', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('65919de1-721d-4913-bee2-bb86e0c0adef', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:02:06', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('65d1b9f3-9e3c-4889-8323-6c07e5509822', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 11:31:45', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('6630ca97-256c-426f-b97a-040868ae2de2', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('669e93f9-7ba5-4413-8623-de84bbb03fd0', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '532', '2022-06-14 10:22:25', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '3', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('676bda79-1e2b-47fa-9f67-e937e0ab2f39', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('6a2cec83-8b14-4d39-84f2-e3c86f2fc455', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 11:41:31', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('6af53c45-ecef-4c0d-a3f3-779996adad13', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '626', '2022-06-14 11:10:11', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '18', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('6c3420d9-0fd2-4743-9666-92e3f3e6caae', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:02:50', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('6df34c12-588e-4dfb-8213-182871b6e286', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('6dfef4ff-1b76-482f-8528-6c5950e218ff', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('6e1ad51d-13e4-4923-aa17-005db08563fe', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:38:45', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('6f34b3cb-deeb-4d65-81a1-18942ed3fbca', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('700452fd-ada6-44b1-a8f4-56b521bb7f30', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:21:38', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('70132f65-b29c-472c-9a22-426c848f1dcb', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:32:08', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('70c0d15e-c520-4ddd-9460-4f6eadf263f3', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 11:31:45', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7168a946-3805-4a9e-aa2c-d4d9365f32d2', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('73ad5d1e-6664-4b1c-b06c-382d5a9e9aad', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7432027d-5389-409c-b88e-61dc93cfe75b', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('74328241-ff31-4215-8a3e-be15c7615291', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('74f2a95d-0caa-451b-9703-5c9d98cf9ad7', 'NOT_BUILT', 'dd7cf0c9-0936-4bab-9bf2-6f86014b74a6', '0', '2022-06-17 09:25:31', '[]', null, '0', 'admin', 'text', 'aotu_bushu06-17', 'admin自动触发');
INSERT INTO `job_build` VALUES ('75a715f4-54fa-42ba-a552-e39e8fd45339', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('764ec687-598d-4c91-a538-208125963f3f', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('770eb4cf-1a18-4b64-a2b5-54c9270227e5', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:03:11', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('77f75631-2d87-4aeb-b605-f4c6721fb0b7', 'NOT_BUILT', '098c67fc-5c14-4a5c-950c-cb354cec2459', '0', '2022-06-17 08:11:14', '[]', null, '0', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('78b27893-64af-487a-9ab8-6e418cd69aa6', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('79363c1d-09f2-4178-a8e1-ff500e3bc9fb', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('794f9716-5579-4efb-8e53-3cc46a1c56b9', 'NOT_BUILT', 'fd37a92e-c5cb-4f1d-952a-3188338ffb6f', '0', '2022-06-17 09:24:59', '[]', null, '0', 'admin', 'text', 'aotu_bushu06160010', 'admin自动触发');
INSERT INTO `job_build` VALUES ('79579283-07dc-4af9-90fd-0ee235871766', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7a1a475c-0b78-4b11-99ea-c8957c39185f', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7af76afa-3a77-48fd-9529-478a41f35124', 'NOT_BUILT', '4e52f1bb-aafe-4dfc-864b-c660d4e86bf6', '0', '2022-06-17 09:26:27', '[]', null, '0', 'admin', 'text', 'aotu_bushu06-17-5', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7b352cf9-bded-4788-9148-f740c66d6adf', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:24:27', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7b9cdfdb-ad13-4ad7-9196-27917c552b77', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7bac3756-47be-4825-95d5-fd69689c2cb9', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7bcc0bc3-002f-4538-94f6-92a90eb7244f', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:10:25', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7c0006ab-32c1-46a3-acf7-1bc450e46c9f', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7d32ba86-3a42-42b7-860d-a9dde82abc95', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('7f51f693-24a8-4c5a-923e-c8d6c2942433', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:52:28', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('7fd6a3c8-1a41-4fa7-90f8-5dd39feb433d', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('80509cda-c50c-48c4-b07c-004b0ff6df15', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:02:17', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('82cab1a7-dc5f-4a25-9b31-a611e6df442a', 'NOT_BUILT', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '0', '2022-06-15 11:37:46', '[]', null, '0', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('8324dffc-4a03-47d1-8283-c2bfa346058f', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('83dd0759-dbc1-405d-8b5b-3aa9a8d6b83c', 'NOT_BUILT', '3f0de026-8235-46d1-a9d9-642de6bd199e', '0', '2022-06-15 15:24:41', '[]', null, '0', 'admin', 'text', '全自动化测试', 'admin自动触发');
INSERT INTO `job_build` VALUES ('84412998-0200-4190-abc7-eeae125c386f', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '345', '2022-06-14 10:58:00', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '15', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('8530b3b8-cc56-4f1d-b526-948403c779f6', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8605431d-4227-44a2-83f7-adf1f402419b', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:14:51', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('86aebdd7-041a-4150-a9b3-53f811a89bdb', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8749193d-2f64-4ec5-8a3d-c6ed9b3ddeb7', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '405', '2022-06-14 10:22:45', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('88cfa67e-ac17-4006-89f5-240d737c09a4', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:35:29', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('89545483-e0b3-44f8-95c5-9d21aa82003e', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '816', '2022-06-14 10:36:00', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '8', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('8959b488-eca6-4a09-9912-daccca1208e9', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8a694552-43d9-44eb-a799-c8a86390de22', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8a6ba298-2e94-471b-b5fb-622a90c98693', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:18:13', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8bd2dd44-c59c-4575-a74a-5d6bfba9938d', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:04:00', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8c3ec61b-5201-46d4-b856-8c0d5dae2d8e', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('8ddf2deb-4bd8-4f0c-842b-5305fcb83d49', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('90a9d10f-fffc-49cd-90aa-15744b70e271', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('9129afd6-b214-4ef0-a888-0226932c97d2', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('929067af-fc66-4068-9f6f-b8ab4d6e1c5b', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9348c593-98de-463c-8ec1-5d3c97ac4a8b', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:24:27', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('961c7149-cba2-4428-a3ba-042996e098fc', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:17:39', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('976b217d-984b-488c-a17e-6effa357266d', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 05:37:58', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9771baf5-6299-4d63-854c-4700eac47e96', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('97e89273-d8b8-4c4f-8b61-14b2512cb04a', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('9890eadd-66eb-496e-9fa0-6e2e09598222', 'ABORTED', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '0', '2022-06-14 15:46:23', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('98d8cf02-d71d-45e4-81a3-7cdd611b8c39', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:10:27', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('9928c545-ec46-463c-bfa9-3c3708aed718', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 11:41:35', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('994c70a4-d9a4-47b0-b740-c46c5c968d4a', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('99d6557b-33df-45f9-979f-3845a7df8667', 'NOT_BUILT', 'f1bd23c6-d6fe-4f72-ae0f-0bdcaba3f4b8', '0', '2022-06-17 09:29:45', '[]', null, '0', 'admin', 'text', '112', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9a397c38-9464-4c48-9281-8617c5d6800e', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:01:31', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9a866e0b-d02f-4e8b-8195-e1983eddabdd', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:20:51', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9bb1cabc-2888-43a8-bc96-b785307accb5', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:58:19', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9bc004ce-77ce-4bc8-8646-e9b635200707', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:56:45', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9cb56ce5-6b0b-491e-a740-bf00c76fda46', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:20:57', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('9f821964-2183-4f83-bd0e-8973ae2b8cce', 'NOT_BUILT', '3df8fcda-2580-4268-8a95-e664d1d1af2f', '0', '2022-06-17 03:19:13', '[]', null, '0', 'admin', 'text', '全自动化测试部署', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a00ce5d9-1f37-4712-8f15-8f2653e4f38f', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a0d45e15-d526-467a-b399-9d6a68598bcf', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '69', '2022-06-15 16:50:49', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '4', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a0e4b150-d753-444c-b863-3cd71e661752', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:18:52', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a1289757-6c37-4868-96c4-d92ebc1e725f', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a1c17b5c-d7b4-436f-bfed-843c3a590402', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a250ac09-7121-4dea-887d-3b9df8bbf368', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '267', '2022-06-14 11:19:01', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '20', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('a2651726-11dd-44bf-9bad-9cfb0cc68eee', 'FAILURE', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '46', '2022-06-15 15:11:39', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a3214501-423f-427d-b61d-97c4327331c7', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:49:25', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a36d9085-5c1b-4c02-8ca1-dd787bb796bb', 'NOT_BUILT', '098c67fc-5c14-4a5c-950c-cb354cec2459', '0', '2022-06-17 08:03:55', '[]', null, '0', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a41667ed-6919-4367-a519-ded14484876a', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a5f6f0bc-71de-430c-8979-2353046eaf7f', 'FAILURE', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '176', '2022-06-15 11:38:24', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', null, 'admin页面手动触发');
INSERT INTO `job_build` VALUES ('a6887519-8dcf-4583-80c1-4a799f9c6c8c', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a69821c3-7c22-4a0b-bcc8-439614378d01', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '773', '2022-06-14 10:54:26', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '13', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('a6ab2da2-570b-4706-9378-41d36f3b6048', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a713fc68-71fb-4a29-b17a-08784d629079', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a734a791-3dba-48f0-be89-2d03c56fc62e', 'FAILURE', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '46', '2022-06-15 15:11:39', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a8081dd3-b78d-4107-bbbb-653c567273f8', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:09:38', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('a80b9790-d1ae-44ec-a065-f07adb4033b7', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('a841e103-cc59-4e4a-a4ce-45cdc31fd8c0', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '244', '2022-06-14 10:58:41', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '16', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('aaad284e-5ce9-490d-92a5-0dc75ae383d0', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('abc2fcf4-d504-4b5c-82d0-914b7d935aef', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:38:46', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('abf3875f-5941-465b-9420-244da15790e5', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ac52613b-0a97-4d91-98fe-c8aca4b8bd42', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '595', '2022-06-14 10:45:25', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '10', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('acccbb71-5ec9-491d-be00-8b836e8964ee', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:50:06', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ad089171-60e3-4c3f-a514-c33e5c300f81', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ae60d1f0-8e00-4db1-9004-e94841a6b0d8', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '561', '2022-06-14 10:48:41', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '12', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('aeb71f87-0253-4b77-86c0-b8a91658d67f', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('aece0695-96ab-4f87-9a4f-1aac3183bae2', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '517', '2022-06-14 10:23:15', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '5', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('af3c07a0-2cf6-4873-b5fa-129695ee2b2c', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('af75e9da-8836-46e1-8163-aadcf851aeff', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 11:40:54', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('b23581ef-4232-41d3-97d8-157afd8112a5', 'NOT_BUILT', 'dd7cf0c9-0936-4bab-9bf2-6f86014b74a6', '0', '2022-06-17 09:26:16', '[]', null, '0', 'admin', 'text', 'aotu_bushu06-17', 'admin自动触发');
INSERT INTO `job_build` VALUES ('b256ce09-ce19-4cda-84de-8c718ceb7446', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:59:03', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('b6c921bc-28d2-486c-9ae0-4e85c413c5f4', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:18:13', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('b9979f59-0abb-4b2d-9a8c-baeeb0919371', 'NOT_BUILT', 'fd37a92e-c5cb-4f1d-952a-3188338ffb6f', '0', '2022-06-17 09:22:57', '[]', null, '0', 'admin', 'text', 'aotu_bushu06160010', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ba780f0e-b9cd-42f5-a8c7-aaeaf01367f3', 'NOT_BUILT', '2d633f15-8f8c-4d30-ad15-b65176220478', '0', '2022-06-15 16:09:08', '[]', null, '0', 'admin', 'text', 'auto_bushu_0616', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ba9d7a76-4a44-438c-96f4-d5de5e8bfd2f', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:35:30', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('baa66b57-7c87-4be0-96e4-3db4e7fdf98f', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:59:56', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('bae22f69-9492-4647-8e93-412c37304715', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('bb76ddd7-d6d8-4098-abb8-651d1c4c139b', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '474', '2022-06-14 10:19:45', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('bc0a49fe-20d2-44d9-9dee-d553af62776f', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:20:19', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('bdd413ed-c9fb-4718-9e03-86d6ff902a9d', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('bddaa98f-1397-4cdd-abc8-fb2711a0eb37', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 11:40:54', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('bf1461a2-9e63-4552-bee4-872e81a64062', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 11:41:35', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('bf895ea5-0166-40a6-9024-d266ff27fc0c', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:19:24', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c12704db-5ddc-4e9d-b872-2d3c9df28fcb', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:17:39', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c17947d8-4197-4075-a330-20e106c8cd60', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c1c73bb7-5e41-4d57-8ce2-cdd414de05fd', 'NOT_BUILT', '98a7ebf4-5f49-453b-88bd-6292e7b7d851', '0', '2022-06-15 11:38:23', '[]', null, '0', 'admin', 'text', '测试0615', 'admin自动触发');
INSERT INTO `job_build` VALUES ('c1d38ea9-7153-4cee-9e80-3285694ecec5', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '306', '2022-06-14 10:38:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '9', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c23536ea-1186-41b9-9fd3-74e71c85b5a4', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c2c6cade-fd5c-4604-8d73-f5bdf4af4d8e', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '816', '2022-06-14 10:36:00', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '8', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c3572683-1ede-4160-90a9-526ad3fc2833', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '595', '2022-06-14 10:45:25', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '10', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c37d2bc4-8204-4704-a125-dd449112f69d', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c38f8d4f-f2d2-48e3-acca-99632c160142', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:49:52', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('c46fb5b0-95fa-4d48-b50d-856123c7d040', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c54cb80f-0768-4cb5-9d7d-3002194f9f92', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c5702d1c-c42c-45c4-a060-f068319470f0', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('c7d01039-1f9a-49b9-9808-40b35c0a0ffc', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:43:03', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ca84bc3b-c6a0-401c-bade-2412e931cf48', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('cb2e7ef1-8e95-47f3-9086-e33930b212b4', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ccf48391-17d8-4068-970c-273b6aa4a057', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('cd026d27-5fe2-4d4c-b3c4-ef2ff67af97f', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('d06a9089-27f6-46e3-abe2-5a5d3723c466', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d07a9b1d-d994-44ed-bae9-05e7f629eedc', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:59:56', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d104bcac-fdea-430e-9125-7c1bc94b4675', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d1594d34-5a9b-405f-9968-4d8b7241f846', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-09 12:55:40', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d2b5a3b2-3ad5-4696-9211-98a782ed396a', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('d2bae891-5761-4fde-9845-8c7ae1633bae', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 09:18:49', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d2e982a8-9a11-4b9c-a9cb-1c0691b99fa9', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d31891e3-cf4e-46d5-b445-29b84cdb3cbb', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d3632e53-a272-4565-a0f3-dcbf5342d1e0', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '456', '2022-06-14 10:29:50', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '7', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('d37631c6-1456-4161-ad7a-f9f7729b4e75', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d5b2fa5c-97e7-44e0-b60b-8aaeaa0135e8', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '306', '2022-06-14 10:38:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '9', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('d5d3ae94-56bb-4b91-be99-203a201de4ba', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:20:19', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d7c74c54-919a-4503-acf7-d2e9262d4545', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:21:55', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d83c7396-88e7-4abe-a56b-7c9e37ce9d63', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '517', '2022-06-14 10:23:15', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '5', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('d880696f-36b9-4b33-8877-7cb6797a9e88', 'NOT_BUILT', '3df8fcda-2580-4268-8a95-e664d1d1af2f', '0', '2022-06-15 15:18:00', '[]', null, '0', 'admin', 'text', '全自动化测试部署', 'admin自动触发');
INSERT INTO `job_build` VALUES ('d8aeba46-7a5f-4b96-9e60-39b747c1f45e', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('d93cac90-ff52-4451-a39e-2fcf21e299a9', 'NOT_BUILT', '4e52f1bb-aafe-4dfc-864b-c660d4e86bf6', '0', '2022-06-17 09:31:47', '[]', null, '0', 'admin', 'text', 'aotu_bushu06-17-5', 'admin自动触发');
INSERT INTO `job_build` VALUES ('d954523a-e077-41a7-b46b-5f7ab0d951af', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('dac1b6c7-b512-4b7c-98e0-56a96a92adae', 'NOT_BUILT', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '0', '2022-06-14 10:18:13', '[]', null, '0', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('dce64902-ddec-43bf-b9f4-48e93270f523', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('dd43813b-aa8f-4cc6-8cb5-07d3d0e17a55', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:33:36', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('de6bebd3-efe6-4500-962e-7316fd1a8835', 'NOT_BUILT', 'e3803930-baeb-40e8-8637-4839b3307434', '0', '2022-06-09 11:58:47', '[]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('de92d1fa-03f4-4ac2-9809-dad55f7e7aa8', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '453', '2022-06-14 10:47:16', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '11', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('df01c717-596c-4e35-9d94-3c11bad1f95c', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('e139d2ac-be05-45e5-90b2-395211ef9c7b', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('e19f639f-756f-456f-8c63-79225975e471', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('e1a03c4c-9791-4292-83d8-e6ff342036f2', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 09:07:29', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('e20f69c5-1611-49b8-bda4-28df5210e9af', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:05:17', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('e3e97f3c-48f9-4f7d-8792-6c00c75b2a3f', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('e804ebd2-1763-4a65-87d8-3633e6a64dec', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '737', '2022-06-14 11:28:47', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '22', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('e94637c1-2bfc-4d04-a1c8-93a4a8756e3f', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 15:35:16', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('e9855b87-b974-45ee-85ad-75f7f9fde49f', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 09:16:44', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ea18f3e6-6cc3-4ea7-859f-fc9b1e8acd9f', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ea5b2acb-06f0-4098-80d4-3f94e4bf8603', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:06:01', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ea6881f0-77ff-4aeb-9cc5-31af7590fd94', 'ABORTED', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '0', '2022-06-14 15:49:48', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '3', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('eb1533ca-fc1b-4280-815a-e106b74b8eac', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ebe67e1e-3b0e-4818-b6a1-79f176912443', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 03:02:32', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('ec86af6f-3c58-4aed-87a7-4ea47ab707a2', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ed951f9c-8871-48a0-b17c-5721e418211b', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ee0b9e80-4852-49d5-bb90-85a89fdc000f', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('eeada548-d133-438f-8d47-f1eded259b98', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 09:09:47', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ef2cbe5a-0706-4501-bc0e-38a4993b7530', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ef3a96bd-b138-4423-b30e-54154764a085', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 10:40:45', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ef91cc35-d7f9-4d4b-80c7-61d5b9351901', 'NOT_BUILT', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '0', '2022-06-15 16:10:24', '[]', null, '0', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('f01b0a34-7cfa-470d-836a-22e06a506383', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:17:21', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f1403cb3-5b38-451a-a14f-cb8fdd52b9cd', 'NOT_BUILT', 'a6d0f5af-77b4-4080-a029-cc434a6ad5c6', '0', '2022-06-17 08:36:45', '[]', null, '0', 'admin', 'text', '的', 'admin自动触发');
INSERT INTO `job_build` VALUES ('f16d25b4-245e-425b-9c2f-b472aaf88194', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f1e794e6-a80b-47a7-a78a-b6b1d5f8e1e9', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:10:06', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('f22e6127-3684-4448-aee2-31883d169c2b', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f34c6704-08fb-469e-aad1-2e9cf0b74259', 'ABORTED', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '0', '2022-06-14 15:27:27', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', null, 'zhangtao页面手动触发');
INSERT INTO `job_build` VALUES ('f3e1b140-ff57-4de0-b2de-e5c98634b0e4', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f5c82660-3153-4498-8830-fd8e580bbbf0', 'NOT_BUILT', 'e3084ca8-85ee-4d04-8a94-fb2af5a52bd1', '0', '2022-06-14 10:19:19', '[]', null, '0', 'zhangtao', 'text', '1', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f732908b-a35d-4102-b705-e9f9f09b09ae', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 15:38:25', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('f99a43f4-9da1-40ff-acfd-3e9e50cbb7c8', 'FAILURE', '0a3cad9a-8d6c-4b54-9838-d64a33fa4c34', '60061', '2022-06-17 03:14:20', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '1', 'admin', 'text', 'aotu_bushu06160009', 'admin自动触发');
INSERT INTO `job_build` VALUES ('fa05701e-4bbd-42bf-8ca6-b53933d51175', 'NOT_BUILT', '4e128f10-27df-41b3-9849-7a2a782d2192', '0', '2022-06-14 11:41:31', '[]', null, '0', 'zhangtao', 'text', '11', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('faf6aeb1-4b4c-4180-9a2f-e54941b8ebab', 'FAILURE', '098c67fc-5c14-4a5c-950c-cb354cec2459', '60040', '2022-06-17 08:16:31', '[com.offbytwo.jenkins.model.BuildCause@3579a3bc]', null, '3', 'admin', 'text', '111', 'admin自动触发');
INSERT INTO `job_build` VALUES ('fb332b52-d4f3-4805-a12f-a5e94bf307e4', 'SUCCESS', 'e3803930-baeb-40e8-8637-4839b3307434', '235', '2022-06-09 12:15:55', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '111111', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('fc1b79b3-b599-49e2-a66b-916d56b4f379', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 08:08:31', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('fc5d3850-b209-4225-bcb0-07f6cc15f64e', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '52847', '2022-06-14 15:29:02', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '2', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('fe4d6022-e2fb-4be6-8d9c-dfd2aa38baf3', 'NOT_BUILT', '9c10e023-c9c6-46d3-ad92-4c7d57c4efb3', '0', '2022-06-17 02:53:52', '[]', null, '0', 'admin', 'text', '三国杀', 'admin自动触发');
INSERT INTO `job_build` VALUES ('fe568d9b-217d-41f1-94ab-892ce5e86f54', 'FAILURE', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '144828', '2022-06-14 15:39:32', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '1', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('fee56883-5390-4a21-8bd3-91b06257b5ab', 'SUCCESS', '6e1fa0ce-7eac-4e2a-883f-6cb1d0f2dfe6', '518', '2022-06-14 11:29:17', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '23', 'zhangtao', 'text', '测试', 'zhangtao自动触发');
INSERT INTO `job_build` VALUES ('ffa3d02f-3ba6-4e5d-9e56-cbea7790214f', 'FAILURE', '9b043a91-6b2f-46e5-8eeb-7521a9b82d37', '44240', '2022-06-14 15:50:53', '[com.offbytwo.jenkins.model.BuildCause@b52ad95]', null, '4', 'zhangtao', 'text', '测试0614', 'zhangtao自动触发');

-- ----------------------------
-- Table structure for `job_collect`
-- ----------------------------
DROP TABLE IF EXISTS `job_collect`;
CREATE TABLE `job_collect` (
  `id` int(11) NOT NULL,
  `collect_id` varchar(255) DEFAULT NULL COMMENT '收藏id',
  `jobI_id` varchar(255) DEFAULT NULL COMMENT '任务id',
  `user` varchar(255) DEFAULT NULL COMMENT '所属用户',
  `participate_id` varchar(255) DEFAULT NULL COMMENT '参与id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_collect
-- ----------------------------

-- ----------------------------
-- Table structure for `job_flow_groups`
-- ----------------------------
DROP TABLE IF EXISTS `job_flow_groups`;
CREATE TABLE `job_flow_groups` (
  `group_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_flow_groups
-- ----------------------------
INSERT INTO `job_flow_groups` VALUES ('0', '第一组');
INSERT INTO `job_flow_groups` VALUES ('2', '第二组');
INSERT INTO `job_flow_groups` VALUES ('3', '第三组');

-- ----------------------------
-- Table structure for `job_setting`
-- ----------------------------
-- ----------------------------
-- Table structure for `job_setting`
-- ----------------------------
DROP TABLE IF EXISTS `job_setting`;
CREATE TABLE `job_setting` (
  `id` varchar(50) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL COMMENT '项目名称',
  `type` varchar(255) DEFAULT NULL COMMENT '部署方式 jar war',
  `svn_url` varchar(255) DEFAULT NULL COMMENT 'svn地址',
  `svn_username` varchar(255) DEFAULT NULL COMMENT 'svn账号',
  `svn_password` varchar(255) DEFAULT NULL COMMENT 'svn密码',
  `svn_credentials_id` varchar(255) DEFAULT NULL COMMENT 'svn凭证id',
  `project_port` int(11) DEFAULT NULL COMMENT '项目访问端口',
  `docker_port` int(11) DEFAULT NULL COMMENT 'docker端口',
  `project_version` varchar(255) DEFAULT NULL COMMENT '项目版本号',
  `project_url_jar` varchar(255) DEFAULT NULL COMMENT '项目jar包位置',
  `project_url_war` varchar(255) DEFAULT NULL COMMENT '项目war包位置',
  `docker_id` varchar(255) DEFAULT NULL COMMENT 'docker容器id',
  `project_describe` varchar(255) DEFAULT NULL COMMENT '项目说明',
  `user_id` varchar(255) DEFAULT NULL COMMENT '用户id',
  `svn_localhost` varchar(255) DEFAULT NULL COMMENT 'jenkinsFile 的svn地址',
  `svn_username_project` varchar(255) DEFAULT NULL COMMENT 'jenkinsFile 的用户名',
  `svn_password_project` varchar(255) DEFAULT NULL COMMENT 'jenkinsFile 的密码',
  `file_path` varchar(255) DEFAULT NULL COMMENT '本地零时存储目录  jenkinsfile',
  `ssh_url` varchar(255) DEFAULT NULL COMMENT 'ssh地址',
  `ssh_port` int(11) DEFAULT NULL COMMENT 'ssh端口',
  `ssh_pwd` varchar(255) DEFAULT NULL COMMENT 'ssh名称',
  `ssh_name` varchar(255) DEFAULT NULL COMMENT 'ssh密码',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_setting
-- ----------------------------
INSERT INTO `job_setting` VALUES ('1', '测试项目', 'jar', '123', '123', '123', '12312', '312', '3213', '123', '2131', '31', '313', '123', '1', '1', null, null, '222', '22', '22', '33', '33', null);
INSERT INTO `job_setting` VALUES ('1adb0655-7689-4c07-8cb5-28274ad7c0d9', '测试项目1', 'jar', '1111111', '11111111111', '11111111111', '11111111111', '2147483647', '2147483647', '111111111', '111111111111', '111111111', '1111111111', '111111111', '1', '1', null, null, '222', '22', '22', '333', '33', null);
INSERT INTO `job_setting` VALUES ('52cc023d-0376-41cd-ad8b-f14c9303f537', '测试项目2', 'war', '1111111', '11111111111', '11111111111', '11111111111', '2147483647', '2147483647', '111111111', '111111111111', '111111111', '1111111111', '111111111', '1', '1', null, null, '22', '22', '22', '3333', '33', null);
INSERT INTO `job_setting` VALUES ('69c4a676-0d48-44fb-bf64-e9ae6ec044b3', '新项目', 'war', 'https://39.106.53.5:4431/svn/jenkinsfile/java', 'bjb', 'bjb123', '3', '8088', '9001', '1.0', 'C', 'C', '1', '测试', null, 'https://39.106.53.5:4431/svn/jenkinsfile/java', 'bjb', 'bjb123', 'C', 'https://39.106.53.5:4431/svn/jenkinsfile/java', '1', 'bib1', 'bib', null);

-- ----------------------------
-- Table structure for `job_template`
-- ----------------------------
DROP TABLE IF EXISTS `job_template`;
CREATE TABLE `job_template` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `description` varchar(255) DEFAULT NULL COMMENT '模板描述',
  `template_id` int(11) DEFAULT NULL COMMENT '模板分类id',
  `template_name` varchar(255) DEFAULT NULL COMMENT '模板分类名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序字段',
  `cread_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `creader` varchar(255) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_template
-- ----------------------------
INSERT INTO `job_template` VALUES ('1', 'java-自动部署', 'checksvn代码，docker部署', '1', 'Java-jar', '1', '2022-06-02 01:08:49', 'zhangtao');
INSERT INTO `job_template` VALUES ('2', 'java-编译-自动部署', 'checksvn代码，mvn编译，docker部署', '1', 'Java-jar', '2', '2022-06-03 01:08:54', 'zhangtao');
INSERT INTO `job_template` VALUES ('3', 'java-测试-编译-自动部署-测试', 'checksvn代码，sonar自动测试，mvn编译，docker部署，jmeter自动测试', '1', 'Java-jar', '3', '2022-06-04 01:08:58', 'zhangtao');
INSERT INTO `job_template` VALUES ('4', 'java-自动部署', 'checksvn代码，docker部署', '2', 'Java-war', '1', '2022-06-06 01:09:01', 'zhangtao');
INSERT INTO `job_template` VALUES ('5', 'java-编译-自动部署', 'checksvn代码，mvn编译，docker部署', '2', 'Java-war', '2', '2022-06-07 01:09:05', 'zhangtao');
INSERT INTO `job_template` VALUES ('6', 'java-测试-编译-自动部署-测试', 'checksvn代码，sonar自动测试，mvn编译，docker部署，jmeter自动测试', '2', 'Java-war', '3', '2022-06-08 01:09:08', 'zhangtao');

-- ----------------------------
-- Table structure for `job_template_type`
-- ----------------------------
DROP TABLE IF EXISTS `job_template_type`;
CREATE TABLE `job_template_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '模板名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序字段',
  `status` int(11) DEFAULT NULL COMMENT '启用状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of job_template_type
-- ----------------------------
INSERT INTO `job_template_type` VALUES ('1', 'Java-jar', '0', '1');
INSERT INTO `job_template_type` VALUES ('2', 'Java-war', '1', '1');
INSERT INTO `job_template_type` VALUES ('3', 'PHP', '2', '0');
INSERT INTO `job_template_type` VALUES ('4', 'Node.js', '3', '1');
INSERT INTO `job_template_type` VALUES ('5', 'Go', '4', '0');
INSERT INTO `job_template_type` VALUES ('6', 'Python', '5', '0');
INSERT INTO `job_template_type` VALUES ('7', '.NET Core', '6', '0');
INSERT INTO `job_template_type` VALUES ('8', 'C++', '7', '0');
INSERT INTO `job_template_type` VALUES ('9', '移动端', '8', '0');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '2022-06-10 21:26:45');
INSERT INTO `user` VALUES ('2', '2022-06-11 21:26:54');
