const path = require("path");
const fs = require("fs");
function resolve(dir) {
  return path.join(__dirname, dir);
}
// const UAT_SERVER = "https://yoursite.com:4430";
const UAT_SERVER = "http://192.168.1.7:8082";
// const SERVER = 'https://yoursite.com';
// const UAT_SERVER = 'http://192.168.1.8:8067';
// 代理配置请编辑项目目录下的proxy.js(此文件会自动创建),范例：
/*
[{
    startsWith: [
        '/api'
    ],
    target: 'http://172.17.53.127:3555'
}]
*/
module.exports = {
  publicPath: "./",
  productionSourceMap: false,
  devServer: {
    proxy: {
      "/": {
        target: UAT_SERVER, // 接口域名
        changeOrigin: true, // 是否跨域
        router: function(req) {
          let host = "";
          let proxyTable = "[]";
          try {
            proxyTable = fs.readFileSync(path.resolve(`./proxy.js`), {
              encoding: "utf8"
            });
          } catch (e) {
            fs.writeFileSync(path.resolve("./proxy.js"), proxyTable);
          }

          proxyTable = proxyTable.replace(/;/g, "");
          proxyTable = eval("(" + proxyTable + ")");
          proxyTable.forEach(config => {
            config.startsWith.forEach(str => {
              if (req.path.startsWith(str)) {
                host = config.target;
              }
            });
          });
          console.log(`${req.path} => ${host || UAT_SERVER}`);
          return host || UAT_SERVER;
        },
        pathRewrite(rPath) {
          return rPath.replace(/^\/smartbi\//, '/');
        }
      }
    }
  },
  chainWebpack: config => {
    config.resolve.alias.set("@", resolve("src"));
  },
  configureWebpack: config => {
    config.externals = {
    };
    if (process.env.NODE_ENV === "production") {
      // 打包时文件的console.log和debugger都去掉
      // config.optimization.minimizer[0].options.exclude = /app*\.js/
      // config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
      config.optimization.minimizer[0].options.terserOptions.compress.drop_debugger = true;
      config.optimization.minimizer[0].options.terserOptions.compress.pure_funcs = [
        "console.log"
      ];
    }
  }
};
