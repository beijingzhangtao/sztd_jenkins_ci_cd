export function deepCopy(obj) {
  if(typeof obj != 'object') return obj;
  let result = null;
  if(Array.isArray(obj)) {
    result = obj.map(o => deepCopy(o));
  }else {
    result = Object.fromEntries(Object.entries(obj).map(([key, value]) => [key, deepCopy(value)]));
  }
  return result;
}
