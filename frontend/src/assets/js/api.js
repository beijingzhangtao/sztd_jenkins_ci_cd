import { fetch } from './http';

// 创建流水线
export const ceateJob = params => fetch('/web/job/ceateJob', { ...params });
// 修改流水线
export const updateJob = params => fetch('/web/job/updateJob', { ...params });
// 流水线列表
export const getJobList = params => fetch('/web/job/getJobList', { ...params });
// 收藏
export const collectJob = params => fetch('/web/job/collectJob', { ...params });
// 运行
export const buildJob = params => fetch('/web/job/buildJob', { ...params });
// 删除流水线
export const deleteJob = params => fetch('/web/job/deleteJob', { ...params });
// 最近运行任务
export const getLastJob = params => fetch('/web/job/getLastJob', { ...params });
// 流水线信息
export const getJob = params => fetch('/web/job/getJob', { ...params });
// 运行历史
export const getHistoryJobList = params => fetch('/web/job/getHistoryJobList', { ...params });
// 获取模板列表
export const gettemplateList = () => fetch('/web/job/gettemplateList');
// 获取所有分组信息
export const getGroupList = () => fetch('/web/job/getGroupList');
// 获取构建中日志
export const getCurrentLog = params => fetch('/web/job/getCurrentLog', { ...params });
// 获取构建完成日志
export const getJobBuildLog = params => fetch('/web/job/getJobBuildLog', { ...params });


//判断 jenkins 账号和密码是否正确
export const jenkinsCheckImpl = params => fetch('/jenkins/jobsetting/jenkinsCheckImpl', { ...params });
//判断 svn 账号和密码是否正确
export const svnCheckImpl = params => fetch('/svn/svnCheckImpl', { ...params });
//判断 ssh 账号和密码是否正确
export const svnCheck = params => fetch('/svn/svnCheck', { ...params });
//获取所有配置列表(分页)
export const jobGetList = params => fetch('/jenkins/jobsetting/getList', { ...params });
//获取所有配置列表(全部)
export const jobGetListAll = params => fetch('/jenkins/jobsetting/getListAll', { ...params });
//获取单个任务配置
export const jobGetOne = params => fetch('/jenkins/jobsetting/getOne', { ...params });
//删除单个任务配置
export const jobDeteleOne = params => fetch('/jenkins/jobsetting/deteleSetting', { ...params });
//创建单个任务
export const jobAddOne = params => fetch('/jenkins/jobsetting/addSetting', { ...params });
//修改单个任务配置
export const jobUpdateOne = params => fetch('/jenkins/jobsetting/updateSetting', { ...params });
//创建SVN凭证ID
export const createCredentials = params => fetch('/authentication/createCredentials', { ...params });
//验证端口是否被占用
export const isPortUsing = params => fetch('/jenkins/isPortUsing', { ...params });
