import axios from 'axios';
import { Loading, Message } from 'element-ui';

let loading =  {
    loadingInstance1: '',
    loadingStart() {
        this.loadingInstance = Loading.service({ 
            fullscreen: true,
            lock: true,
            text: '数据加载中...',
            background: 'rgba(0, 0, 0, 0.6)'
        });
    },
    loadingEnd() {
        this.loadingInstance.close();
    }
}
// let _IV = "I0MDg3ODUtYTQyZC"
// let _aesKey = "Y1NWI0NjctM2ZkZC"
export function fetch(url, params = {}, method = 'POST', formData = false, { withMask = true, ...config } = {}) {
    if (!url) {
        throw new Error('interface path not found');
    }
    withMask && loading.loadingStart();
    let _headers = {
        'Token': localStorage.getItem('token'), 
        'Content-Type': formData ? 'multipart/form-data' : 'application/json'
    }
    
    let isPOST = method === 'POST'
    return new Promise((resolve, reject) => {
        axios({
            url: url,
            method,
            params: isPOST ? '' : params,
            data: isPOST ? params : '',
            headers: _headers,
            ...config
        }).then(rep => {
            withMask && loading.loadingEnd();
            if(rep.code !== '1000') {
              Message.error(rep.message)
              return Promise.reject(new Error(rep.message))
            }
            resolve(rep);
        }, err => {
            withMask && loading.loadingEnd();
            Message.warning('请求异常，请稍候再试')
            reject({requestUrl: url, resErr: err});
        }).finally(()=>{
            withMask && loading.loadingEnd();
        });
    });
}

export function fetchAll(...promises) {
    loading.loadingStart();
    return Promise.all(promises).then((rep) => {
        loading.loadingEnd();
        return rep;
    }, err => {
        loading.loadingEnd();
        return err;
    });
}
