import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Axios from 'axios';
import ElementUI from 'element-ui';
import { messages } from '@/assets/js/i18n';
import VueI18n from 'vue-i18n';
import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/css/main.css';
import '@/assets/iconfont/iconfont.css'
import 'babel-polyfill';
Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
  size: 'mini'
});
window.onunhandledrejection = function(e) {
  let _reason = e.reason;
  console.log(e);
  console.error('UNHANDLED PROMISE REJECTION: ', _reason);
  e.preventDefault();
};
const i18n = new VueI18n({
  locale: 'zh',
  messages
});
Vue.prototype.$axios = Axios;
// Axios.defaults.baseURL = 'https://172.17.53.12:81'
Axios.defaults.timeout = 45000;
Axios.interceptors.request.use(
  function(config) {
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);
// 添加一个响应拦截器
Axios.interceptors.response.use(
  function(res) {
    return res.data;
  },
  function(error) {
    return Promise.reject(error);
  }
);

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app');
