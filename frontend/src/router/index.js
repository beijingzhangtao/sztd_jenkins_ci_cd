import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);
export default new Router({
    routes: [
      {
        path: '/',
        component: () => import('@/components/pages/Index'),
        children: [
          {
            path: '',
            component: () => import('@/components/common/MainContent'),
            children: [
              {
                path: '', 
                name: 'MyFlow',
                component: () => import('@/components/pages/MyFlow')
              },
              {
                path: '/AllFlow',
                name: 'AllFlow',
                component: () => import('@/components/pages/AllFlow')
              },
              {
                path: '/TestResult',
                name: 'TestResult',
                component: () => import('@/components/pages/TestResult')
              },
              {
                path: '/configure',
                name: 'configure',
                component: () => import('@/components/pages/configure')
              },
              {
                path: '404',
                component: () => import('@/components/pages/404')
              },
            ]
          },
          {
            path: 'BuildHistory/:id',
            name: 'BuildHistory',
            component: () => import('@/components/pages/BuildHistory'),
            children: []
          },
          {
              path: '*',
              redirect: '/404'
          }
        ]
      }
    ]
});
