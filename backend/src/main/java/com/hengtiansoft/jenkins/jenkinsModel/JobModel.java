package com.hengtiansoft.jenkins.jenkinsModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.offbytwo.jenkins.model.BuildResult;

import java.util.List;
import java.util.Map;

/**
 * @author zdd_x
 */
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class JobModel {

    private String jobName;

    private String displayName;

    private BuildResult result;

    private List actions;

    private String xml;

    private Map<String,String> param;

    private Integer buildNumber;

    private Integer firstRange;

    private Integer lastRange;

    private String state;

    private String logFormat;

    private long timestamp;

    private long creadTime;

    private String creader;

    private int page;

    private int row;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public BuildResult getResult() {
        return result;
    }

    public void setResult(BuildResult result) {
        this.result = result;
    }

    public List getActions() {
        return actions;
    }

    public void setActions(List actions) {
        this.actions = actions;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getCreadTime() {
        return creadTime;
    }

    public void setCreadTime(long creadTime) {
        this.creadTime = creadTime;
    }

    public String getCreader() {
        return creader;
    }

    public void setCreader(String creader) {
        this.creader = creader;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public Map<String, String> getParam() {
        return param;
    }

    public void setParam(Map<String, String> param) {
        this.param = param;
    }

    public Integer getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(Integer buildNumber) {
        this.buildNumber = buildNumber;
    }

    public Integer getFirstRange() {
        return firstRange;
    }

    public void setFirstRange(Integer firstRange) {
        this.firstRange = firstRange;
    }

    public Integer getLastRange() {
        return lastRange;
    }

    public void setLastRange(Integer lastRange) {
        this.lastRange = lastRange;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLogFormat() {
        return logFormat;
    }

    public void setLogFormat(String logFormat) {
        this.logFormat = logFormat;
    }
}
