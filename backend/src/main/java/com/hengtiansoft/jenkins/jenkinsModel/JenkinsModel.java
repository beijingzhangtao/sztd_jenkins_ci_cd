package com.hengtiansoft.jenkins.jenkinsModel;

/**
 * @author zdd_x
 */
public class JenkinsModel {

    private String labelName;

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }
}
