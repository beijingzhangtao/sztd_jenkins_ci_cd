package com.hengtiansoft.jenkins.jenkinsModel;

/**
 * @author zdd_x
 */
public class JobViewModel {

    private String viewName;

    private String xml;

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
