package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Timestamp;

/**
 * 任务模板
 * @author zdd_x
 */
@TableName("job_template")
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class JobTemplateModel {

    /**
     * 模板id
     */
    private int id;

    /**
     * 模板名称
     */
    private String name;

    /**
     * 模板描述
     */
    private String description;

    /**
     * 模板分类id
     */
    private int templateId;

    /**
     * 模板分类名称
     */
    private String templateName;

    /**
     * 模板分类名称
     */
    private int sort;

    /**
     * 创建时间
     */
    private Timestamp creadTime;

    /**
     * 创建人
     */
    private String creader;

    public Timestamp getCreadTime() {
        return creadTime;
    }

    public void setCreadTime(Timestamp creadTime) {
        this.creadTime = creadTime;
    }

    public String getCreader() {
        return creader;
    }

    public void setCreader(String creader) {
        this.creader = creader;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}
