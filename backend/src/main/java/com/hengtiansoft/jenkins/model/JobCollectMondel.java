package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 任务收藏参与表
 * @author zdd_x
 */
@TableName("job_collect")
public class JobCollectMondel {

    /**
     * id
     */
    private int id;

    /**
     * 收藏id
     */
    private String collectId;

    /**
     * 任务id
     */
    private String jobId;

    /**
     * 所属用户
     */
    private String user;

    /**
     * 参与id
     */
    private String participateId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCollectId() {
        return collectId;
    }

    public void setCollectId(String collectId) {
        this.collectId = collectId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getParticipateId() {
        return participateId;
    }

    public void setParticipateId(String participateId) {
        this.participateId = participateId;
    }
}
