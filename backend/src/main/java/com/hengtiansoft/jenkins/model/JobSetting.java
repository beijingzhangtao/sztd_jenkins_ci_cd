package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.sql.Timestamp;

/**
 * 任务类
 * @author zdd_x
 */
@TableName("job_setting")
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class JobSetting {

    /**
     * id
     */
    private String id;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * svn地址
     */
    private String svnUrl;

    /**
     * 部署方式
     */
    private String type;

    /**
     * svn账号
     */
    private String svnUsername;

    /**
     * svn密码
     */
    private String svnPassword;

    /**
     * svn凭证id
     */
    private String svnCredentialsId;

    /**
     * 项目访问端口
     */
    private int projectPort;

    /**
     * docker端口
     */
    private int dockerPort;

    /**
     * 项目版本号
     */
    private String projectVersion;

    /**
     * 项目jar包位置
     */
    private String projectUrlJar;

    /**
     * 项目war包位置
     */
    private String projectUrlWar;

    /**
     * docker容器id
     */
    private String dockerId;

    /**
     * 项目说明
     */
    private String projectDescribe;

    /**
     * 用户id
     */
    private String userId;

    /**
     * jenkinsFile 的svn地址
     */
    private String svnLocalhost;

    /**
     * jenkinsFile 的svn地址
     */
    private String svnUsernameProject;

    /**
     * jenkinsFile 的svn地址
     */
    private String svnPasswordProject;

    /**
     * 本地零时存储目录  jenkinsfile
     */
    private String filePath;

    /**
     * ssh地址
     */
    private String sshUrl;

    /**
     * ssh端口
     */
    private int sshPort;

    /**
     * ssh名称
     */
    private String sshName;

    /**
     * ssh密码
     */
    private String sshPwd;

    /**
     *  jar 发布方式
     */
    private int exeTypeJar;

    /**
     * war 发布方式
     */
    private int exeTypeWar;

    /**
     * jenkins地址
     */
    private String jenkinsUrl;

    /**
     * jenkins用户名
     */
    private String jenkinsUsername;

    /**
     * jenkins Token
     */
    private String jenkinsToken;

    /**
     * jenkins Token
     */
    private Timestamp createTime;

    @TableField(exist = false)
    private int page;

    @TableField(exist = false)
    private int row;

    public String getJenkinsCallBack() {
        return jenkinsCallBack;
    }

    public void setJenkinsCallBack(String jenkinsCallBack) {
        this.jenkinsCallBack = jenkinsCallBack;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    /**
     *
     * @return
     */
    @TableField(exist = false)
    private String jenkinsCallBack;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSvnUrl() {
        return svnUrl;
    }

    public void setSvnUrl(String svnUrl) {
        this.svnUrl = svnUrl;
    }

    public String getSvnUsername() {
        return svnUsername;
    }

    public void setSvnUsername(String svnUsername) {
        this.svnUsername = svnUsername;
    }

    public String getSvnPassword() {
        return svnPassword;
    }

    public void setSvnPassword(String svnPassword) {
        this.svnPassword = svnPassword;
    }

    public String getSvnCredentialsId() {
        return svnCredentialsId;
    }

    public void setSvnCredentialsId(String svnCredentialsId) {
        this.svnCredentialsId = svnCredentialsId;
    }

    public int getProjectPort() {
        return projectPort;
    }

    public void setProjectPort(int projectPort) {
        this.projectPort = projectPort;
    }

    public int getDockerPort() {
        return dockerPort;
    }

    public void setDockerPort(int dockerPort) {
        this.dockerPort = dockerPort;
    }

    public void setSshPort(int sshPort) {
        this.sshPort = sshPort;
    }

    public String getProjectVersion() {
        return projectVersion;
    }

    public void setProjectVersion(String projectVersion) {
        this.projectVersion = projectVersion;
    }

    public String getProjectUrlJar() {
        return projectUrlJar;
    }

    public void setProjectUrlJar(String projectUrlJar) {
        this.projectUrlJar = projectUrlJar;
    }

    public String getProjectUrlWar() {
        return projectUrlWar;
    }

    public void setProjectUrlWar(String projectUrlWar) {
        this.projectUrlWar = projectUrlWar;
    }

    public String getDockerId() {
        return dockerId;
    }

    public void setDockerId(String dockerId) {
        this.dockerId = dockerId;
    }

    public String getProjectDescribe() {
        return projectDescribe;
    }

    public void setProjectDescribe(String projectDescribe) {
        this.projectDescribe = projectDescribe;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSvnLocalhost() {
        return svnLocalhost;
    }

    public void setSvnLocalhost(String svnLocalhost) {
        this.svnLocalhost = svnLocalhost;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getSshUrl() {
        return sshUrl;
    }

    public void setSshUrl(String sshUrl) {
        this.sshUrl = sshUrl;
    }

    public int getSshPort() {
        return sshPort;
    }

    public String getSshName() {
        return sshName;
    }

    public void setSshName(String sshName) {
        this.sshName = sshName;
    }

    public String getSshPwd() {
        return sshPwd;
    }

    public void setSshPwd(String sshPwd) {
        this.sshPwd = sshPwd;
    }

    public String getSvnUsernameProject() {
        return svnUsernameProject;
    }

    public void setSvnUsernameProject(String svnUsernameProject) {
        this.svnUsernameProject = svnUsernameProject;
    }

    public String getSvnPasswordProject() {
        return svnPasswordProject;
    }

    public void setSvnPasswordProject(String svnPasswordProject) {
        this.svnPasswordProject = svnPasswordProject;
    }

    public int getExeTypeJar() {
        return exeTypeJar;
    }

    public void setExeTypeJar(int exeTypeJar) {
        this.exeTypeJar = exeTypeJar;
    }

    public int getExeTypeWar() {
        return exeTypeWar;
    }

    public void setExeTypeWar(int exeTypeWar) {
        this.exeTypeWar = exeTypeWar;
    }

    public String getJenkinsUrl() {
        return jenkinsUrl;
    }

    public void setJenkinsUrl(String jenkinsUrl) {
        this.jenkinsUrl = jenkinsUrl;
    }

    public String getJenkinsUsername() {
        return jenkinsUsername;
    }

    public void setJenkinsUsername(String jenkinsUsername) {
        this.jenkinsUsername = jenkinsUsername;
    }

    public String getJenkinsToken() {
        return jenkinsToken;
    }

    public void setJenkinsToken(String jenkinsToken) {
        this.jenkinsToken = jenkinsToken;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
