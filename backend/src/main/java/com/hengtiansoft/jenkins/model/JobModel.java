package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.hengtiansoft.jenkins.model.jobEnum.TriggerType;

import java.sql.Timestamp;


/**
 * 任务类
 * @author zdd_x
 */
@TableName("job")
@JsonInclude(value= JsonInclude.Include.NON_NULL)
public class JobModel {

    /**
     * 任务id
     */
    private String jobId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 环境
     */
    private String environment;

    /**
     * 标签
     */
    private String label;

    /**
     * 流水线分组id
     */
    private int groupId;

    /**
     * pipeline
     */
    private String pipeline;

    /**
     * 任务简称
     */
    private String displayName;

    /**
     * 任务创建时间
     */
    private Timestamp creadTime;

    /**
     * 创建用户
     */
    private String creader;

    /**
     * 任务描述
     */
    private String description;

    /**
     * 是否收藏1收藏 0没有收藏
     */
    private String iscollect;

    /**
     * 最近执行状态
     */
    private String status;

    /**
     * 最近执行状态
     */
    private String settingId;

    /**
     * 最近执行状态
     */
    private String templateId;

    /**
     * 最近执行时间
     */
    private Timestamp timestamp;

    @TableField(exist = false)
    private int page;

    @TableField(exist = false)
    private int row;

    @TableField(exist = false)
    private String scope;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String number = "";

    @TableField(exist = false)
    private String triggers = TriggerType.NOTBUILT.getName();

    @TableField(exist = false)
    private String sort;

    @TableField(exist = false)
    private JobTemplateModel jobTemplateModel;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getIscollect() {
        return iscollect;
    }

    public void setIscollect(String iscollect) {
        this.iscollect = iscollect;
    }

    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getPipeline() {
        return pipeline;
    }

    public void setPipeline(String pipeline) {
        this.pipeline = pipeline;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Timestamp getCreadTime() {
        return creadTime;
    }

    public void setCreadTime(Timestamp creadTime) {
        this.creadTime = creadTime;
    }

    public String getCreader() {
        return creader;
    }

    public void setCreader(String creader) {
        this.creader = creader;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public String getSettingId() {
        return settingId;
    }

    public void setSettingId(String settingId) {
        this.settingId = settingId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public JobTemplateModel getJobTemplateModel() {
        return jobTemplateModel;
    }

    public void setJobTemplateModel(JobTemplateModel jobTemplateModel) {
        this.jobTemplateModel = jobTemplateModel;
    }
}




