package com.hengtiansoft.jenkins.model;


/**
 *任务视图
 * @author zdd_x
 */
public class JobViewModel {

    private String jobViewId;

    private String viewName;

    private String xml;

    public String getJobViewId() {
        return jobViewId;
    }

    public void setJobViewId(String jobViewId) {
        this.jobViewId = jobViewId;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
