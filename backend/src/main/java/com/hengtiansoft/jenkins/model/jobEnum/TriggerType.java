package com.hengtiansoft.jenkins.model.jobEnum;

/**
 * 任务构建触发方式
 * @author zdd_x
 */

public enum TriggerType {

    /**
     * 流水线为构建
     */
    NOTBUILT("未构建"),

    /**
     * 页面手动触发
     */
    MANUAL("页面手动触发"),
    /**
     * 自动触发
     */
    AUTOMATIC("自动触发");

    private String name;

    TriggerType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
