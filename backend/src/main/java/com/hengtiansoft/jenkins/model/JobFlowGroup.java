package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 流水线分组
 * @author zdd_x
 */
@TableName("job_flow_groups")
public class JobFlowGroup {

    /**
     * 分组id
     */
    private int groupId;

    /**
     * 分组名称
     */
    private String name;

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
