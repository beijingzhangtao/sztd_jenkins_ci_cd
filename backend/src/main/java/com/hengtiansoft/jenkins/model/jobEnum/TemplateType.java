package com.hengtiansoft.jenkins.model.jobEnum;

/**
 * 模板分类
 * @author zdd_x
 */
public enum TemplateType {

    /**
     * java-jar
     */
    JAVA("1","Java jar"),
    /**
     * java-war
     */
    JAVAWAR("0","Java war"),
    /**
     * PHP
     */
    PHP("2","PHP"),
    /**
     * Node.js
     */
    NODE_JS("3","Node.js"),
    /**
     * Go
     */
    GO("4","Go"),
    /**
     * Python
     */
    PYTHON("5","Python"),
    /**
     * .NET Core
     */
    NET_CORE("6",".NET Core"),
    /**
     * C++
     */
    C("7","C++"),
    /**
     * 移动端
     */
    APP("8","移动端");

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    TemplateType(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
