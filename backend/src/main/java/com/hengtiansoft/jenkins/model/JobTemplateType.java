package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 *  模板类型表
 * @author zdd_x
 */
@TableName("job_template_type")
public class JobTemplateType {

    /**
     * id
     */
    private int id;

    /**
     * 模板名称
     */
    private String name;

    /**
     * 排序字段
     */
    private int sort;

    /**
     * 启用状态
     */
    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
