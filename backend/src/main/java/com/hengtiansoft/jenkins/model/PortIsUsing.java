package com.hengtiansoft.jenkins.model;

/**
 * 端口占用类
 * @author zdd_x
 */
public class PortIsUsing {

    /** 判断类型 linux  & docker */
    private String type;

    /** 端口号 */
    private int port;

    /** 命令输出 */
    private String log;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}
