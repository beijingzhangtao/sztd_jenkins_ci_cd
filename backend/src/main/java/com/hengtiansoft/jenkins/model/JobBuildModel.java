package com.hengtiansoft.jenkins.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import java.sql.Timestamp;

/**
 * 任务构建类
 * @author zdd_x
 */
@TableName("job_build")
public class JobBuildModel {

    /**
     * 构建id
     */
    private String buildId;

    /**
     * 构建状态
     */
    private String status;

    /**
     * 任务id
     */
    private String jobId;

    /**
     * 持续时间
     */
    private int keeptime;

    /**
     * 构建时间
     */
    private Timestamp timestamp;

    /**
     * 构建备注
     */
    private String remake;

    /**
     * 构建日志
     */
    private String log;

    /**
     * 构建编号
     */
    private String number;

    /**
     * 构建用户
     */
    private String creader;

    /**
     * 日志类型 xml&text
     */
    private String logFormat;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 触发信息
     */
    private String triggers;

    @TableField(exist = false)
    private String state;

    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getBuildId() {
        return buildId;
    }

    public void setBuildId(String buildId) {
        this.buildId = buildId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public int getKeeptime() {
        return keeptime;
    }

    public void setKeeptime(int keeptime) {
        this.keeptime = keeptime;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getRemake() {
        return remake;
    }

    public void setRemake(String remake) {
        this.remake = remake;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCreader() {
        return creader;
    }

    public void setCreader(String creader) {
        this.creader = creader;
    }

    public String getLogFormat() {
        return logFormat;
    }

    public void setLogFormat(String logFormat) {
        this.logFormat = logFormat;
    }
}
