package com.hengtiansoft.jenkins.svn;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.trilead.ssh2.Connection;
import com.trilead.ssh2.ConnectionInfo;
import com.trilead.ssh2.Session;
import com.trilead.ssh2.StreamGobbler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author zdd_x
 */
@RestController
@RequestMapping("/svn")
public class SvnController {

    /**
     * 判断 svn 账号和密码是否正确
     */
    @RequestMapping(value = "/svnCheckImpl", method = RequestMethod.POST)
    public BaseRespResult svnCheckImpl(@RequestBody SvnModel svnModel) {
        try {
            boolean status = svnCheckImpl(svnModel.getSshIp(), svnModel.getSshName(), svnModel.getSshPwd());
            return BaseRespResult.successResult(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 判断 ssh 登录账号是否正确
     */
    @RequestMapping(value = "/svnCheck", method = RequestMethod.POST)
    public BaseRespResult svnCheck(@RequestBody SvnModel svnModel) {
        try {
            boolean status = svnCheck(svnModel.getSshIp(), svnModel.getSshName(), svnModel.getSshPwd(), svnModel.getSshPort());
            return BaseRespResult.successResult(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 判断 svn 账号和密码是否正确
     * @param svnLocalhost
     * @param svnName
     * @param svnPwd
     * @return  返回 false 表示当前登录 账号和密码不正确
     */
    public boolean svnCheckImpl(String svnLocalhost,String svnName,String svnPwd){
        boolean csvn = SVNUtil.isURLExist(svnLocalhost,svnName,svnPwd);
        System.out.println("获取svn 登录情况： "+csvn);
        return  csvn;
    }

    /**
     * 判断 ssh 登录账号是否正确
     * @param sshIp
     * @param sshName
     * @param sshPwd
     * @param sshPort
     * @return 返回 false 表示当前登录 账号和密码不正确
     */
    public boolean svnCheck(String sshIp,String sshName,String sshPwd,int sshPort) {
        Connection con = null;
        Session session = null;
        BufferedReader dr = null;
        boolean result =false;
        try {
            con = new Connection(sshIp, sshPort);
            ConnectionInfo info = con.connect();
            result = con.authenticateWithPassword(sshName, sshPwd);
            session = con.openSession();
            session.execCommand("df -T");
            InputStream stdout = session.getStdout();
            stdout = new StreamGobbler(session.getStdout());
            dr = new BufferedReader(new InputStreamReader(stdout));
            String line;
            while ((line = dr.readLine()) != null) {
                System.out.println(line);
//                if (line.startsWith("/dev/")) {
//                    Pattern p = pile("[\\s]+");
//                    String[] arrs = p.split(line);
//                    for (String s : arrs) {
//                        System.out.println(s);
//                    }
//                    if (!arrs[1].startsWith("iso")) {
//                        if (Long.parseLong(arrs[4]) < 5L * 1024 * 1024 || Double.parseDouble(arrs[5]) > 0.9d) {
//                            doAfterThing(mc, arrs[0]);
//                        }
//                    }
//                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                dr.close();
                session.close();
                con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
