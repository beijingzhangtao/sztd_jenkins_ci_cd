package com.hengtiansoft.jenkins.jenkinsApi;

public class PipineStr109 {

    public static String PipineFrame(){
        String ss = "<?xml version='1.1' encoding='UTF-8'?>\n" +
                "<flow-definition plugin=\"workflow-job@1180.v04c4e75dce43\">\n" +
                "  <actions>\n" +
                "    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobAction plugin=\"pipeline-model-definition@2.2081.v3919681ffc1e\"/>\n" +
                "    <org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction plugin=\"pipeline-model-definition@2.2081.v3919681ffc1e\">\n" +
                "      <jobProperties/>\n" +
                "      <triggers/>\n" +
                "      <parameters/>\n" +
                "      <options/>\n" +
                "    </org.jenkinsci.plugins.pipeline.modeldefinition.actions.DeclarativeJobPropertyTrackerAction>\n" +
                "  </actions>\n" +
                "  <description></description>\n" +
                "  <keepDependencies>false</keepDependencies>\n" +
                "  <properties/>\n" +
                "  <definition class=\"org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition\" plugin=\"workflow-cps@2692.v76b_089ccd026\">\n" +
                "    <script>" +
                "</script>\n" +
                "    <sandbox>true</sandbox>\n" +
                "  </definition>\n" +
                "  <triggers/>\n" +
                "  <disabled>false</disabled>\n" +
                "</flow-definition>";
        return ss;
    }
    /**
     *  Pipeline 空模板
     * @return
     */
    public static String pipine_0() {
        String pipine_0 ="" +
                "pipeline{\n" +
                "    //指定运行此流水线的节点\n" +
                "    agent any\n" +
                     "tools {\n" +
                         "maven 'maven3.8.5'\n" +
                            "}\n" +
                "    //流水线的阶段\n" +
                "    stages{\n" +
                "\n" +
                "        //阶段1 获取代码\n" +
                "        stage(\"CheckOut\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage(\"Build\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"运行构建\"\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "    post {\n" +
                "        always{\n" +
                "            script{\n" +
                "                echo \"流水线结束后，经常做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        success{\n" +
                "            script{\n" +
                "                echo \"流水线成功后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "        failure{\n" +
                "            script{\n" +
                "                echo \"流水线失败后，要做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        aborted{\n" +
                "            script{\n" +
                "                echo \"流水线取消后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "    }\n" +
                "};\n" ;

        return pipine_0;
    }

    /**
     *  Pipeline 空模板+svn
     * @return
     */
    public static String pipine_0_svn() {
        String pipine_0_svn ="" +
                "pipeline{\n" +
                "    //指定运行此流水线的节点\n" +
                "    agent any\n" +
                "tools {\n" +
                "maven 'maven3.8.5'\n" +
                "}\n" +
                "\n" +
                "    //流水线的阶段\n" +
                "    stages{\n" +
                "\n" +
                "        //阶段1 获取代码\n" +
                "        stage(\"CheckOut\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '06396f3e-0ee0-4c29-aef4-3c3134042435', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage(\"Build\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"运行构建\"\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "    post {\n" +
                "        always{\n" +
                "            script{\n" +
                "                echo \"流水线结束后，经常做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        success{\n" +
                "            script{\n" +
                "                echo \"流水线成功后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "        failure{\n" +
                "            script{\n" +
                "                echo \"流水线失败后，要做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        aborted{\n" +
                "            script{\n" +
                "                echo \"流水线取消后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "    }\n" +
                "};\n" ;
        return pipine_0_svn;
    }

    /**
     *  Pipeline 空模板+svn+build
     * @return
     */
    public static String pipine_0_svn_build() {
        String pipine_str ="pipeline{\n" +
                "    //指定运行此流水线的节点\n" +
                "    agent any\n" +
                "\n" +
                "   tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    //流水线的阶段\n" +
                "    stages{\n" +
                "\n" +
                "        //阶段1 获取代码\n" +
                "        stage(\"CheckOut\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '06396f3e-0ee0-4c29-aef4-3c3134042435', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') { \n" +
                "             steps {\n" +
                "                  script{\n" +
                "             echo \"开始编译和打包\" \n" +
                "             sh 'mvn clean package'\n" +
                "             echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "    }\n" +
                "    post {\n" +
                "        always{\n" +
                "            script{\n" +
                "                echo \"流水线结束后，经常做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        success{\n" +
                "            script{\n" +
                "                echo \"流水线成功后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "        failure{\n" +
                "            script{\n" +
                "                echo \"流水线失败后，要做的事情\"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        aborted{\n" +
                "            script{\n" +
                "                echo \"流水线取消后，要做的事情\"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "    }\n" +
                "};\n" ;
        return pipine_str;
    }


    /**
     *  Pipeline 空模板+svn+build_auto_dockerfile  动态dockerfile
     * @return
     */
    public static String pipine_0_svn_build_auto_dockerfile() {
        String pipine_str ="pipeline {\n" +
                "    agent any\n" +
                "\n" +
                "    tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    environment {\n" +
                "        docker_directory = 'docker-app'\n" +
                "        docker_repository = '仓库URL'\n" +
                "    }\n" +
                "    \n" +
                "    stages {\n" +
                "        stage('Hello') {\n" +
                "            steps {\n" +
                "                echo 'Hello World'\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "          //阶段1 获取代码\n" +
                "        stage(\"CheckOut\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '06396f3e-0ee0-4c29-aef4-3c3134042435', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') { \n" +
                "             steps {\n" +
                "                  script{\n" +
                "             echo \"开始编译和打包\" \n" +
                "             sh 'mvn clean package'\n" +
                "             echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "\n" +
                "        stage('Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf ${docker_directory}\n" +
                "                mkdir -p ${docker_directory}\n" +
                "                cp /var/jenkins_home/workspace/sss/target/spring-boot-hello-1.0.jar ${docker_directory}/auto-client.jar\n" +
                "                cd ${docker_directory}\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                MAINTAINER cicadasmile\n" +
                "                VOLUME /data/docker/logs\n" +
                "                ADD auto-client.jar application.jar\n" +
                "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/application.jar\"]\n" +
                "                EOF\n" +
                "                cat Dockerfile\n" +
                "                '''\n" +
                "                echo \"create Dockerfile success\"\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}\n" ;
        return pipine_str;
    }


    /**
     *  Pipeline 空模板+svn+build_auto_dockerfile  动态dockerfile  静态参数
     * @return
     */
    public static String pipine_0_svn_build_auto_dockerfile_ok() {
        String pipine_str ="def getHost(){\n" +
                "    def remote = [:]\n" +
                "    remote.name = 'root'\n" +
                "    remote.host = '685vp-393877568-001.iego.vip'\n" +
                "    remote.user = 'root'\n" +
                "    remote.port = 17896\n" +
                "    remote.password = 'sztd1'\n" +
                "    remote.allowAnyHosts = true\n" +
                "    return remote\n" +
                "}\n" +
                "\n" +
                "pipeline {\n" +
                "    agent any\n" +
                "    \n" +
                "      tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    \n" +
                "    environment {\n" +
                "            app_name='application'\n" +
                "            app_port='8099'  //访问端口\n" +
                "            docker_directory = 'docker-app'\n" +
                "            docker_repository = '仓库URL'\n" +
                "            }\n" +
                "            \n" +
                "    stages {\n" +
                "        stage(\"CheckOut\"){  //阶段1 获取代码\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '06396f3e-0ee0-4c29-aef4-3c3134042435', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') {\n" +
                "             steps {\n" +
                "                  script{\n" +
                "                     echo \"开始编译和打包\" \n" +
                "                     sh 'mvn clean package'\n" +
                "                     echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "\n" +
                "        stage('create Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf ${docker_directory}\n" +
                "                mkdir -p ${docker_directory}\n" +
                "                cp /var/jenkins_home/workspace/Pipine_0_svn_build_ssh/target/spring-boot-hello-1.0.jar ${docker_directory}/auto-client.jar\n" +
                "                cd ${docker_directory}\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                EXPOSE 8099\n" +
                "                MAINTAINER ${app_name}\n" +
                "                VOLUME /data/docker/logs\n" +
                "                ADD auto-client.jar /application.jar\n" +
                "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/application.jar\"]\n" +
                "\n" +
                "                '''\n" +
                "                echo \"create Dockerfile success\"\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('docker to images'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/Pipine_0_svn_build_ssh/${docker_directory}\n" +
                "                    docker -v\n" +
                "                    docker stop ${app_name}\n" +
                "                    docker rm ${app_name}\n" +
                "                    docker rmi ${app_name}:auto-client\n" +
                "                    \n" +
                "                    ${app_name}:auto-client\n" +
                "                    docker build -t ${app_name}:auto-client .\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker to run'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/Pipine_0_svn_build_ssh/${docker_directory}\n" +
                "                    docker run -itd --name=${app_name} -p ${app_port}:8080 ${app_name}:auto-client\n" +
                "                    \n" +
                "                    'ifconfig -a | grep inet | grep -v '127.0.0.1' | awk '{ print \\$2}' | awk 'NR==3''\n" +
                "                    echo ${app_port}\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "    }\n" +
                "}\n" ;
        return pipine_str;
    }



    /**
     *  Pipeline 空模板+svn+build_auto_dockerfile  动态dockerfile  动态参数
     * @return
     */
    public static String pipine_0_svn_build_auto_dockerfile_ok1() {
        String pipine_str ="def getHost(){\n" +
                "    def remote = [:]\n" +
                "    remote.name = 'root'\n" +
                "    remote.host = '685vp-393877568-001.iego.vip'\n" +
                "    remote.user = 'root'\n" +
                "    remote.port = 17896\n" +
                "    remote.password = 'sztd1'\n" +
                "    remote.allowAnyHosts = true\n" +
                "    return remote\n" +
                "}\n" +
                "\n" +
                "pipeline {\n" +
                "    agent any\n" +
                "    \n" +
                "      tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    \n" +
                "    environment {\n" +
                "            app_name='application'\n" +
                "            app_v='V1.0' //版本号  docker run 镜像的时候需要使用\n" +
                "            app_port='8099'  //外网访问端口\n" +
                "            app_port_docker='8080'  //docker 容器端口\n" +
                "            jenkins_name='Pipine_0_svn_build_ssh' //jenkins项目名称\n" +
                "            docker_directory='docker-app'   //临时创建的 用于 jenkins保存jar的 目录\n" +
                "            docker_repository='https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello' //'''仓库URL'\n" +
                "            }\n" +
                "            \n" +
                "    stages {\n" +
                "        stage(\"CheckOut\"){  //阶段1 获取代码\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '06396f3e-0ee0-4c29-aef4-3c3134042435', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: 'https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') {\n" +
                "             steps {\n" +
                "                  script{\n" +
                "                     echo \"开始编译和打包\" \n" +
                "                     sh 'mvn clean package'\n" +
                "                     echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "\n" +
                "        stage('create Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf ${docker_directory}\n" +
                "                mkdir -p ${docker_directory}\n" +
                "                cp /var/jenkins_home/workspace/${jenkins_name}/target/spring-boot-hello-1.0.jar ${docker_directory}/${app_v}.jar\n" +
                "                cd ${docker_directory}\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                EXPOSE 8099\n" +
                "                MAINTAINER ${app_name}\n" +
                "                VOLUME /data/docker/logs\n" +
                "                ADD ${app_v}.jar /${app_name}.jar\n" +
                "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/${app_name}.jar\"]\n" +
                "\n" +
                "                '''\n" +
                "                echo \"create Dockerfile success\"\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('docker to images'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker -v\n" +
                "                    docker stop ${app_name}\n" +
                "                    docker rm ${app_name}\n" +
                "                    docker rmi ${app_name}:${app_v}\n" +
                "                    \n" +
                "                    ${app_name}:${app_v}\n" +
                "                    docker build -t ${app_name}:${app_v} .\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker to run'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker run -itd --name=${app_name} -p ${app_port}:${app_port_docker} ${app_name}:${app_v}\n" +
                "              \n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "    }\n" +
                "}\n" ;
        return pipine_str;
    }


    public static String pipelin_jenkinsfile(String remote,String credentialsId){
        String jenkinsfile="<?xml version='1.1' encoding='UTF-8'?>\n" +
                "<flow-definition plugin=\"workflow-job@1180.v04c4e75dce43\">\n" +
                "  <description></description>\n" +
                "  <keepDependencies>false</keepDependencies>\n" +
                "  <properties/>\n" +
                "  <definition class=\"org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition\" plugin=\"workflow-cps@2692.v76b_089ccd026\">\n" +
                "    <scm class=\"hudson.scm.SubversionSCM\" plugin=\"subversion@2.15.5\">\n" +
                "      <locations>\n" +
                "        <hudson.scm.SubversionSCM_-ModuleLocation>\n" +
                "          <remote>https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello</remote>\n" +
                "          <credentialsId>06396f3e-0ee0-4c29-aef4-3c3134042435</credentialsId>\n" +
                "          <local>.</local>\n" +
                "          <depthOption>infinity</depthOption>\n" +
                "          <ignoreExternalsOption>true</ignoreExternalsOption>\n" +
                "          <cancelProcessOnExternalsFail>true</cancelProcessOnExternalsFail>\n" +
                "        </hudson.scm.SubversionSCM_-ModuleLocation>\n" +
                "      </locations>\n" +
                "      <excludedRegions></excludedRegions>\n" +
                "      <includedRegions></includedRegions>\n" +
                "      <excludedUsers></excludedUsers>\n" +
                "      <excludedRevprop></excludedRevprop>\n" +
                "      <excludedCommitMessages></excludedCommitMessages>\n" +
                "      <workspaceUpdater class=\"hudson.scm.subversion.UpdateUpdater\"/>\n" +
                "      <ignoreDirPropChanges>false</ignoreDirPropChanges>\n" +
                "      <filterChangelog>false</filterChangelog>\n" +
                "      <quietOperation>true</quietOperation>\n" +
                "    </scm>\n" +
                "    <scriptPath>Jenkinsfile</scriptPath>\n" +
                "    <lightweight>true</lightweight>\n" +
                "  </definition>\n" +
                "  <triggers/>\n" +
                "  <disabled>false</disabled>\n" +
                "</flow-definition>";
        return jenkinsfile;
    }
}
