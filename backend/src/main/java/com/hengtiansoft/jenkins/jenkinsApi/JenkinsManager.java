package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Computer;
import com.offbytwo.jenkins.model.LabelWithDetails;
import com.offbytwo.jenkins.model.Plugin;
import com.offbytwo.jenkins.model.PluginManager;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 获取 Jenkins 相关信息
 *
 * 例如获取插件信息、获取Label信息、关闭Jenkins等
 * @author zdd_x
 */
@Component
@DependsOn("jenkinsConnect")
public class JenkinsManager {


    /**
     * Jenkins 对象
     */
    private JenkinsServer jenkinsServer;

    /**
     * 构造方法中调用连接 Jenkins 方法
     */
    public JenkinsManager(){
        // 连接 Jenkins
        jenkinsServer = JenkinsConnect.connection();
        // 设置客户端连接 Jenkins
//        jenkinsHttpClient = JenkinsConnect.getClient();
    }

    /**
     * 获取主机信息
     */
    public Map<String, Computer> getComputerInfo() throws IOException {
        Map<String, Computer> map = jenkinsServer.getComputers();
//        return map;
        for(Computer computer : map.values()) {
            // 获取当前节点-节点名称
            System.out.println(computer.details().getDisplayName());
            // 获取当前节点-执行者数量
            System.out.println(computer.details().getNumExecutors());
            // 获取当前节点-执行者详细信息
//            List<Executor> executorList = computer.details().getExecutors();
//            // 查看当前节点-是否脱机
//            System.out.println(computer.details().getOffline());
//            // 获得节点的全部统计信息
//            LoadStatistics loadStatistics = computer.details().getLoadStatistics();
//            // 获取节点的-监控数据
//            Map<String, Map> monitorData = computer.details().getMonitorData();
            //......
        }
        return map;
    }


    /**
     * 重启 Jenkins
     */
    public void restart() throws IOException {
        jenkinsServer.restart(true);
    }

    /**
     * 安全重启 Jenkins
     */
    public void safeRestart() throws IOException {
        jenkinsServer.safeRestart(true);
    }

    /**
     * 安全结束 Jenkins
     */
    public void safeExit() throws IOException {
        jenkinsServer.safeExit(true);
    }

    /**
     * 关闭 Jenkins 连接
     */
    public void close() {
        jenkinsServer.close();
    }

    /**
     * 根据 Label 查找代理节点信息
     */
    public LabelWithDetails getLabelNodeInfo(String labeName) throws IOException {
        if(StringUtils.isEmpty(labeName)){
            labeName = "jnlp-agent";
        }
        LabelWithDetails labelWithDetails = jenkinsServer.getLabel(labeName);
        // 获取标签名称
        System.out.println(labelWithDetails.getName());
        // 获取 Cloud 信息
        System.out.println(labelWithDetails.getClouds());
        // 获取节点信息
        System.out.println(labelWithDetails.getNodeName());
        // 获取关联的 Job
        System.out.println(labelWithDetails.getTiedJobs());
        // 获取参数列表
        System.out.println(labelWithDetails.getPropertiesList());
        // 是否脱机
        System.out.println(labelWithDetails.getOffline());
        // 获取描述信息
        System.out.println(labelWithDetails.getDescription());
        return labelWithDetails;
    }

    /**
     * 判断 Jenkins 是否运行
     */
    public boolean isRunning() {
        boolean isRunning = jenkinsServer.isRunning();
        System.out.println(isRunning);
        return isRunning;
    }

    /**
     * 获取 Jenkins 插件信息
     */
    public List<Plugin> getPluginInfo() throws IOException {
        PluginManager pluginManager =jenkinsServer.getPluginManager();
        // 获取插件列表
        List<Plugin> plugins = pluginManager.getPlugins();
        for (Plugin plugin:plugins){
            // 插件 wiki URL 地址
            System.out.println(plugin.getUrl());
            // 版本号
            System.out.println(plugin.getVersion());
            // 简称
            System.out.println(plugin.getShortName());
            // 完整名称
            System.out.println(plugin.getLongName());
            // 是否支持动态加载
            System.out.println(plugin.getSupportsDynamicLoad());
            // 插件依赖的组件
            System.out.println(plugin.getDependencies());
        }
        return plugins;
    }

//    public static void main(String[] args) {
//        // 创建 JenkinsManager 对象，并在构造方法中连接 Jenkins
//        JenkinsManager jenkinsManager = new JenkinsManager();
//        ViewApiHelper viewApi = new ViewApiHelper();
//        // 创建视图
//        viewApi.createView("","");
//        // 重启 Jenkinsx
//        //jenkinsApi.restart();
//        // 安全重启 Jenkins
//        //jenkinsApi.safeRestart();
//        // 获取节点信息
//        //jenkinsApi.getComputerInfo();
//        // 安全结束 Jenkins
//        //jenkinsApi.safeExit();
//        // 关闭 Jenkins 连接
//        //jenkinsApi.close();
//        // 获取 Label 节点信息
//        //jenkinsApi.getLabelNodeInfo();
//        // 查看 Jenkins 是否允许
//        //jenkinsApi.isRunning();
//        // 获取 Jenkins 插件信息
//        //jenkinsApi.getPluginInfo();
//    }

}
