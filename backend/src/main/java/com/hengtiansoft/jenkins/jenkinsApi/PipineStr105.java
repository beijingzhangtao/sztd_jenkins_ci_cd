package com.hengtiansoft.jenkins.jenkinsApi;



import java.util.Locale;

public class PipineStr105 {



    // jenkinsFile  所在的svn 创建jenkinsJob的时候使用本方法
    public static String pipelin_jenkinsfile(String remote,String credentialsId){
        String jenkinsfile="<?xml version='1.1' encoding='UTF-8'?>\n" +
                "<flow-definition plugin=\"workflow-job@1180.v04c4e75dce43\">\n" +
                "  <actions/>\n" +
                "  <description></description>\n" +
                "  <keepDependencies>false</keepDependencies>\n" +
                "  <properties/>\n" +
                "  <definition class=\"org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition\" plugin=\"workflow-cps@2692.v76b_089ccd026\">\n" +
                "    <scm class=\"hudson.scm.SubversionSCM\" plugin=\"subversion@2.15.5\">\n" +
                "      <locations>\n" +
                "        <hudson.scm.SubversionSCM_-ModuleLocation>\n" +
                "          <remote>"+remote+"</remote>\n" +
                "          <credentialsId>"+credentialsId+"</credentialsId>\n" +
                "          <local>.</local>\n" +
                "          <depthOption>infinity</depthOption>\n" +
                "          <ignoreExternalsOption>true</ignoreExternalsOption>\n" +
                "          <cancelProcessOnExternalsFail>true</cancelProcessOnExternalsFail>\n" +
                "        </hudson.scm.SubversionSCM_-ModuleLocation>\n" +
                "      </locations>\n" +
                "      <excludedRegions></excludedRegions>\n" +
                "      <includedRegions></includedRegions>\n" +
                "      <excludedUsers></excludedUsers>\n" +
                "      <excludedRevprop></excludedRevprop>\n" +
                "      <excludedCommitMessages></excludedCommitMessages>\n" +
                "      <workspaceUpdater class=\"hudson.scm.subversion.UpdateUpdater\"/>\n" +
                "      <ignoreDirPropChanges>false</ignoreDirPropChanges>\n" +
                "      <filterChangelog>false</filterChangelog>\n" +
                "      <quietOperation>true</quietOperation>\n" +
                "    </scm>\n" +
                "    <scriptPath>Jenkinsfile</scriptPath>\n" +
                "    <lightweight>true</lightweight>\n" +
                "  </definition>\n" +
                "  <triggers/>\n" +
                "  <disabled>false</disabled>\n" +
                "</flow-definition>";
        return jenkinsfile;
    }



    // 需要执行的  Pipeline xml  脚本   上传到svn的时候使用

    /**
     *
     * @param sshUrl   ssh 服务器 需要执行ssh命令，生成jenkinsFile  cp jinkinsFile
     * @param sshUser
     * @param sshPwd
     * @param sshProt
     * @param projectSvnUrl  项目的svn地址，下载最新代码  编译jar需要使用
     * @param jenkinsJobName  jenkinsJob  任务名称
     * @param app_port      项目访问的端口
     * @param app_port_docker  项目对应docker部署的内部端口
     * @param app_jar_name     生成的jar包的名字
     * @return
     */
    public static String Pipine_0_svn_build(String sshUrl,String sshUser,String sshPwd,int sshProt,String projectSvnUrl,String jenkinsJobName,int app_port,int app_port_docker,String app_jar_name) {
        String ss = "def getHost(){\n" +
                "    def remote = [:]\n" +
                "    remote.name = 'root'\n" +
                "    remote.host = '"+sshUrl+"'\n" +
                "    remote.user = '"+sshUser+"'\n" +
                "    remote.port = "+sshProt+"\n" +
                "    remote.password = '"+sshPwd+"'\n" +
                "    remote.allowAnyHosts = true\n" +
                "    return remote\n" +
                "}\n" +
                "\n" +
                "pipeline {\n" +
                "    agent any\n" +
                "    \n" +
                "      tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    \n" +
                "    environment {\n" +
                "            app_name='"+jenkinsJobName+"'\n" +
                "            app_v='V1.0' //版本号  docker run 镜像的时候需要使用\n" +
                "            app_port='"+app_port+"'  //外网访问端口\n" +
                "            app_port_docker='"+app_port_docker+"'  //docker 容器端口\n" +
                "            jenkins_name='"+jenkinsJobName+"' //jenkins项目名称\n" +
                "            docker_directory='"+jenkinsJobName+"'   //临时创建的 用于 jenkins保存jar的 目录\n" +
                "            docker_repository='"+projectSvnUrl+"' //'''仓库URL'\n" +
                "            }\n" +
                "            \n" +
                "    stages {\n" +
                "        stage(\"CheckOut\"){  //阶段1 获取代码\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '3a9d8f1a-491e-4be2-a55f-b37a04541883', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: '"+projectSvnUrl+"']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') {\n" +
                "             steps {\n" +
                "                  script{\n" +
                "                     echo \"开始编译和打包\" \n" +
                "                     sh 'mvn clean install -Dmaven.test.skip=true'\n" +
                "                     echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "\n" +
                "        stage('create Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf ${docker_directory}\n" +
                "                mkdir -p ${docker_directory}\n" +
                "                cp /var/jenkins_home/workspace/${jenkins_name}/"+app_jar_name+" ${docker_directory}/${app_v}.jar\n" +
                "                cd ${docker_directory}\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                EXPOSE 8099\n" +
                "                MAINTAINER ${app_name}\n" +
                "                VOLUME /data/docker/logs\n" +
                "                ADD ${app_v}.jar /${app_name}.jar\n" +
                "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/${app_name}.jar\"]\n" +
                "\n" +
                "                '''\n" +
                "                echo \"create Dockerfile success\"\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('docker to images'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker -v\n" +
                "                    docker stop ${app_name}\n" +
                "                    docker rm ${app_name}\n" +
                "                    docker rmi ${app_name}:${app_v}\n" +
                "                    \n" +
                "                    ${app_name}:${app_v}\n" +
                "                    docker build -t ${app_name}:${app_v} .\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker to run'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker run -itd --name=${app_name} -p ${app_port}:${app_port_docker} ${app_name}:${app_v}\n" +
                "              \n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "    }\n" +
                "}\n";
        return ss;
    }


    /**
     *
     * @param sshUrl   ssh 服务器 需要执行ssh命令，生成jenkinsFile  cp jinkinsFile
     * @param sshUser
     * @param sshPwd
     * @param sshProt
     * @param projectSvnUrl  项目的svn地址，下载最新代码  编译jar需要使用
     * @param jenkinsJobName  jenkinsJob  任务名称
     * @param app_port      项目访问的端口
     * @param app_port_docker  项目对应docker部署的内部端口
     * @param app_jar_name     生成的jar包的名字
     * @return
     */
    public static String Pipine_0_svn(String sshUrl,String sshUser,String sshPwd,int sshProt,String projectSvnUrl,String jenkinsJobName,int app_port,int app_port_docker,String app_jar_name) {
        String ss = "def getHost(){\n" +
                "    def remote = [:]\n" +
                "    remote.name = 'root'\n" +
                "    remote.host = '"+sshUrl+"'\n" +
                "    remote.user = '"+sshUser+"'\n" +
                "    remote.port = "+sshProt+"\n" +
                "    remote.password = '"+sshPwd+"'\n" +
                "    remote.allowAnyHosts = true\n" +
                "    return remote\n" +
                "}\n" +
                "\n" +
                "pipeline {\n" +
                "    agent any\n" +
                "    \n" +
                "      tools {\n" +
                "        maven 'maven3.8.5'\n" +
                "    }\n" +
                "    \n" +
                "    environment {\n" +
                "            app_name='"+jenkinsJobName+"'\n" +
                "            app_v='V1.0' //版本号  docker run 镜像的时候需要使用\n" +
                "            app_port='"+app_port+"'  //外网访问端口\n" +
                "            app_port_docker='"+app_port_docker+"'  //docker 容器端口\n" +
                "            jenkins_name='"+jenkinsJobName+"' //jenkins项目名称\n" +
                "            docker_directory='"+jenkinsJobName+"'   //临时创建的 用于 jenkins保存jar的 目录\n" +
                "            docker_repository='"+projectSvnUrl+"' //'''仓库URL'\n" +
                "            }\n" +
                "            \n" +
                "    stages {\n" +
                "        stage(\"CheckOut\"){  //阶段1 获取代码\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '3a9d8f1a-491e-4be2-a55f-b37a04541883', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: '"+projectSvnUrl+"']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('build project') {\n" +
                "             steps {\n" +
                "                  script{\n" +
                "                     echo \"开始编译和打包\" \n" +
                "                     sh 'mvn clean package -Dmaven.test.skip=true' \n" +
                "                     echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } \n" +
                "\n" +
                "        stage('create Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf ${docker_directory}\n" +
                "                mkdir -p ${docker_directory}\n" +
                "                cp /var/jenkins_home/workspace/${jenkins_name}/"+app_jar_name+" ${docker_directory}/${app_v}.jar\n" +
                "                cd ${docker_directory}\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                EXPOSE 8099\n" +
                "                MAINTAINER ${app_name}\n" +
                "                VOLUME /data/docker/logs\n" +
                "                ADD ${app_v}.jar /${app_name}.jar\n" +
                "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/${app_name}.jar\"]\n" +
                "\n" +
                "                '''\n" +
                "                echo \"create Dockerfile success\"\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('docker to images'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker -v\n" +
                "                    docker stop ${app_name}\n" +
                "                    docker rm ${app_name}\n" +
                "                    docker rmi ${app_name}:${app_v}\n" +
                "                    \n" +
                "                    ${app_name}:${app_v}\n" +
                "                    docker build -t ${app_name}:${app_v} .\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker to run'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/${jenkins_name}/${docker_directory}\n" +
                "                    docker run -itd --name=${app_name} -p ${app_port}:${app_port_docker} ${app_name}:${app_v}\n" +
                "              \n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "    }\n" +
                "}\n";
        return ss;
    }

    public static String get_pipine_ok(String get_pipine_docker_ssh,String get_pipine_svn_checkout_code,String get_pipine_test_script_sonarqube,String get_pipine_build_project,String set_pipine_docker_dockerfile_image_run,String set_pipeine_end_post) {
       String script="" +
               "\n" +
               "       "+get_pipine_docker_ssh+"\n" +
               "\n" +
               "pipeline {\n" +
               "    agent any\n" +
               "    \n" +
               "      tools {\n" +
               "        maven 'maven3.8.5'\n" +
               "    }\n" +
               "\n" +
               "    stages {\n" +
               "\n" +
               "    // 1、check code\n" +
               "\n" +
               "       "+get_pipine_svn_checkout_code+"\n" +
               "\n" +
               "    // 2、sonarqube createfiel getscript\n" +
               "\n" +
               "       "+get_pipine_test_script_sonarqube+"\n" +
               "\n" +
               "    // 3、code build\n" +
               "\n" +
               "      "+get_pipine_build_project+"\n" +
               "\n" +
               "    // 2、docker file image run\n" +
               "\n" +
               "      "+set_pipine_docker_dockerfile_image_run+"\n" +
               "\n" +
               "    }\n" +
               "    // 4、post pipelin执行完成  post请求\n" +
               "      "+set_pipeine_end_post+"\n" +
               "}";
       return script;
    }
        //======================================================================================================================

    /**
     *   jenkins 里面无法执行  docker  需要ssh登录后执行
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @return
     */
    public static String get_pipine_docker_ssh(String sshUrl,int sshPort,String sshName,String sshPwd) {

        String sshScript="def getHost(){\n" +
                "    def remote = [:]\n" +
                "    remote.name = 'root'\n" +
                "    remote.host = '"+sshUrl+"'\n" +
                "    remote.user = '"+sshName+"'\n" +
                "    remote.port = "+sshPort+"\n" +
                "    remote.password = '"+sshPwd+"'\n" +
                "    remote.allowAnyHosts = true\n" +
                "    return remote\n" +
                "}";
        return sshScript;
    }
    public static String get_pipine_svn_checkout_code(String projectSvn,String credentialsId){

        String script="        stage(\"CheckOut\"){\n" +
                "            steps{\n" +
                "                script{\n" +
                "                    echo \"获取代码\"\n" +
                "checkout([$class: 'SubversionSCM', additionalCredentials: [], excludedCommitMessages: '', excludedRegions: '', excludedRevprop: '', excludedUsers: '', filterChangelog: false, ignoreDirPropChanges: false, includedRegions: '', locations: [[cancelProcessOnExternalsFail: true, credentialsId: '"+credentialsId+"', depthOption: 'infinity', ignoreExternalsOption: true, local: '.', remote: '"+projectSvn+"']], quietOperation: true, workspaceUpdater: [$class: 'UpdateUpdater']])                }\n" +
                "         \n" +
                "            }\n" +
                "        }";
       return script;
    }
    /**
     * 代码检查
     * 1、jenkins 安装 sonarqube插件  配置sonarqubu tools 配置连接sonarqube服务器的凭证
     * 2、上传 sonar-project.properties 到需要测试的项目的根目录中
     * 3、在pipelin中执行如下脚本
     * @return
     */
    public static String get_pipine_test_script_sonarqube(String jobName,String app_v){

        String script="stage('create sonar-project.properties') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                cd /var/jenkins_home/workspace/"+jobName+"\n" +
                "              \n" +
                "                cat>sonar-project.properties<<EOF\n" +
                "\n" +
                "                sonar.projectKey="+jobName+"\n" +
                "                sonar.projectName="+jobName+"\n" +
                "                sonar.projectVersion="+app_v+"\n" +
                "                \n" +
                "                sonar.sources=.\n" +
//                "                sonar.exclusions=**/test/**,**/target/**\n" +
                "                \n" +
                "                sonar.java.binaries=.\n" +
                "                \n" +
                "                sonar.java.source =1.8\n" +
                "                sonar.java.target =1.8\n" +
                "                sonar.sourceEncoding=UTF-8\n" +
                "                \n" +
                "                ls\n" +
                "\n" +
                "                '''\n" +
                "                echo \"create sonar-project.properties success-----------------------------------------------------------\"\n" +
                "                \n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "         stage('code checking') {\n" +
                "                steps {\n" +
                "                    script {\n" +
                "                        //引入sonarQubeScanner工具\n" +
                "                        scannerHome = tool 'sonar-scanner'\n" +
                "                    }\n" +
                "                    //引入sonarQube的服务器环境\n" +
                "                    withSonarQubeEnv('sonarqube') {\n" +
                "                        sh \"${scannerHome}/bin/sonar-scanner\"\n" +
                "                    }\n" +
                "    \n" +
                "                }\n" +
                "            }";
        return script;
    }

    public static String get_pipine_build_project(){

        String script="     stage('build project') {\n" +
                "             steps {\n" +
                "                  script{\n" +
                "                     echo \"开始编译和打包\" \n" +
                "                     sh 'mvn clean package'\n" +
                "                     echo \"编译和打包结束\"\n" +
                "                  }\n" +
                "            } \n" +
                "        } ";
        return script;
    }

    /**
     *
     * @param docker_directory  临时创建的 用于 jenkins保存jar的 目录
     * @param jenkins_name jenkins job name
     * @param app_v   项目版本号
     * @param app_name  docker容器 name
     * @param app_port  项目访问的端口
     * @param app_port_docker 项目对应docker部署的内部端口
     * @return
     */
    public static String set_pipine_jar_docker_dockerfile_image_run(String docker_directory,String jenkins_name,String app_v,String app_name,int app_port,int app_port_docker,String app_jar_name){
        app_name = app_name.toLowerCase(Locale.ROOT);
        app_v=app_v.toLowerCase(Locale.ROOT);

        String script="        stage('create Dockerfile') {\n" +
                "            steps {\n" +
                "                sh '''\n" +
                "                rm -rf "+docker_directory+"\n" +
                "                mkdir -p "+docker_directory+"\n" +
                "                cp /var/jenkins_home/workspace/"+jenkins_name+"/"+app_jar_name+" "+docker_directory+"/"+app_v+".jar\n" +
                "                cd "+docker_directory+"\n" +
                "                cat>Dockerfile<<EOF\n" +
                "                FROM java:8\n" +
                "                EXPOSE "+app_port+"\n" +
                "                MAINTAINER "+app_name+"\n" +
            "                VOLUME /data/docker/logs\n" +
            "                ADD "+app_v+".jar /"+app_name+".jar\n" +
        "                ENTRYPOINT [\"java\",\"-Dspring.profiles.active=dev\",\"-Djava.security.egd=file:/dev/./urandom\",\"-jar\",\"/"+app_name+".jar\"]\n" +
        "\n" +
        "                '''\n" +
        "                echo \"create Dockerfile success\"\n" +
        "            }\n" +
        "        }\n" +
        "\n" +
        "        stage('init-server'){\n" +
        "            steps {\n" +
        "                script {                 \n" +
        "                   server = getHost()                                   \n" +
        "                }\n" +
        "            }\n" +
        "        }\n" +
        "        stage('docker to images'){\n" +
        "            steps {\n" +
        "                script {\n" +
        "                sshCommand remote: server, command: \"\"\"                 \n" +
        "                    cd /var/jenkins_mount/workspace/"+jenkins_name+"/"+docker_directory+"\n" +
        "                    docker -v\n" +
        "                    docker stop "+app_name+"\n" +
        "                    docker rm "+app_name+"\n" +
        "                    docker rmi "+app_name+":"+app_v+"\n" +
        "                    \n" +
        "                    "+app_name+":"+app_v+"\n" +
        "                    docker build -t "+app_name+":"+app_v+" .\n" +
        "                  \"\"\"\n" +
        "                }\n" +
        "            }\n" +
        "            \n" +
        "        }\n" +
        "        \n" +
        "        stage('docker to run'){\n" +
        "            steps {\n" +
        "                script {\n" +
        "                sshCommand remote: server, command: \"\"\"                 \n" +
        "                    cd /var/jenkins_mount/workspace/"+jenkins_name+"/"+docker_directory+"\n" +
        "                    docker run -itd --name="+app_name+" -p "+app_port+":"+app_port_docker+" "+app_name+":"+app_v+"\n" +
        "              \n" +
        "                  \"\"\"\n" +
        "                }\n" +
        "            }\n" +
        "            \n" +
        "        }";
                return script;
    }

    /**
     * 部署war  到 docker
     * @param conName
     * @param app_jar_name
     * @return
     */
    public static String set_pipine_war_tomcat_run(String jobName,String conName,String app_jar_name){

        String[] spstr = app_jar_name.split("/");
        String script="        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('cp war tomcat'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    cd /var/jenkins_mount/workspace/"+jobName+"/"+spstr[0]+"\n" +
                "                    docker cp "+spstr[1]+" "+conName+":/usr/local/tomcat/webapps\n" +
                "         \n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker restart tomact'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    docker restart "+conName+"\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }";
        return script;
    }

    /**
     * 部署war  到 docker
     * @param projectName
     * @param wPort
     * @param cPort
     * @return
     */
    public static String set_pipine_war_vue_run(String projectName,int wPort,int cPort){

        String script="        stage('init-server'){\n" +
                "            steps {\n" +
                "                script {                 \n" +
                "                   server = getHost()                                   \n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        stage('docker vue images'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                    docker stop "+projectName+"\n" +
                "                    docker rm "+projectName+"\n" +
                "                    docker rmi "+projectName+":"+projectName+"\n" +
                "                    \n" +
                "                    cd /var/jenkins_mount/workspace/"+projectName+"\n" +
                "                    docker build -t nginx:"+projectName+" .\n" +
                "         \n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }\n" +
                "        \n" +
                "        stage('docker start nginx'){\n" +
                "            steps {\n" +
                "                script {\n" +
                "                sshCommand remote: server, command: \"\"\"                 \n" +
                "                   docker run --name "+projectName+" -p "+wPort+":"+cPort+" -d nginx:"+projectName+"\n" +
                "                  \"\"\"\n" +
                "                }\n" +
                "            }\n" +
                "            \n" +
                "        }";
        return script;
    }

    /**
     *
     * @param exe_end_script
     * @param exe_ok_set_pipine_jmeter
     * @param exe_fail_script
     * @param exe_cancel_script
     * @return
     */
    public static String set_pipeine_end_post(String jobName,String jenkinsCallBack,String exe_end_script,String exe_ok_set_pipine_jmeter,String exe_fail_script,String exe_cancel_script){
        System.out.println("回调地址："+jenkinsCallBack);
        if(jenkinsCallBack.equals("")||jenkinsCallBack==null){
            System.out.println("回调地址异常");
        }else{
            exe_end_script = "curl "+jenkinsCallBack+" -X POST -d '{\"jobName\":"+jobName+"}' --header \"Content-Type: application/json\"";
            exe_ok_set_pipine_jmeter = "curl "+jenkinsCallBack+" -X POST -d '{\"jobName\":"+jobName+",\"state\":\"2\"}' --header \"Content-Type: application/json\"";
            exe_fail_script = "curl "+jenkinsCallBack+" -X POST -d '{\"jobName\":"+jobName+",\"state\":\"3\"}' --header \"Content-Type: application/json\"";
            exe_cancel_script = "curl "+jenkinsCallBack+" -X POST -d '{\"jobName\":"+jobName+",\"state\":\"4\"}' --header \"Content-Type: application/json\"";
        }
        String script="    post {\n" +
                "        always{\n" +
                "            script{\n" +
                "                echo \"流水线结束后，经常做的事情\"\n" +
                "                     "+exe_end_script+"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        success{\n" +
                "            script{\n" +
                "                echo \"流水线成功后，要做的事情\"\n" +
                "                    "+exe_ok_set_pipine_jmeter+"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "        failure{\n" +
                "            script{\n" +
                "                echo \"流水线失败后，要做的事情\"\n" +
                "                    "+exe_fail_script+"\n" +
                "            }\n" +
                "        }\n" +
                "        \n" +
                "        aborted{\n" +
                "            script{\n" +
                "                echo \"流水线取消后，要做的事情\"\n" +
                "                    "+exe_cancel_script+"\n" +
                "            }\n" +
                "        \n" +
                "        }\n" +
                "    }";
        return script;
    }

    /**
     *
     * @param jtl_file_name  demo.jtl
     * @return
     */
    public static String get_exe_ok_set_pipine_jmeter(String jtl_file_name,String script_xml){
        String script="" +
                "mkdir /home/sztd1/jmeter\n" +
                "rm -rf  /home/sztd1/jmeter/"+jtl_file_name+".jmx\n" +
                "cd /home/sztd1/  \n" +
                "cat>"+jtl_file_name+".jmx<<EOF\n" +
                ""+script_xml+"\n" +
                "EOF\n" +
                "                   ls                                  \n" +
                "                echo \"create jmeter  jmx  success--------------------------------------------\"\n" +
                "/home/sztd1/apache-jmeter-5.4.3/bin/jmeter.sh -n -t /home/sztd1/"+jtl_file_name+".jmx  -l /home/sztd1/jmeter/"+jtl_file_name+".jtl -j /home/sztd1/jmeter/"+jtl_file_name+".log -e -o /home/sztd1/jmeter/output";
        return script;
    }

    public static String get_jmeter_xml(){
        String script="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<jmeterTestPlan version=\"1.2\" properties=\"5.0\" jmeter=\"5.0 r1840935\">\n" +
                "  <hashTree>\n" +
                "    <TestPlan guiclass=\"TestPlanGui\" testclass=\"TestPlan\" testname=\"Demo\" enabled=\"true\">\n" +
                "      <stringProp name=\"TestPlan.comments\"></stringProp>\n" +
                "      <boolProp name=\"TestPlan.functional_mode\">false</boolProp>\n" +
                "      <boolProp name=\"TestPlan.tearDown_on_shutdown\">true</boolProp>\n" +
                "      <boolProp name=\"TestPlan.serialize_threadgroups\">false</boolProp>\n" +
                "      <elementProp name=\"TestPlan.user_defined_variables\" elementType=\"Arguments\" guiclass=\"ArgumentsPanel\" testclass=\"Arguments\" testname=\"User Defined Variables\" enabled=\"true\">\n" +
                "        <collectionProp name=\"Arguments.arguments\"/>\n" +
                "      </elementProp>\n" +
                "      <stringProp name=\"TestPlan.user_define_classpath\"></stringProp>\n" +
                "    </TestPlan>\n" +
                "    <hashTree>\n" +
                "      <ThreadGroup guiclass=\"ThreadGroupGui\" testclass=\"ThreadGroup\" testname=\"Thread Group\" enabled=\"true\">\n" +
                "        <stringProp name=\"ThreadGroup.on_sample_error\">continue</stringProp>\n" +
                "        <elementProp name=\"ThreadGroup.main_controller\" elementType=\"LoopController\" guiclass=\"LoopControlPanel\" testclass=\"LoopController\" testname=\"Loop Controller\" enabled=\"true\">\n" +
                "          <boolProp name=\"LoopController.continue_forever\">false</boolProp>\n" +
                "          <stringProp name=\"LoopController.loops\">10</stringProp>\n" +
                "        </elementProp>\n" +
                "        <stringProp name=\"ThreadGroup.num_threads\">2</stringProp>\n" +
                "        <stringProp name=\"ThreadGroup.ramp_time\">0</stringProp>\n" +
                "        <boolProp name=\"ThreadGroup.scheduler\">false</boolProp>\n" +
                "        <stringProp name=\"ThreadGroup.duration\"></stringProp>\n" +
                "        <stringProp name=\"ThreadGroup.delay\"></stringProp>\n" +
                "      </ThreadGroup>\n" +
                "      <hashTree>\n" +
                "        <HTTPSamplerProxy guiclass=\"HttpTestSampleGui\" testclass=\"HTTPSamplerProxy\" testname=\"index\" enabled=\"true\">\n" +
                "          <elementProp name=\"HTTPsampler.Arguments\" elementType=\"Arguments\" guiclass=\"HTTPArgumentsPanel\" testclass=\"Arguments\" testname=\"User Defined Variables\" enabled=\"true\">\n" +
                "            <collectionProp name=\"Arguments.arguments\"/>\n" +
                "          </elementProp>\n" +
                "          <stringProp name=\"HTTPSampler.domain\">127.0.0.1</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.port\">8585</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.protocol\">http</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.contentEncoding\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.path\">/</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.method\">GET</stringProp>\n" +
                "          <boolProp name=\"HTTPSampler.follow_redirects\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.auto_redirects\">false</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.use_keepalive\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.DO_MULTIPART_POST\">false</boolProp>\n" +
                "          <stringProp name=\"HTTPSampler.embedded_url_re\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.connect_timeout\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.response_timeout\"></stringProp>\n" +
                "        </HTTPSamplerProxy>\n" +
                "        <hashTree/>\n" +
                "        <HTTPSamplerProxy guiclass=\"HttpTestSampleGui\" testclass=\"HTTPSamplerProxy\" testname=\"rightaway\" enabled=\"true\">\n" +
                "          <elementProp name=\"HTTPsampler.Arguments\" elementType=\"Arguments\" guiclass=\"HTTPArgumentsPanel\" testclass=\"Arguments\" testname=\"User Defined Variables\" enabled=\"true\">\n" +
                "            <collectionProp name=\"Arguments.arguments\"/>\n" +
                "          </elementProp>\n" +
                "          <stringProp name=\"HTTPSampler.domain\">127.0.0.1</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.port\">8585</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.protocol\">http</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.contentEncoding\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.path\">/rightaway</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.method\">GET</stringProp>\n" +
                "          <boolProp name=\"HTTPSampler.follow_redirects\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.auto_redirects\">false</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.use_keepalive\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.DO_MULTIPART_POST\">false</boolProp>\n" +
                "          <stringProp name=\"HTTPSampler.embedded_url_re\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.connect_timeout\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.response_timeout\"></stringProp>\n" +
                "        </HTTPSamplerProxy>\n" +
                "        <hashTree/>\n" +
                "        <HTTPSamplerProxy guiclass=\"HttpTestSampleGui\" testclass=\"HTTPSamplerProxy\" testname=\"sleep\" enabled=\"true\">\n" +
                "          <elementProp name=\"HTTPsampler.Arguments\" elementType=\"Arguments\" guiclass=\"HTTPArgumentsPanel\" testclass=\"Arguments\" testname=\"User Defined Variables\" enabled=\"true\">\n" +
                "            <collectionProp name=\"Arguments.arguments\"/>\n" +
                "          </elementProp>\n" +
                "          <stringProp name=\"HTTPSampler.domain\">192.168.64.128/</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.port\">8999</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.protocol\">http</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.contentEncoding\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.path\">/sleep</stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.method\">GET</stringProp>\n" +
                "          <boolProp name=\"HTTPSampler.follow_redirects\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.auto_redirects\">false</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.use_keepalive\">true</boolProp>\n" +
                "          <boolProp name=\"HTTPSampler.DO_MULTIPART_POST\">false</boolProp>\n" +
                "          <stringProp name=\"HTTPSampler.embedded_url_re\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.connect_timeout\"></stringProp>\n" +
                "          <stringProp name=\"HTTPSampler.response_timeout\"></stringProp>\n" +
                "        </HTTPSamplerProxy>\n" +
                "        <hashTree/>\n" +
                "        <ResultCollector guiclass=\"ViewResultsFullVisualizer\" testclass=\"ResultCollector\" testname=\"View Results Tree\" enabled=\"true\">\n" +
                "          <boolProp name=\"ResultCollector.error_logging\">false</boolProp>\n" +
                "          <objProp>\n" +
                "            <name>saveConfig</name>\n" +
                "            <value class=\"SampleSaveConfiguration\">\n" +
                "              <time>true</time>\n" +
                "              <latency>true</latency>\n" +
                "              <timestamp>true</timestamp>\n" +
                "              <success>true</success>\n" +
                "              <label>true</label>\n" +
                "              <code>true</code>\n" +
                "              <message>true</message>\n" +
                "              <threadName>true</threadName>\n" +
                "              <dataType>true</dataType>\n" +
                "              <encoding>false</encoding>\n" +
                "              <assertions>true</assertions>\n" +
                "              <subresults>true</subresults>\n" +
                "              <responseData>false</responseData>\n" +
                "              <samplerData>false</samplerData>\n" +
                "              <xml>false</xml>\n" +
                "              <fieldNames>true</fieldNames>\n" +
                "              <responseHeaders>false</responseHeaders>\n" +
                "              <requestHeaders>false</requestHeaders>\n" +
                "              <responseDataOnError>false</responseDataOnError>\n" +
                "              <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>\n" +
                "              <assertionsResultsToSave>0</assertionsResultsToSave>\n" +
                "              <bytes>true</bytes>\n" +
                "              <sentBytes>true</sentBytes>\n" +
                "              <url>true</url>\n" +
                "              <threadCounts>true</threadCounts>\n" +
                "              <idleTime>true</idleTime>\n" +
                "              <connectTime>true</connectTime>\n" +
                "            </value>\n" +
                "          </objProp>\n" +
                "          <stringProp name=\"filename\"></stringProp>\n" +
                "        </ResultCollector>\n" +
                "        <hashTree/>\n" +
                "        <ResultCollector guiclass=\"SummaryReport\" testclass=\"ResultCollector\" testname=\"Summary Report\" enabled=\"true\">\n" +
                "          <boolProp name=\"ResultCollector.error_logging\">false</boolProp>\n" +
                "          <objProp>\n" +
                "            <name>saveConfig</name>\n" +
                "            <value class=\"SampleSaveConfiguration\">\n" +
                "              <time>true</time>\n" +
                "              <latency>true</latency>\n" +
                "              <timestamp>true</timestamp>\n" +
                "              <success>true</success>\n" +
                "              <label>true</label>\n" +
                "              <code>true</code>\n" +
                "              <message>true</message>\n" +
                "              <threadName>true</threadName>\n" +
                "              <dataType>true</dataType>\n" +
                "              <encoding>false</encoding>\n" +
                "              <assertions>true</assertions>\n" +
                "              <subresults>true</subresults>\n" +
                "              <responseData>false</responseData>\n" +
                "              <samplerData>false</samplerData>\n" +
                "              <xml>false</xml>\n" +
                "              <fieldNames>true</fieldNames>\n" +
                "              <responseHeaders>false</responseHeaders>\n" +
                "              <requestHeaders>false</requestHeaders>\n" +
                "              <responseDataOnError>false</responseDataOnError>\n" +
                "              <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>\n" +
                "              <assertionsResultsToSave>0</assertionsResultsToSave>\n" +
                "              <bytes>true</bytes>\n" +
                "              <sentBytes>true</sentBytes>\n" +
                "              <url>true</url>\n" +
                "              <threadCounts>true</threadCounts>\n" +
                "              <idleTime>true</idleTime>\n" +
                "              <connectTime>true</connectTime>\n" +
                "            </value>\n" +
                "          </objProp>\n" +
                "          <stringProp name=\"filename\"></stringProp>\n" +
                "        </ResultCollector>\n" +
                "        <hashTree/>\n" +
                "        <ResultCollector guiclass=\"StatVisualizer\" testclass=\"ResultCollector\" testname=\"Aggregate Report\" enabled=\"true\">\n" +
                "          <boolProp name=\"ResultCollector.error_logging\">false</boolProp>\n" +
                "          <objProp>\n" +
                "            <name>saveConfig</name>\n" +
                "            <value class=\"SampleSaveConfiguration\">\n" +
                "              <time>true</time>\n" +
                "              <latency>true</latency>\n" +
                "              <timestamp>true</timestamp>\n" +
                "              <success>true</success>\n" +
                "              <label>true</label>\n" +
                "              <code>true</code>\n" +
                "              <message>true</message>\n" +
                "              <threadName>true</threadName>\n" +
                "              <dataType>true</dataType>\n" +
                "              <encoding>false</encoding>\n" +
                "              <assertions>true</assertions>\n" +
                "              <subresults>true</subresults>\n" +
                "              <responseData>false</responseData>\n" +
                "              <samplerData>false</samplerData>\n" +
                "              <xml>false</xml>\n" +
                "              <fieldNames>true</fieldNames>\n" +
                "              <responseHeaders>false</responseHeaders>\n" +
                "              <requestHeaders>false</requestHeaders>\n" +
                "              <responseDataOnError>false</responseDataOnError>\n" +
                "              <saveAssertionResultsFailureMessage>true</saveAssertionResultsFailureMessage>\n" +
                "              <assertionsResultsToSave>0</assertionsResultsToSave>\n" +
                "              <bytes>true</bytes>\n" +
                "              <sentBytes>true</sentBytes>\n" +
                "              <url>true</url>\n" +
                "              <threadCounts>true</threadCounts>\n" +
                "              <idleTime>true</idleTime>\n" +
                "              <connectTime>true</connectTime>\n" +
                "            </value>\n" +
                "          </objProp>\n" +
                "          <stringProp name=\"filename\"></stringProp>\n" +
                "        </ResultCollector>\n" +
                "        <hashTree/>\n" +
                "      </hashTree>\n" +
                "    </hashTree>\n" +
                "  </hashTree>\n" +
                "</jmeterTestPlan>\n";
        return script;
    }
}
