package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import com.offbytwo.jenkins.model.View;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * View(视图) 相关操作
 *
 * 例如对视图的增、删、改、查等操作
 */
@Component
public class ViewApiHelper {

    /**
     * Jenkins 对象
     */
    private JenkinsServer jenkinsServer;

    /**
     * http 客户端对象
     */
    private JenkinsHttpClient jenkinsHttpClient;

    /**
     * 构造方法中调用连接 Jenkins 方法
     */
    public ViewApiHelper() {
        // 连接 Jenkins
        jenkinsServer = JenkinsConnect.connection();
        // 设置客户端连接 Jenkins
        jenkinsHttpClient = JenkinsConnect.getClient();
    }

    /**
     * 创建视图
     */
    public void createView(String viewName, String xml) throws IOException {
        if(StringUtils.isEmpty(xml)){
            xml = "<listView _class=\"hudson.model.ListView\">\n" +
                    "<description>zhangtao  create view ...用于测试的视图</description>\n" +
                    "</listView>";
        }
        jenkinsServer.createView(viewName, xml,true);
    }

    /**
     * 获取视图基本信息
     */
    public String getView(String viewName) throws IOException {
        // 获取视图基本信息
        View view = jenkinsServer.getView(viewName);
        System.out.println(view.getName());
        System.out.println(view.getUrl());
        System.out.println(view.getDescription());
        // 获取视图xml信息
        String viewXml = jenkinsHttpClient.get("/view/" + viewName + "/api/xml");
        System.out.println(viewXml);
        return viewXml;
    }

    /**
     * 获取视图基本信息
     */
    public String getViewXml(String viewName) throws IOException {
        String viewXml = jenkinsHttpClient.get("/view/" + viewName + "/api/xml");
        System.out.println(viewXml);
        return viewXml;
    }

    /**
     * 获取视图配置 XML 信息
     */
    public String getViewConfig(String viewName) throws IOException {
        // 获取视图配置xml信息
        String viewConfigXml = jenkinsHttpClient.get("/view/" + viewName + "/config.xml");
        System.out.println(viewConfigXml);
        return viewConfigXml;
    }

    /**
     * 更新视图信息
     */
    public void updateView(String jobName, String xml) throws IOException {
        if(StringUtils.isEmpty(xml)){
            xml = "<hudson.model.ListView>\n" +
                    "<name>test-view</name>\n" +
                    "<description>update view 用于测试的视图1111</description>\n" +
                    "<filterExecutors>false</filterExecutors>\n" +
                    "<filterQueue>false</filterQueue>\n" +
                    "<properties class=\"hudson.model.View$PropertyList\"/>\n" +
                    "<jobNames>\n" +
                    "<comparator class=\"hudson.util.CaseInsensitiveComparator\"/>\n" +
                    "</jobNames>\n" +
                    "<jobFilters/>\n" +
                    "<columns>\n" +
                    "<hudson.views.StatusColumn/>\n" +
                    "<hudson.views.WeatherColumn/>\n" +
                    "<hudson.views.JobColumn/>\n" +
                    "<hudson.views.LastSuccessColumn/>\n" +
                    "<hudson.views.LastFailureColumn/>\n" +
                    "<hudson.views.LastDurationColumn/>\n" +
                    "<hudson.views.BuildButtonColumn/>\n" +
                    "<hudson.plugins.favorite.column.FavoriteColumn plugin=\"favorite@2.3.2\"/>\n" +
                    "</columns>\n" +
                    "<recurse>false</recurse>\n" +
                    "</hudson.model.ListView>";
        }
        jenkinsServer.updateView(jobName, xml);
    }

    /**
     * 删除视图信息
     */
    public void deleteView(String viewName) throws IOException {
        jenkinsHttpClient.post("/view/" + viewName + "/doDelete");
    }


//    public static void main(String[] args) {
//        ViewApiHelper viewApi = new ViewApiHelper();
//        String viewName = "test-view";
//        // 创建视图
//        viewApi.createView(viewName,"");
//        // 获取视图信息
//        viewApi.getView(viewName);
//        // 获取视图配置xml信息
//        viewApi.getViewConfig(viewName);
//        // 更新视图信息
//        viewApi.updateView(viewName,"");
//        // 删除视图
//        viewApi.deleteView(viewName);
//    }
}
