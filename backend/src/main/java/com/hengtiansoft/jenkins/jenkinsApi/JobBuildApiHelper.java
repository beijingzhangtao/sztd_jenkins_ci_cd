package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.unit.JobBuildLogException;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.helper.Range;
import com.offbytwo.jenkins.model.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Job Build(任务构建) 相关操作
 *
 * 例如对任务 Build 相关的信息进行获取操作、例如获取构建日志
 * @author zdd_x
 */
@Component
public class JobBuildApiHelper {

    /**
     * Jenkins 对象
     */
    private JenkinsServer jenkinsServer;


    /**
     * 构造方法中调用连接 Jenkins 方法
     */
    public JobBuildApiHelper() {
        // 连接 Jenkins
        jenkinsServer = JenkinsConnect.connection();
    }

    /**
     * 获取 Job 最后的 Build
     */
    public JobWithDetails getJobLastBuild(String jobName) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 获得首次编译信息
        Build firstBuild = job.getFirstBuild();
        // 获得最后编译信息
        Build lastBuild = job.getLastBuild();
        // 获取最后成功的编译信息
        Build lastSuccessfulBuild = job.getLastSuccessfulBuild();
        // 获取最后事变的编译信息
        Build lastFailedBuild = job.getLastFailedBuild();
        // 获取最后完成的编译信息
        Build lastCompletedBuild = job.getLastCompletedBuild();
        // 获取最后稳定的编译信息
        Build lastStableBuild = job.getLastStableBuild();
        // 获取最后不稳定的编译信息
        Build lastUnstableBuild = job.getLastUnstableBuild();
        // 获取最后未成功的编译信息
        Build lastUnsuccessfulBuild = job.getLastUnsuccessfulBuild();
        return job;
    }


    /**
     * 根据 Job Build 编号获取编译信息
     */
    public Build getJobByNumber(String jobName,int buildNumber ) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 根据 Build 编号获取编译信息
        Build numberBuild = job.getBuildByNumber(buildNumber);
        return numberBuild;
    }

    /**
     * 根据 Job Build 编号获取日志
     */
    public String getJobByNumberForLog(String jobName,int buildNumber ) throws IOException, JobBuildLogException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 根据 Build 编号获取编译信息
        Build buildByNumber = job.getBuildByNumber(buildNumber);
        if(null == buildByNumber){
            throw new JobBuildLogException("该任务当前没有日志，或已删除");
        }
        BuildWithDetails build = buildByNumber.details();
        return build.getConsoleOutputText();
    }

    /**
     * 获取全部 Job Build列表
     */
    public List<Build> getJobBuildListAll(String jobName) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 获取全部 Build 信息
        List<Build> buildsList = job.getAllBuilds();
        for (Build build:buildsList){
            System.out.println(build.getNumber());
        }
        return buildsList;
    }

    /**
     * 获取 Job 一定范围的 Build 列表
     */
    public List<Build> getJobBuildListRange(String jobName, int firstRange, int lastRange) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 设定范围
        Range range = Range.build().from(firstRange).to(lastRange);
        System.err.println(range.getRangeString());
        // 获取一定范围的 Build 信息
        List<Build> buildsList = job.getAllBuilds(range);
        for (Build build:buildsList){
            System.out.println(build.getNumber());
        }
        return buildsList;
    }

    /**
     * 获取 Build 基本信息
     */
    public JobWithDetails getJobBuildInfo(String jobName) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 这里用最后一次编译来示例
        Build build = job.getLastBuild();
        // 获取构建的 URL 地址
        System.out.println(build.getUrl());
        // 获取构建编号
        System.out.println(build.getNumber());
        // 获取测试报告
        //build.getTestReport();
        // 获取测试结果
        //build.getTestResult();
        return job;
    }

    /**
     * 获取 Build 详细信息
     */
    public BuildWithDetails getJobBuildDetailInfo(String jobName, String state) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        if(null == job){
            return null;
        }
        if("first".equals(state)){
            return job.getFirstBuild().details();
        }else if("last".equals(state)){
            return job.getLastBuild().details();
        }else if("lastSuccess".equals(state)){
            return job.getLastSuccessfulBuild().details();
        }else if("lastFailed".equals(state)){
            return job.getLastFailedBuild().details();
        }else if("lastCompleted".equals(state)){
            return job.getLastCompletedBuild().details();
        }else if("lastStable".equals(state)){
            return job.getLastStableBuild().details();
        }else if("lastUnstable".equals(state)){
            return job.getLastUnstableBuild().details();
        }else if("lastUnsuccessful".equals(state)){
            return job.getLastUnsuccessfulBuild().details();
        }else{
            return job.getFirstBuild().details();
        }
//        // 这里用最后一次编译来示例
//        BuildWithDetails build = job.getLastBuild().details();
//        // 获取构建的显示名称
//        System.out.println(build.getDisplayName());
//        // 获取构建的参数信息
//        System.out.println(build.getParameters());
//        // 获取构建编号
//        System.out.println(build.getNumber());
//        // 获取构建结果，如果构建未完成则会显示为null
//        System.out.println(build.getResult());
//        // 获取执行构建的活动信息
//        System.out.println(build.getActions());
//        // 获取构建持续多少时间(ms)
//        System.out.println(build.getDuration());
//        // 获取构建开始时间戳
//        System.out.println(build.getTimestamp());
//        // 获取构建头信息，里面包含构建的用户，上游信息，时间戳等
//        List<BuildCause> buildCauses = build.getCauses();
//        for (BuildCause bc:buildCauses){
//            System.out.println(bc.getUserId());
//            System.out.println(bc.getShortDescription());
//            System.out.println(bc.getUpstreamBuild());
//            System.out.println(bc.getUpstreamProject());
//            System.out.println(bc.getUpstreamUrl());
//            System.out.println(bc.getUserName());
//        }
    }

    /**
     * 获取 Build Log 日志信息
     */
    public String getJobBuildLog(String jobName, String logFormat) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jobName);
        // 这里用最后一次编译来示例
        BuildWithDetails build = job.getLastBuild().details();
        if("text".equals(logFormat)){
            return build.getConsoleOutputText();
        }else if("html".equals(logFormat)){
            return build.getConsoleOutputHtml();
        }else{
            return build.getConsoleOutputText();
        }

//            // 获取构建的日志，如果正在执行构建，则会只获取已经执行的过程日志
//            // Text格式日志
//            System.out.println(build.getConsoleOutputText());
//            // Html格式日志
//            System.out.println(build.getConsoleOutputHtml());
//
//            // 获取部分日志,一般用于正在执行构建的任务
//            ConsoleLog consoleLog = build.getConsoleOutputText(0);
//            // 获取当前日志大小
//            System.out.println(consoleLog.getCurrentBufferSize());
//            // 是否已经构建完成，还有更多日志信息
//            System.out.println(consoleLog.getHasMoreData());
//            // 获取当前截取的日志信息
//            System.out.println(consoleLog.getConsoleLog());
    }

    /**
     * 获取正在执行构建任务的日志信息
     */
    public String getBuildActiveLog(String jobName) throws IOException {
        // 这里用最后一次编译来示例
        BuildWithDetails build = jenkinsServer.getJob(jobName).getLastBuild().details();
        // 当前日志
        ConsoleLog currentLog = build.getConsoleOutputText(0);
        // 输出当前获取日志信息
        String consoleLog = currentLog.getConsoleLog();
        System.out.println(consoleLog);
        return consoleLog;
//            // 检测是否还有更多日志,如果是则继续循环获取
//            while (currentLog.getHasMoreData()){
//                // 获取最新日志信息
//                ConsoleLog newLog = build.getConsoleOutputText(currentLog.getCurrentBufferSize());
//                // 输出最新日志
//                System.out.println(newLog.getConsoleLog());
//                currentLog = newLog;
//                // 睡眠1s
//                Thread.sleep(1000);
//            }
//        }catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    public static void main(String[] args) throws IOException {
        JobBuildApiHelper jobBuildApi = new JobBuildApiHelper();
        String jobName = "pip_test";
        // 获取 Job 最后的 Build
//        jobBuildApi.getJobLastBuild(jobName);
//        // 根据 Job Build 编号获取编译信息
        Build msg = jobBuildApi.getJobByNumber(jobName,1);
//        // 获取 Build 全部列表
//        jobBuildApi.getJobBuildListAll(jobName);
//        // 获取一定范围的 Build 列表
//        jobBuildApi.getJobBuildListRange(jobName, 1, 2);
//        // 获取 Build 基本信息
//        jobBuildApi.getJobBuildInfo(jobName);
        // 获取 Build 详细信息
//        jobBuildApi.getJobBuildDetailInfo(jobName);
        // 获取 Build Log 日志信息
//        String msg =jobBuildApi.getJobBuildLog(jobName, "text");
        System.out.println(msg);
        // 获得正在执行的编译 Log 日志信息
//        jobBuildApi.getBuildActiveLog(jobName);
    }
}
