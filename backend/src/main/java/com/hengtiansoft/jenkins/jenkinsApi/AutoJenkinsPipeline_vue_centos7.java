package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.svn.SVNUtil;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.trilead.ssh2.Connection;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * zhangtao
 * 2022-5-28
 */
public class AutoJenkinsPipeline_vue_centos7 {

    public static void main(String[] args) throws SVNException, IOException {
        String []exeTypeJar= {"vue包只发布","vue不编译","发布+测试",""};
        String []exeTypeWar= {"vue包只发布","vue包测试发布",""};
        String type="vue";
        jenkins_ai_auto_105_vue(type,exeTypeJar[0],exeTypeWar[2]);
    }

    // 调用测试方法-------------------------------------------------------------------------------------------------
    //105 vue 105-----调用方法
    private static String jenkins_ai_auto_105_vue(String type,String exeTypeJar1,String exeTypeWar1) throws SVNException, IOException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://192.168.0.105:10240/";
        JenkinsConnect.JENKINS_USERNAME = "admin";
        JenkinsConnect.JENKINS_PASSWORD = "11ab3021f899dd31764e54a37631839ae0";

        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/vue";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/vue";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = svnLocalhost;//"svn://39.106.53.5:4431/svn/jenkinsfile/vue";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
        String jobName = "AI_AUTO_TEST_VUE";
        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022-htsy-ai-test/auto_bushu";
        String credentialsId="f595296b-436a-4e36-ae65-bafc87f3830f";

        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=8068;  //docker 容器对外访问端口
        int app_port_docker=8080; //docker容器内部端口
        String app_v="1.0";

        //参数7：可以在流水线中执行 docker命令
        String sshUrl="192.168.0.105";
        int sshPort=22;
        String sshName="root";
        String sshPwd="root";

        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);
        String jenkinsCallBack = "";
        pipelin_exe_vue_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, jenkinsJobXml, app_v, sshUrl, sshPort, sshName, sshPwd,app_port,app_port_docker, jenkinsCallBack);

      return "ok";
    }
    //105 vue 1.8-----调用方法
    private static String jenkins_ai_auto_1_8_vue(String type,String exeTypeJar1,String exeTypeWar1) throws SVNException, IOException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://192.168.1.8:10240/";
        JenkinsConnect.JENKINS_USERNAME = "admin";
        JenkinsConnect.JENKINS_PASSWORD = "11146a21df77f13e7d5b6d6f522e05c7bb";

        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
        String jobName = "ai_auto_test_vue";
        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022-htsy-ai-test/auto_bushu";
        String credentialsId="ad9aae1f-8b93-44b2-9d73-4a81ce0c9d27";

        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=8068;  //docker 容器对外访问端口
        int app_port_docker=8080; //docker容器内部端口
        String app_v="1.0";

        //参数7：可以在流水线中执行 docker命令
        String sshUrl="192.168.1.8";
        int sshPort=22;
        String sshName="root";
        String sshPwd="Admin123";

        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);
        String jenkinsCallBack = "";
        pipelin_exe_vue_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, jenkinsJobXml, app_v, sshUrl, sshPort, sshName, sshPwd,app_port,app_port_docker, jenkinsCallBack);

        return "ok";
    }

    //集成方法--------------------------------------------------------------------------------------------------

    /**
     *  war【ssh:ok】【svn:ok】【sonarqube:no】【mvn:no】【tomcat:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param jenkinsJobXml
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_vue_docker(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String jenkinsJobXml, String app_v, String sshUrl, int sshPort, String sshName, String sshPwd,int app_port,int app_port_docker, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
//        String get_pipine_test_script_sonarqube=PipineStr103.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_war_vue_run(jobName, app_port,app_port_docker);

        // jmeter测试
//        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack,"","","","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,"","",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }

    /**
     *  war【ssh:ok】【svn:ok】【sonarqube:ok】【mvn:no】【tomcat:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_war_name
     * @param jenkinsJobXml
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_test_vue_docker(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_war_name, String jenkinsJobXml, String contName, String app_v,String sshUrl, int sshPort, String sshName, String sshPwd,int app_port,int app_port_docker, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
        String get_pipine_test_script_sonarqube=PipineStr105.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_war_vue_run(jobName, app_port,app_port_docker);

        // jmeter测试
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack,"",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,get_pipine_test_script_sonarqube,"",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }


    // 基础方法---------------------------------------------------------------------------------------------------
    //创建jenkins 任务
    public static void createJenkinsJob(String jobName, String PipelineXml, JobApiHelper jobApi) {
        try {
            //判断是否已经创建任务，匹配重复创建
            JobWithDetails currentJob = jobApi.getJob1(jobName);
            if (currentJob == null) {
                // 第一步： 创建项目
                jobApi.ceateJob(jobName, PipelineXml);
                System.out.println("任务: " + jobName + "创建成功--------------------------------");
            } else if (currentJob.getName().equals(jobName)) {
                System.out.println("任务: " + jobName + " 已经创建，执行删除 重建。。。");
                jobApi.deleteJob(jobName);
                jobApi.ceateJob(jobName, PipelineXml);
            }
            System.out.println("创建任务: " + jobName + "脚本路径： "+PipelineXml);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //创建jenkinsfile上传到svn
    public static void createJenkinsFileToSvn(String svnLocalhost, String jenkinsFileXml, String filePath, String jobName) throws SVNException {
        //创建jenkinsFile 文件到svn目录
        extracted(jenkinsFileXml, filePath + "/" + jobName);

        SVNUtil.authSvn();
        //更新svn到本地
//        SVNUtil.update(filePath, SVNRevision.HEAD, SVNDepth.FILES);
        SVNUtil.addEntry(filePath);
        //commat jenkinsFile文件到svn
        SVNCommitInfo st = SVNUtil.commit(filePath, true, "项目： "+jobName+" : "+System.currentTimeMillis());
        SVNUtil.getLog(svnLocalhost);
        System.out.println("----------创建jenkinsfile上传到svn-----------");
        System.out.println("创建的jenkinsfile: " + jobName + "  本地脚本路径： "+filePath + "/" + jobName);
        System.out.println("创建的jenkinsfile上传到svn : " + jobName + "  svn地址： "+svnLocalhost);
    }

    //创建jenkinsFile 文件到svn目录
    private static void extracted(String jenkinsFileXml, String filePath) {
        //创建jenkinsfile文件
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();// 能创建多级目录
        }
        try {
            file = new File(filePath + "/Jenkinsfile");
            if (file.createNewFile()) {
                System.out.println("文件创建成功！");
            } else {
                System.out.println(filePath + "/Jenkinsfile，该文件已经存在。");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream fileOutputStream;
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(jenkinsFileXml.getBytes("gbk"));
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //执行jenkins 任务
    public static void runJenkinsJobBuild(String jobName, JobApiHelper jobApi) {
        // 构建对应的job
        try {
            System.out.println("***********************开始构建任务: "+ jobApi.getJob1(jobName).getDisplayName() +"  ************************************");
            jobApi.buildJob(jobName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
