package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import com.offbytwo.jenkins.model.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Job(任务) 相关操作
 *
 * 例如对任务的增、删、改、查等操作
 * @author zdd_x
 */
@Component
public class JobApiHelper {

    /**
     * Jenkins 对象
     */
    private JenkinsServer jenkinsServer;

    /**
     * http 客户端对象
     */
    private JenkinsHttpClient jenkinsHttpClient;

    /**
     * 构造方法中调用连接 Jenkins 方法
     */
    public JobApiHelper() {
        jenkinsServer = JenkinsConnect.connection();
    }

    /**
     * 创建 Job
     */
    public void ceateJob(String jobName, String xml) throws IOException {
        if(StringUtils.isEmpty(xml)){
            /**创建一个流水线任务，且设置一个简单的脚本**/
            // 创建 Pipeline 脚本
            String script = "node(){ \n" +
                    "echo 'hello world!' \n" +
                    "}";
            // xml配置文件，且将脚本加入到配置中
            xml = "<flow-definition plugin=\"workflow-job@2.32\">\n" +
                    "<description>"+jobName+"_project </description>\n" +
                    "<definition class=\"org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition\" plugin=\"workflow-cps@2.66\">\n" +
                    "<script>" + script + "</script>\n" +
                    "<sandbox>true</sandbox>\n" +
                    "</definition>\n" +
                    "</flow-definition>";
            // 创建 Job
        }
        jenkinsServer.createJob(jobName, xml, true);
    }

    /**
     * 创建 Job
     */
    public void ceateJobPipelin(String jobName,String pipelin) throws IOException {
        /**创建一个流水线任务，且设置一个简单的脚本**/
        // 创建 Pipeline 脚本
        String script = "node(){ \n" +
                "echo 'hello world!' \n" +
                "}";
        // xml配置文件，且将脚本加入到配置中
        String xml = "<flow-definition plugin=\"workflow-job@2.32\">\n" +
                "<description>"+jobName+"_project </description>\n" +
                "<definition class=\"org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition\" plugin=\"workflow-cps@2.66\">\n" +
                "<script>" + pipelin + "</script>\n" +
                "<sandbox>true</sandbox>\n" +
                "</definition>\n" +
                "</flow-definition>";
        // 创建 Job
        jenkinsServer.createJob(jobName, xml, true);
    }

    /**
     * 更新 Job
     *
     * 更改之前创建的无参数Job，更改其为参数Job
     */
    public void updateJob(String jobName, String xml) throws IOException {
        if(StringUtils.isEmpty(xml)){
            /**创建一个流水线任务，且设置一个简单的脚本**/
            // 创建 Pipeline 脚本
            String script = "node(){ \n" +
                    "echo 'hello world  update !' \n" +
                    "}";
            // xml配置文件，且将脚本加入到配置中
            xml = "<flow-definition plugin=\"workflow-job@2.32\">\n" +
                    "<description>测试项目</description>\n" +
                    "<definition class=\"org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition\" plugin=\"workflow-cps@2.66\">\n" +
                    "<script>" + script + "</script>\n" +
                    "<sandbox>true</sandbox>\n" +
                    "</definition>\n" +
                    "</flow-definition>";
            // 创建 Job
        }
        jenkinsServer.updateJob(jobName,xml);
    }
    /**
     * 更新 Job
     *
     * 更改之前创建的无参数Job，更改其为参数Job
     */
    public void updateJob1(String jobName, String script) throws IOException {
            /**创建一个流水线任务，且设置一个简单的脚本**/
            // 创建 Pipeline 脚本
            // xml配置文件，且将脚本加入到配置中
            String xml = "<flow-definition plugin=\"workflow-job@2.32\">\n" +
                    "<description>测试项目</description>\n" +
                    "<definition class=\"org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition\" plugin=\"workflow-cps@2.66\">\n" +
                    "<script>" + script + "</script>\n" +
                    "<sandbox>true</sandbox>\n" +
                    "</definition>\n" +
                    "</flow-definition>";
            // 创建 Job
        jenkinsServer.updateJob(jobName,xml);
    }

    /**
     * 获取 Job 基本信息
     */
    public JobWithDetails getJob1(String jboName) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jboName);
        return job;
    }

    /**
     * 获取 Job 基本信息
     */
    public JobWithDetails getJob(String jboName) throws IOException {
        // 获取 Job 信息
        JobWithDetails job = jenkinsServer.getJob(jboName);
        // 获取 Job 名称
        System.out.println(job.getName());
        // 获取 Job URL
        System.out.println(job.getUrl());
        // 获取 Job 下一个 build 编号
        System.out.println(job.getNextBuildNumber());
        // 获取 Job 显示的名称
        System.out.println(job.getDisplayName());
        // 输出 Job 描述信息
        System.out.println(job.getDescription());
        // 获取 Job 下游任务列表
        System.out.println(job.getDownstreamProjects());
        // 获取 Job 上游任务列表
        System.out.println(job.getUpstreamProjects());
        return job;
    }

    /**
     * 获取 Maven Job 信息
     */
    public MavenJobWithDetails getMavenJob(String jboName){
        try {
            // 获取 Job 信息
            MavenJobWithDetails job = jenkinsServer.getMavenJob(jboName);
            return job;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取 Job 列表
     */
    public Map<String,Job> getJobList() throws IOException {
        Map<String,Job> jobs = new HashMap<>();
        jobs = jenkinsServer.getJobs();
        return jobs;
    }

    /**
     * 通过 View 名称获取 Job 列表
     */
    public Map<String,Job> getJobListByView(String viewName) throws IOException {
        Map<String,Job> jobs = new HashMap<>();
        if(StringUtils.isEmpty(viewName)){
            viewName = "all";
        }
        jobs = jenkinsServer.getJobs(viewName);
        for (Job job:jobs.values()){
            System.out.println(job.getName());
        }
        return jobs;
    }

    /**
     * 查看 Job XML 信息
     */
    public String getJobConfig(String jboName){
        String xml = "";
        try {
            xml = jenkinsServer.getJobXml(jboName);
            System.out.println("==========================================================");
            System.out.println(xml);
            System.out.println("==========================================================");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xml;
    }

    /**
     * 执行无参数 Job build
     */
    public void buildJob(String jobName) throws IOException {
        jenkinsServer.getJob(jobName).build(true);
    }

    /**
     * 执行带参数 Job build
     */
    public void buildParamJob(String jobName, Map<String,String> param) throws IOException {
        jenkinsServer.getJob(jobName).build(param);
    }

    /**
     * 停止最后构建的 Job Build
     */
    public void stopLastJobBuild(String jobName) throws IOException {
        // 获取最后的 build 信息
        Build build = jenkinsServer.getJob(jobName).getLastBuild();
        // 停止最后的 build
        build.Stop();
    }

    /**
     * 删除 Job
     */
    public void deleteJob(String jobName) throws IOException {
        jenkinsServer.deleteJob(jobName);
    }

    /**
     * 禁用 Job
     */
    public void disableJob(String jobName) throws IOException {
        jenkinsServer.disableJob(jobName);
    }

    /**
     * 启用 Job
     */
    public void enableJob(String jobName) throws IOException {
        jenkinsServer.enableJob(jobName);
    }

    public static void updateBuild_exec() throws IOException {
        JenkinsManager jenkinsApi = new JenkinsManager();
        String jobName = "jenkinsfile";

        JobApiHelper jobApi = new JobApiHelper();
        String newJobXml = jobApi.getJobConfig(jobName);

        // 修改构建脚本
        jobApi.updateJob(jobName,newJobXml);
        // 构建对应的job
        jobApi.getJob1(jobName).build();
            // 获取html格式日志
            // jenkins.getJob("jobName").getLastBuild().details().getConsoleOutputHtml()
            // 获取text格式日志
        String Result_Text=jobApi.getJob1(jobName).getLastBuild().details().getConsoleOutputText();
            // 获取执行结果（是否成功）
        BuildResult result = jobApi.getJob(jobName).getLastBuild().details().getResult();
        System.out.println("***********************************************************");
        System.out.println(result.toString());
        System.out.println(Result_Text);
        System.out.println("***********************************************************");


    }
    public static void main(String[] args) throws IOException {

        String jobName = "test";
        JobApiHelper jobApi = new JobApiHelper();
        jobApi.getJob(jobName);
        jobApi.getJobConfig(jobName);

        // 获取text格式日志
//        String Result_Text=jobApi.getJob1(jobName).getLastBuild().details().getConsoleOutputText();
//        // 获取执行结果（是否成功）
//        BuildResult result = jobApi.getJob(jobName).getLastBuild().details().getResult();
//        System.out.println("***********************************************************");
//        System.out.println(result.toString());
//        System.out.println(Result_Text);
//        System.out.println("***********************************************************");


//        updateBuild_exec();

//        JenkinsManager jenkinsApi = new JenkinsManager();
//        String jobName = "pip_test";
////
//        JobApiHelper jobApi = new JobApiHelper();
//        String p = jobApi.getJobConfig(jobName);
//        System.out.println(p);

//        // 创建 Job
//        jobApi.ceateJob(jobName, "");
//        // 构建无参数的 Job
//        jobApi.buildJob(jobName);
//        // 构建带参数的 Job
//        jobApi.buildParamJob();
//        // 停止最后构建的 Job Build
////        jobApi.stopLastJobBuild();
//        // 更新 Job
//        jobApi.updateJob(jobName,"");
//        // 获取 Job 信息
//        jobApi.getJob(jobName);
//        // 获取 Maven 项目 Job
//        jobApi.getMavenJob(jobName);
//        // 获取 Job 配置xml
//        jobApi.getJobConfig(jobName);
//        // 获取全部 Job 列表
//        jobApi.getJobList();
//        // 根据 view 名称获取 Job 列表
//        jobApi.getJobListByView(jobName);
//        // 禁用 Job
////        jobApi.disableJob();
//        // 启用 Job
//        jobApi.enableJob(jobName);
//        // 删除 Job
////        jobApi.deleteJob();
    }

}
