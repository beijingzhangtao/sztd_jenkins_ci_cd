package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.svn.SVNUtil;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * zhangtao
 * 2022-5-28
 */
public class AutoJenkinsPipeline_centos7_100 {

    public static void main(String[] args) throws SVNException, IOException {
        String []exeTypeJar= {"jar包只发布","jar不编译","发布+测试",""};
        String []exeTypeWar= {"war包只发布","war包测试发布",""};
        String type="jar";
        jenkins_ai_auto_105_run(type,0,0);


    }

    // 调用测试方法-------------------------------------------------------------------------------------------------
    //105  jar ok
    private static void jenkins_ai_auto_105_jar() throws SVNException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://192.168.0.105:10240/";
        JenkinsConnect.JENKINS_USERNAME = "admin";
        JenkinsConnect.JENKINS_PASSWORD = "1191a8fa2b22d874e3c427cce0b68eac1e";

        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
        String jobName = "c_abc";
        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";
        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
        String app_jar_name="target/spring-boot-hello-1.0.jar";

        //参数4：设置项目相关参数 **************************************************************
//        String jobName = "zhjy_taishi_object_jar_docker";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/2022_zhjy_max_data/jeecg-boot/jeecg-boot";
//        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
//        String app_jar_name="jeecg-boot-module-system/target/jeecg-boot-module-system-3.1.0.jar";


        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);


        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=7071;  //docker 容器对外访问端口
        int app_port_docker=8080; //docker容器内部端口
        String app_v="1.0";

        //参数7：可以在流水线中执行 docker命令
        String sshUrl="192.168.0.105";
        int sshPort=22;
        String sshName="root";
        String sshPwd="root";

        String jenkinsCallBack = "";

//        pipelin_exe_no_build(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd);
        pipelin_exe_jar_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
   }

    //105  jar 测试中
    private static void jenkins_ai_auto_105_jar_test() throws SVNException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://192.168.0.105:10240/";
        JenkinsConnect.JENKINS_USERNAME = "sztd1";
        JenkinsConnect.JENKINS_PASSWORD = "1191a8fa2b22d874e3c427cce0b68eac1e";

        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
        String jobName = "zhjy_taishi_object_jar_docker";
        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/2022_zhjy_max_data/jeecg-boot/jeecg-boot";
        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
        String app_jar_name="jeecg-boot-module-system/target/jeecg-boot-module-system-3.1.0.jar";


        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);


        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=7071;  //docker 容器对外访问端口
        int app_port_docker=8080; //docker容器内部端口
        String app_v="1.0";

        //参数7：可以在流水线中执行 docker命令
        String sshUrl="192.168.0.105";
        int sshPort=22;
        String sshName="root";
        String sshPwd="root";

        String jenkinsCallBack = "";

//        pipelin_exe_no_build(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd);
        pipelin_exe_jar_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
    }

    //105 war-----调用方法
    private static String jenkins_ai_auto_105_run(String type,int exeTypeJar1,int exeTypeWar1) throws SVNException, IOException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://192.168.1.8:10240/";
        JenkinsConnect.JENKINS_USERNAME = "admin";
        JenkinsConnect.JENKINS_PASSWORD = "11146a21df77f13e7d5b6d6f522e05c7bb";

        //参数1：登录jenkins系统 调用jenkins API接口
//        JenkinsConnect.JENKINS_URL = "http://535v360l82.51vip.biz:53580/";
//        JenkinsConnect.JENKINS_USERNAME = "sztd1";
//        JenkinsConnect.JENKINS_PASSWORD = "1191a8fa2b22d874e3c427cce0b68eac1e";


        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
//        String jobName = "c_abc";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";
//        String credentialsId="347b9825-e5bc-4c2e-87b0-9a5d10e15990";
//        String app_jar_name="target/spring-boot-hello-1.0.jar";
//        int app_port=8066;  //docker 容器对外访问端口
//        int app_port_docker=8080; //docker容器内部端口
//        String app_v="1.0";

        //参数4：设置项目相关参数 **************************************************************
//        String jobName = "zhjy_taishi_object_jar_docker";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/2022_zhjy_max_data/jeecg-boot/jeecg-boot";
//        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
//        String app_jar_name="jeecg-boot-module-system/target/jeecg-boot-module-system-3.1.0.jar";

//        String jobName = "zhjy_taishi_object_war_docker2";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        //参数4：设置项目相关参数 ************1.8 验证ok *****************************************
//        String jobName = "HelloWord";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";
//        String credentialsId="ad9aae1f-8b93-44b2-9d73-4a81ce0c9d27";
//        String app_jar_name="target/spring-boot-hello-1.0.jar";
//
//        String app_war_name="target/LuckyFrameWeb.war";
//        int app_port=8066;  //docker 容器对外访问端口
//        int app_port_docker=8080; //docker容器内部端口
//        String app_v="V1";

//        String jobName = "zhjy_6_main";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/zhjy_main_project/bdc_svn1";
//        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
//        String app_jar_name="demo/target/demo-2.1.jar";

        // 态势系统
//        String jobName = "zhjy_6_taishi";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/2022_zhjy_max_data/jeecg-boot/jeecg-boot";
//        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
//        String app_jar_name="jeecg-boot-module-system/target/jeecg-boot-module-system-3.1.0.jar";
//        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
//        int app_port=7074;  //docker 容器对外访问端口
//        int app_port_docker=7070; //docker容器内部端口
//        String app_v="1.0";

        // 测试系统  ui
//        String jobName = "auto_ui_test_java_web_jar";
//        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
//        String projectSvn = "https://39.106.53.5:4431/svn/2022-htsy-ai-test/auto_ui_test_java_web";
//        String credentialsId="7c14e40e-afb6-4138-8222-ecd0eac75f2e";
//        String app_jar_name="target/LuckyFrameWeb.jar";
//        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
//        int app_port=9009;  //docker 容器对外访问端口
//        int app_port_docker=9008; //docker容器内部端口
//        String app_v="1.0";

        // pipeline 自动化测试 后台java 接口
        String jobName = "AI_AUTO_TEST_JAVA";
        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022-htsy-ai-test/auto_bushu_java1";
        String credentialsId="ad9aae1f-8b93-44b2-9d73-4a81ce0c9d27";
        String app_jar_name="target/JenkinsApiDemo-0.0.1-SNAPSHOT.jar";
        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=8067;  //docker 容器对外访问端口
        int app_port_docker=8082; //docker容器内部端口
        String app_v="V1.0";
        String app_war_name="target/LuckyFrameWeb.war";


        //参数7：可以在流水线中执行 docker命令
        String sshUrl="192.168.1.8";
        int sshPort=22;
        String sshName="root";
        String sshPwd="Admin123";

        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);

        //当前tomcat 容器的id
        String contName="bb4f658e49df";

        String jenkinsCallBack = "";

        extractedApi(type, exeTypeJar1, exeTypeWar1, svnLocalhost, filePath, app_war_name, jobName, projectSvn, credentialsId, app_jar_name, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, contName, jenkinsCallBack);
        return "ok";
    }

    /**
     *
     * @param type java 项目类型  jar  war
     * @param exeTypeJar0  jar 发布方式  0、只发布 1、编译+发布   2、检测+编译+发布+测试
     * @param exeTypeWar0  war 发布方式  0、只发布  1、编译+发布  2、检测+编译+发布+测试
     * @param svnLocalhost  jenkinsFile 的svn地址
     * @param filePath  本地零时存储目录  jenkinsfile
     * @param app_war_name   war包的位置  target/abc.war
     * @param jobName  任务名称
     * @param projectSvn  项目的svn地址
     * @param credentialsId 项目的svn  tocken id
     * @param app_jar_name  jar 包的位置  target/abc.war
     * @param app_port  docker 容器对外访问端口
     * @param app_port_docker  docker容器内部端口
     * @param app_v   项目版本号   V1.0
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @param contName 部署 war包 需要  tomcat 容器的id
     * @throws SVNException
     */
    public static String extractedApi(String type, int exeTypeJar0, int exeTypeWar0, String svnLocalhost, String filePath, String app_war_name, String jobName, String projectSvn, String credentialsId, String app_jar_name, int app_port, int app_port_docker, String app_v, String sshUrl, int sshPort, String sshName, String sshPwd, String contName, String jenkinsCallBack) throws SVNException {
       String res="ok";
        JobApiHelper jobApi = new JobApiHelper();
        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,credentialsId);

        String []exeTypeJar= {"jar包只发布","jar不编译","发布+测试",""};
        String []exeTypeWar= {"war包只发布","war包测试发布",""};
        String exeTypeJar1=exeTypeJar[exeTypeJar0];
        String exeTypeWar1=exeTypeWar[exeTypeWar0];

        if(type.equals("jar")){
            System.out.println("本次发布是  jar包 ");
            if(exeTypeJar1.equals("jar包只发布")){  //直接发布 停止编译 测试
                pipelin_exe_jar_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
            }else if(exeTypeJar1.equals("jar不编译")){ //直接发布 停止mvn 编译
                pipelin_exe_no_build(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
           }else if(exeTypeJar1.equals("发布+测试")){ // svn 测试  编译  docker发布 jemmt测试 post 发送
                pipelin_exe_all(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_jar_name, jenkinsJobXml, app_port, app_port_docker, app_v, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
            }
        }else if(type.equals("war")){
            System.out.println("本次发布是  war 包 ");
            if(exeTypeWar1.equals("war包只发布")) {  //直接发布 停止编译 测试
                pipelin_exe_war_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_war_name, jenkinsJobXml, contName, sshUrl, sshPort, sshName, sshPwd, jenkinsCallBack);
            }else if(exeTypeWar1.equals("war包测试发布")) {  //直接发布 停止编译 测试
                pipelin_exe_test_war_docker(jobApi, svnLocalhost, filePath, jobName, projectSvn, credentialsId, app_war_name, jenkinsJobXml, contName, app_v, sshUrl, sshPort, sshName, sshPwd ,jenkinsCallBack);
            }
        }else{
            System.out.println("本次发布是  类型 错误 ");
            res="本次发布是  类型 错误 ";
        }
        return  res;
    }

    //集成方法--------------------------------------------------------------------------------------------------
    /**
     *  jar  jar 【ssh:ok】【svn:ok】【sonarqube:ok】【mvn:ok】【dockerfile:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_jar_name
     * @param jenkinsJobXml
     * @param app_port
     * @param app_port_docker
     * @param app_v
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_all(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_jar_name, String jenkinsJobXml, int app_port, int app_port_docker, String app_v, String sshUrl, int sshPort, String sshName, String sshPwd, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
        String get_pipine_test_script_sonarqube= PipineStr105.get_pipine_test_script_sonarqube(jobName, app_v);
        String get_pipine_build_project= PipineStr105.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_jar_docker_dockerfile_image_run(jobName, jobName, app_v, jobName, app_port, app_port_docker, app_jar_name);

        //
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName,jenkinsCallBack,"",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,get_pipine_test_script_sonarqube,get_pipine_build_project,set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }

    /**
     *  jar 【ssh:ok】【svn:ok】【sonarqube:ok】【mvn:no】【dockerfile:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_jar_name
     * @param jenkinsJobXml
     * @param app_port
     * @param app_port_docker
     * @param app_v
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_no_build(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_jar_name, String jenkinsJobXml, int app_port, int app_port_docker, String app_v, String sshUrl, int sshPort, String sshName, String sshPwd, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
        String get_pipine_test_script_sonarqube= PipineStr105.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_jar_docker_dockerfile_image_run(jobName, jobName, app_v, jobName, app_port, app_port_docker, app_jar_name);

        //
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack, "",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,get_pipine_test_script_sonarqube,"",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }

    /**
     *  jar  jar 【ssh:ok】【svn:ok】【sonarqube:no】【mvn:no】【dockerfile:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_jar_name
     * @param jenkinsJobXml
     * @param app_port
     * @param app_port_docker
     * @param app_v
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_jar_docker(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_jar_name, String jenkinsJobXml, int app_port, int app_port_docker, String app_v, String sshUrl, int sshPort, String sshName, String sshPwd, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
//        String get_pipine_test_script_sonarqube=PipineStr103.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_jar_docker_dockerfile_image_run(jobName, jobName, app_v, jobName, app_port, app_port_docker, app_jar_name);

        //
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack,"","","","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,"","",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }



    /**
     *  war【ssh:ok】【svn:ok】【sonarqube:no】【mvn:no】【tomcat:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_war_name
     * @param jenkinsJobXml
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_war_docker(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_war_name, String jenkinsJobXml, String contName, String sshUrl, int sshPort, String sshName, String sshPwd, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
//        String get_pipine_test_script_sonarqube=PipineStr103.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_war_tomcat_run(jobName,contName, app_war_name);

        // jmeter测试
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack, "",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,"","",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }

    /**
     *  war【ssh:ok】【svn:ok】【sonarqube:ok】【mvn:no】【tomcat:ok】【jmeter:ok】【post:ok】
     * @param jobApi
     * @param svnLocalhost
     * @param filePath
     * @param jobName
     * @param projectSvn
     * @param credentialsId
     * @param app_war_name
     * @param jenkinsJobXml
     * @param sshUrl
     * @param sshPort
     * @param sshName
     * @param sshPwd
     * @throws SVNException
     */
    public static void pipelin_exe_test_war_docker(JobApiHelper jobApi, String svnLocalhost, String filePath, String jobName, String projectSvn, String credentialsId, String app_war_name, String jenkinsJobXml, String contName, String app_v,String sshUrl, int sshPort, String sshName, String sshPwd, String jenkinsCallBack) throws SVNException {
        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl, sshPort, sshName, sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn, credentialsId);
        String get_pipine_test_script_sonarqube=PipineStr105.get_pipine_test_script_sonarqube(jobName, app_v);
//        String get_pipine_build_project=PipineStr103.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_war_tomcat_run(jobName,contName, app_war_name);

        // jmeter测试
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName, jenkinsCallBack, "",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,get_pipine_test_script_sonarqube,"",set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }


    // 基础方法---------------------------------------------------------------------------------------------------
    //创建jenkins 任务
    public static void createJenkinsJob(String jobName, String PipelineXml, JobApiHelper jobApi) {
        try {
            //判断是否已经创建任务，匹配重复创建
            JobWithDetails currentJob = jobApi.getJob1(jobName);
            if (currentJob == null) {
                // 第一步： 创建项目
                jobApi.ceateJob(jobName, PipelineXml);
                System.out.println("任务: " + jobName + "创建成功--------------------------------");
            } else if (currentJob.getName().equals(jobName)) {
                System.out.println("任务: " + jobName + " 已经创建，执行删除 重建。。。");
                jobApi.deleteJob(jobName);
                jobApi.ceateJob(jobName, PipelineXml);
            }
            System.out.println("创建任务: " + jobName + "脚本路径： "+PipelineXml);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //创建jenkinsfile上传到svn
    public static void createJenkinsFileToSvn(String svnLocalhost, String jenkinsFileXml, String filePath, String jobName) throws SVNException {
        //创建jenkinsFile 文件到svn目录
        extracted(jenkinsFileXml, filePath + "/" + jobName);

        SVNUtil.authSvn();
        //更新svn到本地
        SVNUtil.update(filePath, SVNRevision.HEAD, SVNDepth.FILES);
        SVNUtil.addEntry(filePath);
        //commat jenkinsFile文件到svn
        SVNCommitInfo st = SVNUtil.commit(filePath, true, "项目： "+jobName+" : "+System.currentTimeMillis());
        SVNUtil.getLog(svnLocalhost);
        System.out.println("----------创建jenkinsfile上传到svn-----------");
        System.out.println("创建的jenkinsfile: " + jobName + "  本地脚本路径： "+filePath + "/" + jobName);
        System.out.println("创建的jenkinsfile上传到svn : " + jobName + "  svn地址： "+svnLocalhost);
    }

    //创建jenkinsFile 文件到svn目录
    private static void extracted(String jenkinsFileXml, String filePath) {
        //创建jenkinsfile文件
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();// 能创建多级目录
        }
        try {
            file = new File(filePath + "/Jenkinsfile");
            if (file.createNewFile()) {
                System.out.println("文件创建成功！");
            } else {
                System.out.println(filePath + "/Jenkinsfile，该文件已经存在。");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream fileOutputStream;
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(jenkinsFileXml.getBytes("gbk"));
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //执行jenkins 任务
    public static void runJenkinsJobBuild(String jobName, JobApiHelper jobApi) {
        // 构建对应的job
        try {
            System.out.println("***********************开始构建任务: "+ jobApi.getJob1(jobName).getDisplayName() +"  ************************************");
            jobApi.buildJob(jobName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
