package com.hengtiansoft.jenkins.jenkinsApi;

import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.svn.SVNUtil;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * zhangtao
 * 2022-5-28
 */
public class AutoJenkinsPipeline {

    public static void main(String[] args) throws SVNException {
        jenkins_ai_auto_103_taishi_project();
    }

    //台式机VM
    private static void jenkins_ai_auto_109() throws SVNException {
        //登录jenkins系统
        JenkinsConnect.JENKINS_URL = "http://535v360l82.51vip.biz";
        JenkinsConnect.JENKINS_USERNAME = "sztd1";
        JenkinsConnect.JENKINS_PASSWORD = "11dbfee6a51bdb8284ecd32dfa47685630";

        //svn参数 jenkinsFile 账号  密码 本地svn目录
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";
        String jenkinsPingZheng ="3a9d8f1a-491e-4be2-a55f-b37a04541883";

        //java 执行svn中jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //需要部署的项目svn 地址
        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";

        //jenkins 脚本中 有时候需要执行的ssh命令
        String jenkinsSshUrl="qx18911892395.iego.vip";
        String jenkinsSshName="root";
        String jenkinsSshPwd="sztd1";
        int jenkinsSshPort=25039;


        String jobName = "aabb";
        JobApiHelper jobApi = new JobApiHelper();
        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,jenkinsPingZheng);
        int app_port=7002;
        int app_port_docker=7002;
        String app_jar_name="jeecg-demo.jar";

        // 脚本需要的参数
        /**
         *
         */
        String jenkinsJobXmlBuild = PipineStr105.Pipine_0_svn_build(jenkinsSshUrl,jenkinsSshName,jenkinsSshPwd,jenkinsSshPort,projectSvn,jobName,app_port,app_port_docker,app_jar_name);


        //生成报告后发送的地址
        String sshHtmlUrl="";
        String sshName="";
        String sshPwd="";
        String sshDer="";

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }
    //笔记本VM
    private static void jenkins_ai_auto_103() throws SVNException {
        //登录jenkins系统
        JenkinsConnect.JENKINS_URL = "http://535v360l82.51vip.biz";
        JenkinsConnect.JENKINS_USERNAME = "sztd1";
        JenkinsConnect.JENKINS_PASSWORD = "11af75e6387c3e4a089d0b0c8ae0ec4430";

        //svn参数 jenkinsFile 账号  密码 本地svn目录
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";
        String jenkinsPingZheng ="3a9d8f1a-491e-4be2-a55f-b37a04541883";

        //java 执行svn中jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //需要部署的项目svn 地址
        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";


        //jenkins 脚本中 有时候需要执行的ssh命令
        String jenkinsSshUrl="qx18911892395.iego.vip";
        String jenkinsSshName="root";
        String jenkinsSshPwd="sztd1";
        int jenkinsSshPort=25039;

        String jobName = "aabb";
        JobApiHelper jobApi = new JobApiHelper();
        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,jenkinsPingZheng);
        // 脚本需要的参数
        int app_port=7002;
        int app_port_docker=7002;
        String app_jar_name="jeecg-demo.jar";

        // 脚本需要的参数
        /**
         *
         */
        String jenkinsJobXmlBuild = PipineStr105.Pipine_0_svn_build(jenkinsSshUrl,jenkinsSshName,jenkinsSshPwd,jenkinsSshPort,projectSvn,jobName,app_port,app_port_docker,app_jar_name);

        //生成报告后发送的地址
        String sshHtmlUrl="";
        String sshName="";
        String sshPwd="";
        String sshDer="";

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }

    //笔记本VM
    private static void jenkins_ai_auto_103_taishi_project() throws SVNException {
        //参数1：登录jenkins系统 调用jenkins API接口
        JenkinsConnect.JENKINS_URL = "http://535v360l82.51vip.biz";
        JenkinsConnect.JENKINS_USERNAME = "sztd1";
        JenkinsConnect.JENKINS_PASSWORD = "11af75e6387c3e4a089d0b0c8ae0ec4430";
        JobApiHelper jobApi = new JobApiHelper();

        //参数2：指定创建的jenkinsFile文件对应远程的svn 账号  密码 本地svn目录  pipelin远程执行script需要
        String svnLocalhost = "https://39.106.53.5:4431/svn/jenkinsfile/java";
        String svnName = "bjb";
        String svnPwd = "bjb123";
        String filePath = "D:/2021_test/0_jenkinsfile/java";
        String jenkinsPingZheng ="4fac12c0-e6ac-4225-a466-9eb9e7b3ad4e";

        //参数3：java调用svn 执行 jenkinsFile更新和上传
        SVNUtil.SVN_URL = "svn://39.106.53.5:4431/svn/jenkinsfile/java";
        SVNUtil.USER_NAME = svnName;
        SVNUtil.PASSWORD = svnPwd;

        //参数4：设置项目相关参数 **************************************************************
        String jobName = "H_project_taishi";
        //创建jenkinsJob任务  需要的参数； jenkinsXml脚本 、任务名称,[jenkinsFile所在的svn]
        String jenkinsJobXml = PipineStr105.pipelin_jenkinsfile(svnLocalhost+"/"+jobName,jenkinsPingZheng);


        //参数5：需要部署的项目svn 地址--------------------------------------------------------------
        String projectSvn = "https://39.106.53.5:4431/svn/2022_htsy_haijun_jdyjbz/spring-boot-hello";
//        String projectSvn = "https://39.106.53.5:4431/svn/2022-htsy-ai-test/auto_ui_test_java_web";
        String credentialsId="4fac12c0-e6ac-4225-a466-9eb9e7b3ad4e";
        String app_jar_name="target/spring-boot-hello-1.0.jar";

//        String projectSvn = "https://39.106.53.5:4431/svn/2022_zhjy_6/2022_zhjy_max_data/jeecg-boot/jeecg-boot";
//        String credentialsId="4fac12c0-e6ac-4225-a466-9eb9e7b3ad4e";
//        String app_jar_name="jeecg-boot-module-system/target/jeecg-boot-module-system-3.1.0.jar";

        //参数6：jenkins里面无法执行ssh docker命令  需要ss后执行
        int app_port=7071;  //docker 容器对外访问端口
        int app_port_docker=7070; //docker容器内部端口
        String app_v="1.0";

        //参数7：可以在流水线中执行 docker命令
        String sshUrl="qx18911892395.iego.vip";
        int sshPort=25039;
        String sshName="root";
        String sshPwd="sztd1";
        String jenkinsCallBack = "";

        //开始执行
        String get_pipine_docker_ssh= PipineStr105.get_pipine_docker_ssh(sshUrl,sshPort,sshName,sshPwd);
        String get_pipine_svn_checkout_code= PipineStr105.get_pipine_svn_checkout_code(projectSvn,credentialsId);
        String get_pipine_test_script_sonarqube= PipineStr105.get_pipine_test_script_sonarqube(jobName,app_v);
        String get_pipine_build_project= PipineStr105.get_pipine_build_project();
        String set_pipine_docker_dockerfile_image_run= PipineStr105.set_pipine_jar_docker_dockerfile_image_run(jobName,jobName,app_v,jobName,app_port,app_port_docker,app_jar_name);

        //
        String get_exe_ok_set_pipine_jmeter = PipineStr105.get_exe_ok_set_pipine_jmeter(jobName, PipineStr105.get_jmeter_xml());
        String set_pipeine_end_post= PipineStr105.set_pipeine_end_post(jobName ,jenkinsCallBack, "",get_exe_ok_set_pipine_jmeter,"","");
        String jenkinsJobXmlBuild = PipineStr105.get_pipine_ok(get_pipine_docker_ssh,get_pipine_svn_checkout_code,get_pipine_test_script_sonarqube,get_pipine_build_project,set_pipine_docker_dockerfile_image_run,set_pipeine_end_post);

        //创建jenkinsJob
        createJenkinsJob(jobName, jenkinsJobXml, jobApi);
        //创建jenkinsfile
        createJenkinsFileToSvn(svnLocalhost, jenkinsJobXmlBuild, filePath, jobName);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //执行jenkinsJobBuild
        runJenkinsJobBuild(jobName, jobApi);
    }


    //创建jenkins 任务
    public static void createJenkinsJob(String jobName, String PipelineXml, JobApiHelper jobApi) {
        try {
            //判断是否已经创建任务，匹配重复创建
            JobWithDetails currentJob = jobApi.getJob1(jobName);
            if (currentJob == null) {
                // 第一步： 创建项目
                jobApi.ceateJob(jobName, PipelineXml);
                System.out.println("任务: " + jobName + "创建成功--------------------------------");
            } else if (currentJob.getName().equals(jobName)) {
                System.out.println("任务: " + jobName + " 已经创建，执行删除 重建。。。");
                jobApi.deleteJob(jobName);
                jobApi.ceateJob(jobName, PipelineXml);
            }
            System.out.println("创建任务: " + jobName + "脚本路径： "+PipelineXml);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //创建jenkinsfile上传到svn
    public static void createJenkinsFileToSvn(String svnLocalhost, String jenkinsFileXml, String filePath, String jobName) throws SVNException {
        //创建jenkinsFile 文件到svn目录
        extracted(jenkinsFileXml, filePath + "/" + jobName);

        SVNUtil.authSvn();
        //更新svn到本地
//        SVNUtil.update(filePath, SVNRevision.HEAD, SVNDepth.FILES);
        SVNUtil.addEntry(filePath);
        //commat jenkinsFile文件到svn
        SVNCommitInfo st = SVNUtil.commit(filePath, true, "项目： "+jobName+" : "+System.currentTimeMillis());
        SVNUtil.getLog(svnLocalhost);
        System.out.println("----------创建jenkinsfile上传到svn-----------");
        System.out.println("创建的jenkinsfile: " + jobName + "  本地脚本路径： "+filePath + "/" + jobName);
        System.out.println("创建的jenkinsfile上传到svn : " + jobName + "  svn地址： "+svnLocalhost);
    }

    //创建jenkinsFile 文件到svn目录
    private static void extracted(String jenkinsFileXml, String filePath) {
        //创建jenkinsfile文件
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();// 能创建多级目录
        }
        try {
            file = new File(filePath + "/Jenkinsfile");
            if (file.createNewFile()) {
                System.out.println("文件创建成功！");
            } else {
                System.out.println(filePath + "/Jenkinsfile，该文件已经存在。");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream fileOutputStream;
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(jenkinsFileXml.getBytes("gbk"));
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //执行jenkins 任务
    public static void runJenkinsJobBuild(String jobName, JobApiHelper jobApi) {
        // 构建对应的job
        try {
            System.out.println("***********************开始构建任务: "+ jobApi.getJob1(jobName).getDisplayName() +"  ************************************");
            jobApi.buildJob(jobName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
