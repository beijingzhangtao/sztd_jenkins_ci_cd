package com.hengtiansoft.jenkins.unit;

import com.hengtiansoft.RunApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author zdd_x
 */
public class RunShcommandUnit {

//    public static void main(String[] args) {
//        int port = 8086;
//        boolean portUsing = isPortUsing("127.0.0.1", port);
//        if(portUsing){
//            System.out.println("端口已被占用");
//        }else{
//            System.out.println("端口没有占用");
//        }
//
//        runByshcommand("netstat -anp |grep 6379");//
//    }

    /**
     * 执行命令
     * @param command
     * @return
     */
    public static Boolean runByshcommand(String command) {
        try {
            Process process =null;
            process = Runtime.getRuntime().exec(command);
            //得到终端窗口中输出的信息
            InputStream pro_in=process.getInputStream();
            //得到终端窗口中输出的错误流
            InputStream pro_err=process.getErrorStream();
//            new getOutputStreamThread(pro_in,"NORMAL").run();
//            new getOutputStreamThread(pro_err,"ERROR").run();
            String normal = getOutputStream(pro_in, "NORMAL");
            String error = getOutputStream(pro_err, "ERROR");
            process.waitFor();
            // 销毁子进程
            process.destroy();
            process = null;
            String result = normal + error;
            System.out.println("命令执行成功,结果为:" + normal + error);
            if(StringUtils.isEmpty(result)){
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getOutputStream(InputStream inputStream, String printType){
        String szstr = "";
        String result = "";
        BufferedReader in=new BufferedReader(new InputStreamReader(inputStream));
        try {
            while((szstr=in.readLine())!=null){
                result = result +"\n"+ szstr;
                //System.out.println(printType+":"+szstr);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static class getOutputStreamThread extends Thread{
        InputStream ins;
        String print_type;
        public getOutputStreamThread(InputStream is,String print_type){
            this.ins=is;
            this.print_type=print_type;
        }
        @Override
        public void run(){
            String szstr="";
            BufferedReader in=new BufferedReader(new InputStreamReader(ins));
            try {
                while((szstr=in.readLine())!=null){
                    System.out.println(print_type+":"+szstr);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    /**
     * 	检测某个端口是否占用
     * @param host  IP
     * @param port  端口号
     * @return  如果占用返回true；否则返回false
     * @throws UnknownHostException    IP地址不通或错误，则会抛出此异常
     */
    public static boolean isPortUsing(String host, int port) {
        boolean flag = false;
        try{
            InetAddress theAddress = InetAddress.getByName(host);
            Socket socket = new Socket(theAddress, port);
            flag = true;
        } catch (IOException e) {

        }
        return flag;
    }

}
