package com.hengtiansoft.jenkins.unit;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 处理自定义异常
     *
     */
    @ExceptionHandler(value = IOException.class)
    public BaseRespResult ioeExceptionHandler(IOException e) {
        return BaseRespResult.errorResult("网络似乎开小差了...");
    }

    /**
     * 处理自定义异常
     *
     */
    @ExceptionHandler(value = JobBuildLogException.class)
    public BaseRespResult jobBuildLogHandler(JobBuildLogException e) {
        return BaseRespResult.errorResult(e.getMessage());

    }

}
