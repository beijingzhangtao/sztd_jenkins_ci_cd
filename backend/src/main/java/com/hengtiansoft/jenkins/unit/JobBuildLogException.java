package com.hengtiansoft.jenkins.unit;

public class JobBuildLogException extends SpringBootPlusException{

    public JobBuildLogException(String message) {
        super(message);
    }
    public JobBuildLogException(Integer errorCode, String message) {
        super(errorCode,message);
    }
}
