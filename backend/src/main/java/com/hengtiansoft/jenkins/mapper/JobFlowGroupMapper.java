package com.hengtiansoft.jenkins.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hengtiansoft.jenkins.model.JobFlowGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zdd_x
 */
@Mapper
public interface JobFlowGroupMapper extends BaseMapper<JobFlowGroup> {

    /**
     * 获取全部分组
     * @return
     */
    List<JobFlowGroup> selectAll();

}
