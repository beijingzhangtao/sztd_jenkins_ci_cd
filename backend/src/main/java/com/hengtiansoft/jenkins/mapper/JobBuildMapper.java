package com.hengtiansoft.jenkins.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hengtiansoft.jenkins.model.JobBuildModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zdd_x
 */
@Mapper
public interface JobBuildMapper extends BaseMapper<JobBuildModel> {

    /**
     * 查询最近运行任务
     * @param jobId
     * @return
     */
    JobBuildModel getLastJob(@Param("jobId") String jobId);

    /**
     * 查询任务运行历史
     * @param jobId
     * @return
     */
    List<JobBuildModel> getHistoryJobList(@Param("jobId") String jobId);

    /**
     * 插入构建任务
     * @param jobBuildModel
     */
    void insertJobBuild(JobBuildModel jobBuildModel);
}
