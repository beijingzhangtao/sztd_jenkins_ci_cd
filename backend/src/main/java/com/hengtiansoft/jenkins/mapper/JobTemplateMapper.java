package com.hengtiansoft.jenkins.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hengtiansoft.jenkins.model.JobTemplateModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zdd_x
 */
@Mapper
public interface JobTemplateMapper  extends BaseMapper<JobTemplateModel> {

    /**
     * 获取模板信息
     * @param templateId 模板id
     * @return
     */
    List<JobTemplateModel> gettemplateList(@Param("templateId") int templateId);
}
