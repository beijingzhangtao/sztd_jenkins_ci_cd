package com.hengtiansoft.jenkins.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.model.JobSetting;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zdd_x
 */
@Mapper
public interface JobSettingMapper extends BaseMapper<JobSetting> {

    /**
     * 获取所欲配置列表
     * @return
     * @param producePage
     */
    List<JobSetting> getList(Page<JobSetting> producePage);
}
