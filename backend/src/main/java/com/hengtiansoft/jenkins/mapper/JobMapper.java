package com.hengtiansoft.jenkins.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.model.JobModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * job mapper
 * @author zdd_x
 */
@Mapper
public interface JobMapper extends BaseMapper<JobModel> {

    /**
     * 获取任务分组列表
     * @return
     */
    List<JobModel> getJobGroupList();

    /**
     * 查询不同分组的任务
     * @param groupId
     * @return
     */
    List<JobModel>  selectByGoupId(@Param("groupId")int groupId);

    /**
     * 查询流水线列表
     * @param producePage
     * @param scope
     * @return
     */
    List<JobModel> getJobListByScope(Page<JobModel> producePage, @Param("scope")String scope, @Param("creader")String creader, @Param("sort")String sort);

    /**
     * 获取流水线id
     * @param jobId
     * @return
     */
    JobModel getJob(@Param("jobId")String jobId);

    /**
     * 检查是否有job重复
     * @param jobName
     * @return
     */
    JobModel checkJobName(@Param("jobName")String jobName);

    /**
     * 修改流水线状态
     * @param updateJob
     */
    void updateJob(JobModel updateJob);

    /**
     * 收藏流水线
     * @param jobModel
     */
    void collectJob(JobModel jobModel);
}
