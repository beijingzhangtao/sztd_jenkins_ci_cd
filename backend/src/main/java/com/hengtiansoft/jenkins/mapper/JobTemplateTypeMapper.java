package com.hengtiansoft.jenkins.mapper;

import com.hengtiansoft.jenkins.model.JobTemplateType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zdd_x
 */
@Mapper
public interface JobTemplateTypeMapper {

    /**
     * 获取所有模板
     * @return
     */
    List<JobTemplateType> gettemplateList();
}
