package com.hengtiansoft.jenkins.jenkins;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * 连接 Jenkins
 *
 * @author zdd_x
 */
@Component
@Order(0)
public class JenkinsConnect {


    private JenkinsConnect(){}

//    /**
//     * jenkins 服务地址
//     */
//    @Value("${system.jeninks.url}")
//    private String JENKINS_URL;
//
//    /**
//     * jenkins 登录用户名
//     */
//    @Value("${system.jeninks.name}")
//    private String JENKINS_USERNAME;
//
//    /**
//     * jenkins 登录密码或token
//     */
//    @Value("${system.jeninks.passwrod}")
//    private String JENKINS_PASSWORD;
    public static String JENKINS_URL;
    public static String JENKINS_USERNAME;
    public static String JENKINS_PASSWORD;
    @Autowired(required = false)
    @Value(value="${jenkins.url}")
    public void setJenkinsUrl( String JENKINS_URL) {
        JenkinsConnect.JENKINS_URL = JENKINS_URL;
    }
    @Autowired(required = false)
    @Value(value="${jenkins.username}")
    public void setJenkinsUsername( String JENKINS_USERNAME) {
        JenkinsConnect.JENKINS_USERNAME = JENKINS_USERNAME;
    }
    @Autowired(required = false)
    @Value(value="${jenkins.passwrod}")
    public void setJenkinsPassword(String JENKINS_PASSWORD) {
        JenkinsConnect.JENKINS_PASSWORD = JENKINS_PASSWORD;
    }

    public  String getJenkinsUrl() {
        return JENKINS_URL;
    }


    public  String getJenkinsUsername() {
        return JENKINS_USERNAME;
    }

    public  String getJenkinsPassword() {
        return JENKINS_PASSWORD;
    }
    // 连接 Jenkins 需要设置的信息  办公室台式机主机
//    public static  String JENKINS_URL = "http://4591s5s609.qicp.vip";
//    public static  String JENKINS_USERNAME = "sztd1";
//    public static  String JENKINS_PASSWORD = "1195aa1de0d0433e369515bd794b51d97c";

    // 连接 Jenkins 需要设置的信息  办公室笔记本主机
//    public static  String JENKINS_URL = "http://535v360l82.51vip.biz:53580/";
//    public static  String JENKINS_USERNAME = "sztd1";
//    public static  String JENKINS_PASSWORD = "1191a8fa2b22d874e3c427cce0b68eac1e";

    // 连接 Jenkins 需要设置的信息  办公室笔记本主机105
//    public static  String JENKINS_URL = "http://192.168.1.100:10240/";
//    public static  String JENKINS_USERNAME = "admin";
//    public static  String JENKINS_PASSWORD = "11043bfb7f23257a83424cec5d8aa06711";

    /**
     * Http 客户端工具
     *
     * 如果有些 API 该Jar工具包未提供，可以用此Http客户端操作远程接口，执行命令
     * @return
     */
    public static JenkinsHttpClient getClient(){
        JenkinsHttpClient jenkinsHttpClient = null;
        try {
            jenkinsHttpClient = new JenkinsHttpClient(new URI(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return jenkinsHttpClient;
    }

    /**
     * 连接 Jenkins
     */
    public static JenkinsServer connection() {
        JenkinsServer jenkinsServer = null;
        try {
            jenkinsServer = new JenkinsServer(new URI(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return jenkinsServer;
    }
}
