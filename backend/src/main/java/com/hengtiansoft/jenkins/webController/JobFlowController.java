package com.hengtiansoft.jenkins.webController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.jenkinsApi.AutoJenkinsPipeline_centos7;
import com.hengtiansoft.jenkins.model.*;
import com.hengtiansoft.jenkins.service.*;
import com.hengtiansoft.jenkins.unit.GetIpAndPort;
import com.hengtiansoft.jenkins.unit.JobBuildLogException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.tmatesoft.svn.core.SVNException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

/**
 * 首页接口
 * @author zdd_x
 */
@RestController
@RequestMapping("/web/job")
public class JobFlowController {

    @Autowired
    private JobService jobService;

    @Autowired
    private JobBuildService jobBuildService;

    @Autowired
    private JobFlowGroupService jobFlowGroupService;

    @Autowired
    private JobTemplateService jobTemplateService;

    @Autowired
    private JobSettingService jobSettingService;


    @RequestMapping("/test")
    public BaseRespResult test() {
        //返回信息
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("ResrecordNo", "1001");
        resultMap.put("AddcardState", "true");
        resultMap.put("AddfaceState", "true");
        return BaseRespResult.successResult(resultMap);
    }

    /**
     * 获取任务分组列表
     */
    @RequestMapping(value = "/getJobGroupList", method = RequestMethod.POST)
    public BaseRespResult getJobGroupList(){
        Map<String, List<JobModel>> jobList = jobService.getJobGroupList();
        return BaseRespResult.successResult(jobList);
    }

    /**
     * 获取全部分组列表
     */
    @RequestMapping(value = "/getGroupList", method = RequestMethod.POST)
    public BaseRespResult getGroupList(){
        List<JobFlowGroup> jobList = jobFlowGroupService.getGroupList();
        return BaseRespResult.successResult(jobList);
    }

    /**
     * 获取任务列表
     */
    @RequestMapping(value = "/getJobList", method = RequestMethod.POST)
    public BaseRespResult getJobList(@RequestBody JobModel jobModel){
        Page<JobModel> producePage = new Page<>(jobModel.getPage(),jobModel.getRow());
        List<JobModel> jobList = new ArrayList<>();
        //TODO  后期要改 先写死
        jobModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
        try {
            jobList = jobService.getJobList(producePage, jobModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("data", jobList);
        resultMap.put("total", producePage.getTotal());
        return BaseRespResult.successResult(resultMap);
    }

    /**
     * 收藏任务
     */
    @RequestMapping(value = "/collectJob", method = RequestMethod.POST)
    public BaseRespResult collectJob(@RequestBody JobModel jobModel){
        jobService.collectJob(jobModel);
        return BaseRespResult.successResult();
    }


    /**
     * 最近运行任务
     */
    @RequestMapping(value = "/getLastJob", method = RequestMethod.POST)
    public BaseRespResult getLastJob(@RequestBody JobBuildModel jobBuildModel){
        JobBuildModel resultModel = jobBuildService.getLastJob(jobBuildModel);
        return BaseRespResult.successResult(resultModel);
    }


    /**
     * 任务运行历史
     */
    @RequestMapping(value = "/getHistoryJobList", method = RequestMethod.POST)
    public BaseRespResult getHistoryJobList(@RequestBody JobBuildModel jobBuildModel){
        List<JobBuildModel> resultList = jobBuildService.getHistoryJobList(jobBuildModel);
        return BaseRespResult.successResult(resultList);
    }


    /**
     * 查询模板信息
     */
    @RequestMapping(value = "/gettemplateList", method = RequestMethod.POST)
    public BaseRespResult gettemplateList(){
        Map<String, List<JobTemplateModel>> resultMap = jobTemplateService.gettemplateList();
        return BaseRespResult.successResult(resultMap);
    }


    /**
     * 查询流水线信息
     */
    @RequestMapping(value = "/getJob", method = RequestMethod.POST)
    public BaseRespResult getJob(@RequestBody JobModel jobModel){
        JobModel jobModelDB = new JobModel();
        try {
            jobModelDB = jobService.getJob(jobModel);
            JobTemplateModel jobTemplateModel = jobTemplateService.selectOne(jobModelDB.getTemplateId());
            jobModelDB.setJobTemplateModel(jobTemplateModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult(jobModelDB);
    }


    /**
     * 创建job 任务
     */
    @RequestMapping(value = "/ceateJob", method = RequestMethod.POST)
    public BaseRespResult ceateJob(@RequestBody JobModel jobModel){
        boolean ceateStatus = jobService.checkJobName(jobModel);
        if(!ceateStatus){
            return BaseRespResult.errorResult("流水线名称重复！");
        }
        try {
            jobService.ceateJob(jobModel);
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult();
    }

    /**
     * 更新job 任务
     */
    @RequestMapping(value = "/updateJob", method = RequestMethod.POST)
    public BaseRespResult updateJob(@RequestBody JobModel jobModel){
        if(StringUtils.isEmpty(jobModel.getJobName())){
            return BaseRespResult.errorResult("Job 名称为空,请检查...");
        }

        try {
            jobService.updateJob(jobModel);
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 构建流水线
     */
    @RequestMapping(value = "/buildJob", method = RequestMethod.POST)
    public BaseRespResult buildJob(@RequestBody JobModel jobModel){
        try {
            JobSetting jobSetting = jobSettingService.getOne(jobModel.getSettingId());
            //设置回调地址
            jobSetting.setJenkinsCallBack(GetIpAndPort.getLocalUrl()+"/web/job/setJobBuildState");
            String result = AutoJenkinsPipeline_centos7.extractedApi(jobSetting.getType(),
                    jobSetting.getExeTypeJar(),
                    jobSetting.getExeTypeWar(),
                    jobSetting.getSvnLocalhost(),
                    jobSetting.getFilePath(),
                    jobSetting.getProjectUrlWar(),
                    jobModel.getJobName(),
                    jobSetting.getSvnUrl(),
                    jobSetting.getSvnCredentialsId(),
                    jobSetting.getProjectUrlJar(),
                    jobSetting.getDockerPort(),
                    jobSetting.getProjectPort(),
                    jobSetting.getProjectVersion(),
                    jobSetting.getSshUrl(),
                    jobSetting.getSshPort(),
                    jobSetting.getSshName(),
                    jobSetting.getSshPwd(),
                    jobSetting.getDockerId(),
                    jobSetting.getJenkinsCallBack());

        } catch (SVNException e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult();
    }


    /**
     * 删除流水线
     */
    @RequestMapping(value = "/deleteJob", method = RequestMethod.POST)
    public BaseRespResult deleteJob(@RequestBody JobModel jobModel){
        try {
            jobService.deleteJob(jobModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult();
    }


    /**
     * 获取当前日志，构建中
     */
    @RequestMapping(value = "/getCurrentLog", method = RequestMethod.POST)
    public BaseRespResult getCurrentLog(@RequestBody JobModel jobModel){
        try {
            String currentLog = jobService.getCurrentLog(jobModel);
            return BaseRespResult.successResult(currentLog);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult();
    }


    /**
     * 获取当前日志
     */
    @RequestMapping(value = "/getJobBuildLog", method = RequestMethod.POST)
    public BaseRespResult getJobBuildLog(@RequestBody JobModel jobModel) throws JobBuildLogException, IOException {
        String currentLog = jobService.getJobBuildLog(jobModel);
        return BaseRespResult.successResult(currentLog);
    }

    /**
     * 回调接口入库
     * @param jobModel
     * @return
     */
    @RequestMapping(value = "/setJobBuildState", method = RequestMethod.POST)
    public BaseRespResult setJobBuildState(@RequestBody JobModel jobModel){
        try {
            System.err.println("回调成功！1111111111111111111111111111111");
            jobBuildService.buildJob(jobModel);
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.successResult();
    }




}
