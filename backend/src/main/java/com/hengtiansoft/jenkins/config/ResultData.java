package com.hengtiansoft.jenkins.config;

public class ResultData {
    /**
     * 构造方法
     * @param code
     * @param message
     * @param data
     */
    public ResultData(Boolean success,Integer code, String message, Object data) {
        this.success=success;
        this.code = code;
        this.message = message;
        this.data = data;
        System.out.println(data);
    }
    /**
     * 返回结果编码
     */
    private Boolean success;


    /**
     * 返回结果编码
     */
    private Integer code;
    /**
     * 返回状态信息
     */
    private String message;
    /**
     * 返回结果数据
     */
    private Object data;

}
