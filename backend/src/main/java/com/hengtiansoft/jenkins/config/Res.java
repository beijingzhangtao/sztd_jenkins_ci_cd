package com.hengtiansoft.jenkins.config;

import java.util.Map;

public class Res {

    /**
     * 结果返回
     * @param object
     * @return
     */
    //执行失败
    public static ResultData error(String msg,Object object)
    {
        return new ResultData(false,500,msg,object);
    }

    //执行成功
    public static ResultData sucuss(String msg,Object object)
    {
        return new ResultData(true,200,msg,object);
    }
}
