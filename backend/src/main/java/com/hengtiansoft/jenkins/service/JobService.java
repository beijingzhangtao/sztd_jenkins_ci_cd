package com.hengtiansoft.jenkins.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.model.JobModel;
import com.hengtiansoft.jenkins.unit.JobBuildLogException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author zdd_x
 */
public interface JobService {

    /**
     * 检查job 是否创建
     * @param jobName
     * @return
     */
    boolean createdCheckJobName(String jobName) throws IOException;


    /**
     * 在数据库创建job信息
     * @param jobModel
     */
    void insertJob(JobModel jobModel) throws IOException;

    /**
     * 查询job
     * @param jobName
     * @return
     */
    JobModel selectJobByName(String jobName);

    //首页接口 ---------------------------------------------------------------

    /**
     * 首页任务分组接口
     * @return
     */
    Map<String, List<JobModel>> getJobGroupList();

    /**
     * 查询流水线列表
     *
     * @param producePage
     * @return
     */
    List<JobModel> getJobList(Page<JobModel> producePage, JobModel jobModel) throws IOException;

    /**
     * 获取流水线信息
     * @param jobModel
     * @return
     */
    JobModel getJob(JobModel jobModel) throws IOException;

    /**
     * 检查是否有job重复
     * @param jobModel
     * @return
     */
    boolean checkJobName(JobModel jobModel);

    /**
     * 创建任务
     * @param jobModel
     */
    void ceateJob(JobModel jobModel) throws IOException;

    /**
     * 删除流水线
     * @param jobModel
     */
    void deleteJob(JobModel jobModel) throws IOException;

    /**
     * 收藏任务
     * @param jobModel
     */
    void collectJob(JobModel jobModel);

    /**
     * 查询当前构建日志
     * @param jobModel
     * @return
     */
    String getCurrentLog(JobModel jobModel) throws IOException;

    /**
     * 获取当前日志
     * @param jobModel
     * @return
     */
    String getJobBuildLog(JobModel jobModel) throws IOException, JobBuildLogException;

    /**
     *x 修改流水线
     * @param jobModel
     */
    void updateJob(JobModel jobModel);
}
