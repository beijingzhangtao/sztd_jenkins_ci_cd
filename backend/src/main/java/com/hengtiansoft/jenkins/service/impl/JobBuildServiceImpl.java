package com.hengtiansoft.jenkins.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.jenkinsApi.JobApiHelper;
import com.hengtiansoft.jenkins.jenkinsApi.JobBuildApiHelper;
import com.hengtiansoft.jenkins.mapper.JobBuildMapper;
import com.hengtiansoft.jenkins.mapper.JobMapper;
import com.hengtiansoft.jenkins.model.JobBuildModel;
import com.hengtiansoft.jenkins.model.JobModel;
import com.hengtiansoft.jenkins.model.jobEnum.TriggerType;
import com.hengtiansoft.jenkins.service.JobBuildService;
import com.offbytwo.jenkins.model.BuildWithDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

/**
 * @author zdd_x
 */
@Service
public class JobBuildServiceImpl implements JobBuildService {

    @Autowired
    private JobBuildMapper jobBuildMapper;

    @Autowired
    private JobApiHelper jobApiHelper;

    @Autowired
    private JobBuildApiHelper jobBuildApiHelper;

    @Autowired
    private JobMapper jobMapper;

    /**
     * 查看最近运行的任务
     * @param jobBuildModel
     * @return
     */
    @Override
    public JobBuildModel getLastJob(JobBuildModel jobBuildModel) {
        JobBuildModel jobBuildModelDB = jobBuildMapper.getLastJob(jobBuildModel.getJobId());
        return jobBuildModelDB;
    }

    /**
     * 查询任务运行历史
     * @param jobBuildModel
     * @return
     */
    @Override
    public List<JobBuildModel> getHistoryJobList(JobBuildModel jobBuildModel) {
        List<JobBuildModel> resultList = jobBuildMapper.getHistoryJobList(jobBuildModel.getJobId());
        return resultList;
    }

    /**
     * 构建流水线
     * @param jobModel
     */
    @Override
    public void buildJob(JobModel jobModel) throws IOException {
        //jobApiHelper.buildJob(jobModel.getJobName());

        BuildWithDetails buildInfo = jobBuildApiHelper.getJobBuildDetailInfo(jobModel.getJobName(), "last");

        JobModel jobModelDB = jobMapper.checkJobName(jobModel.getJobName());

        // 当前日志
        //ConsoleLog currentLog = buildInfo.getConsoleOutputText(0);

        JobBuildModel jobBuildModel = new JobBuildModel();
        // 构建id
        jobBuildModel.setBuildId(UUID.randomUUID().toString());
        // 构建状态
        jobBuildModel.setStatus(buildInfo.getResult().name());
        // 流水线id
        jobBuildModel.setJobId(jobModelDB.getJobId());
        // 持续时间
        jobBuildModel.setKeeptime(Integer.valueOf(String.valueOf(buildInfo.getDuration())));
        // 构建时间(ms)
        jobBuildModel.setTimestamp(new Timestamp(buildInfo.getTimestamp()));
        // 构建备注
        jobBuildModel.setRemake(buildInfo.getCauses().toString());
        // 构建日志
        jobBuildModel.setLog(buildInfo.getConsoleOutputText());
        // 构建编号
        jobBuildModel.setNumber(String.valueOf(buildInfo.getNumber()));
        // 构建人
        jobBuildModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
        // 日志类型
        jobBuildModel.setLogFormat("text");
        // 触发方式
        jobBuildModel.setTriggers(jobBuildModel.getCreader() + TriggerType.MANUAL.getName());

//        UpdateWrapper<JobBuildModel> qw = new UpdateWrapper<>();
//        qw.eq("build_id", jobModel.getJobId());
//        jobBuildMapper.update(jobBuildModel,qw);
        jobBuildMapper.insert(jobBuildModel);

        //修改流水线状态
        //TODO  需要加8小时时间不准
        JobModel updateJob = new JobModel();
        updateJob.setJobId(jobBuildModel.getJobId());
        updateJob.setStatus(jobBuildModel.getStatus());
        updateJob.setTimestamp(jobBuildModel.getTimestamp());
        jobMapper.updateJob(updateJob);
    }
}
