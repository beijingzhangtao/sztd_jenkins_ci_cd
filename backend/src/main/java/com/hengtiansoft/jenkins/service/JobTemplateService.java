package com.hengtiansoft.jenkins.service;

import com.hengtiansoft.jenkins.model.JobTemplateModel;

import java.util.List;
import java.util.Map;

/**
 * @author zdd_x
 */
public interface JobTemplateService {

    /**
     * 获取模板信息
     * @return
     */
    Map<String, List<JobTemplateModel>> gettemplateList();

    /**
     * 获取单个模板信息
     * @param templateId
     * @return
     */
    JobTemplateModel selectOne(String templateId);
}
