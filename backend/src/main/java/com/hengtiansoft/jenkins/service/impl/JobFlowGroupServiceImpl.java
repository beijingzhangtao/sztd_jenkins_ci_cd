package com.hengtiansoft.jenkins.service.impl;

import com.hengtiansoft.jenkins.mapper.JobFlowGroupMapper;
import com.hengtiansoft.jenkins.model.JobFlowGroup;
import com.hengtiansoft.jenkins.service.JobFlowGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 流水线分组实现类
 * @author zdd_x
 */
@Service
public class JobFlowGroupServiceImpl implements JobFlowGroupService {

    @Autowired
    private JobFlowGroupMapper jobFlowGroupMapper;

    @Override
    public List<JobFlowGroup> getGroupList() {
        return jobFlowGroupMapper.selectAll();
    }
}
