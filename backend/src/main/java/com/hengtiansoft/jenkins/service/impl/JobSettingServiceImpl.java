package com.hengtiansoft.jenkins.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.mapper.JobSettingMapper;
import com.hengtiansoft.jenkins.model.JobSetting;
import com.hengtiansoft.jenkins.service.JobSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author zdd_x
 */
@Service
public class JobSettingServiceImpl implements JobSettingService {

    @Autowired
    private JobSettingMapper jobSettingMapper;


    /**
     * 获取所欲配置列表
     * @return
     * @param producePage
     */
    @Override
    public List<JobSetting> getList(Page<JobSetting> producePage) {
        return jobSettingMapper.getList(producePage);
    }

    /**
     * 新增配置
     * @param jobSetting
     */
    @Override
    public void addSetting(JobSetting jobSetting) {
        jobSetting.setId(UUID.randomUUID().toString());
        jobSetting.setCreateTime(new Timestamp(System.currentTimeMillis()));
        jobSettingMapper.insert(jobSetting);
    }

    /**
     * 获取任务配置
     * @param id
     * @return
     */
    @Override
    public JobSetting getOne(String id) {
        QueryWrapper<JobSetting> wrapper = Wrappers.query();
        wrapper.eq("id",id);
        return jobSettingMapper.selectOne(wrapper);
    }

    /**
     *
     * @param jobSetting
     */
    @Override
    public void updateSetting(JobSetting jobSetting) {
        jobSettingMapper.updateById(jobSetting);
    }

    /**
     * 删除任务配置
     * @param jobSetting
     */
    @Override
    public void deteleSetting(JobSetting jobSetting) {
        jobSettingMapper.deleteById(jobSetting);
    }

    /**
     * 返回所有配置
     * @return
     */
    @Override
    public List<JobSetting> getListAll() {
        return jobSettingMapper.selectList(null);
    }
}
