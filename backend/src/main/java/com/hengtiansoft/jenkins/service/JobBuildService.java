package com.hengtiansoft.jenkins.service;

import com.hengtiansoft.jenkins.model.JobBuildModel;
import com.hengtiansoft.jenkins.model.JobModel;

import java.io.IOException;
import java.util.List;

/**
 * @author zdd_x
 */
public interface JobBuildService {

    /**
     * 查看最近运行的任务
     * @param jobBuildModel
     * @return
     */
    JobBuildModel getLastJob(JobBuildModel jobBuildModel);

    /**
     * 查询任务运行历史
     * @param jobBuildModel
     * @return
     */
    List<JobBuildModel> getHistoryJobList(JobBuildModel jobBuildModel);

    /**
     * 构建流水线
     * @param jobModel
     */
    void buildJob(JobModel jobModel) throws IOException;

}
