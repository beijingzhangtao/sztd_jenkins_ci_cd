package com.hengtiansoft.jenkins.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.hengtiansoft.jenkins.mapper.JobTemplateMapper;
import com.hengtiansoft.jenkins.mapper.JobTemplateTypeMapper;
import com.hengtiansoft.jenkins.model.JobSetting;
import com.hengtiansoft.jenkins.model.JobTemplateModel;
import com.hengtiansoft.jenkins.model.JobTemplateType;
import com.hengtiansoft.jenkins.service.JobTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zdd_x
 */
@Service
public class JobTemplateServiceImpl implements JobTemplateService {

    @Autowired
    private JobTemplateMapper jobTemplateMapper;

    @Autowired
    private JobTemplateTypeMapper jobTemplateTypeMapper;

    /**
     * 查询任务模板信息
     * @return
     */
    @Override
    public Map<String, List<JobTemplateModel>> gettemplateList() {

        List<JobTemplateType> jobTemplateTypeList =  jobTemplateTypeMapper.gettemplateList();

        Map<String, List<JobTemplateModel>> resultMap = new LinkedHashMap<>(jobTemplateTypeList.size());

        for (JobTemplateType templateType:jobTemplateTypeList) {
            List<JobTemplateModel> jobTemplateModels = jobTemplateMapper.gettemplateList(templateType.getId());
            resultMap.put(templateType.getName(),jobTemplateModels);
        }
        return resultMap;
    }

    /**
     * 获取单个模板信息
     * @param templateId
     * @return
     */
    @Override
    public JobTemplateModel selectOne(String templateId) {
        QueryWrapper<JobTemplateModel> wrapper = Wrappers.query();
        wrapper.eq("id",templateId);
        return jobTemplateMapper.selectOne(wrapper);
    }
}
