package com.hengtiansoft.jenkins.service;

import com.hengtiansoft.jenkins.model.JobFlowGroup;

import java.util.List;

/**
 * @author zdd_x
 */
public interface JobFlowGroupService {

    /**
     * 查询全部分组信息
     * @return
     */
    List<JobFlowGroup> getGroupList();
}
