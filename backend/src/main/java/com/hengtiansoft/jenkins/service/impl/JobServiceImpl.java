package com.hengtiansoft.jenkins.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.jenkinsApi.JobApiHelper;
import com.hengtiansoft.jenkins.jenkinsApi.JobBuildApiHelper;
import com.hengtiansoft.jenkins.mapper.JobBuildMapper;
import com.hengtiansoft.jenkins.mapper.JobFlowGroupMapper;
import com.hengtiansoft.jenkins.mapper.JobMapper;
import com.hengtiansoft.jenkins.model.JobBuildModel;
import com.hengtiansoft.jenkins.model.JobFlowGroup;
import com.hengtiansoft.jenkins.model.JobModel;
import com.hengtiansoft.jenkins.model.jobEnum.TriggerType;
import com.hengtiansoft.jenkins.service.JobService;
import com.hengtiansoft.jenkins.unit.JobBuildLogException;
import com.offbytwo.jenkins.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * 任务相关业务类
 * @author zdd_x
 */
@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private JobApiHelper jobApiHelper;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private JobFlowGroupMapper jobFlowGroupMapper;

    @Autowired
    private JobBuildApiHelper jobBuildApiHelper;

    @Autowired
    private JobBuildMapper jobBuildMapper;

    /**
     * 创建任务时，检查任务是否已创建
     * @param jobName   任务名称
     * @return 是否创建
     */
    @Override
    public boolean createdCheckJobName(String jobName) throws IOException {
        //连接jenkins服务，查询所有job
        Map<String, Job> jobList = jobApiHelper.getJobList();
        Collection<Job> jobInfoList = jobList.values();

        //遍历所有job 信息，对比jobName 判断是否已创建
        Iterator<Job> item = jobInfoList.iterator();
        while(item.hasNext()){
            Job itemJob = (Job)item.next();
            if(itemJob.getName().equals(jobName)){
                return false;
            }
        }
        return true;
    }



    /**
     * 在数据库创建job信息
     * @param jobModel
     */
    @Override
    public void insertJob(JobModel jobModel) throws IOException {
        JobWithDetails job = jobApiHelper.getJob(jobModel.getJobName());
        jobModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
        //jobModel.setCreadTime(System.currentTimeMillis());
        jobModel.setDisplayName(job.getDisplayName());
        jobModel.setJobId(UUID.randomUUID().toString());
        jobModel.setIscollect("0");
        jobMapper.insert(jobModel);
    }

    /**
     * 根据名称拆线呢job信息
     * @param jobName
     * @return
     */
    @Override
    public JobModel selectJobByName(String jobName) {
        return null;
    }

    /**
     * 首页任务分组接口
     * @return
     */
    @Override
    public Map<String, List<JobModel>> getJobGroupList() {
        Map<String, List<JobModel>> resultMap = new HashMap();

        List<JobFlowGroup> jobFlowGroupList = jobFlowGroupMapper.selectAll();
        jobFlowGroupList.forEach(jobFlowGroupModel -> {
            List<JobModel>  jobModelList = jobMapper.selectByGoupId(jobFlowGroupModel.getGroupId());
            resultMap.put(jobFlowGroupModel.getName(), jobModelList);
        });

        return resultMap;
    }

    /**
     * 查询流水线列表
     *
     * @param producePage
     * @return
     */
    @Override
    public List<JobModel> getJobList(Page<JobModel> producePage, JobModel jobModel) throws IOException {
        List<JobModel> resultList = jobMapper.getJobListByScope(producePage, jobModel.getScope(), jobModel.getCreader(),jobModel.getSort());
        Iterator it = resultList.iterator();
        while(it.hasNext()){
            JobModel jobModelItem = (JobModel) it.next();
            JobBuildModel lastJob = jobBuildMapper.getLastJob(jobModelItem.getJobId());
            if(null == lastJob){
//            if(true){
                BuildWithDetails last = jobBuildApiHelper.getJobBuildDetailInfo(jobModelItem.getJobName(), "last");
                if(null == last){
                    continue;
                }

                JobBuildModel jobBuildModel = insertJobBuild(last, jobModelItem.getJobId(), jobModelItem.getJobName());

                jobModelItem.setNumber(jobBuildModel.getNumber());
                jobModelItem.setStatus(jobBuildModel.getStatus());
                jobModelItem.setTimestamp(jobBuildModel.getTimestamp());
                jobModelItem.setTriggers(jobBuildModel.getTriggers());
            }else{
                jobModelItem.setNumber(lastJob.getNumber());
                jobModelItem.setStatus(lastJob.getStatus());
                jobModelItem.setTimestamp(lastJob.getTimestamp());
                jobModelItem.setTriggers(lastJob.getTriggers());
            }
        }
        return resultList;
    }

    /**
     * 插入自动构建的jobBild信息
     * @param last
     * @param jobId
     * @throws IOException
     */
    private JobBuildModel insertJobBuild(BuildWithDetails last, String jobId, String jobName) throws IOException {
        JobBuildModel jobBuildModel = new JobBuildModel();
        // 当前日志
        //ConsoleLog currentLog = last.getConsoleOutputText(0);
        // 构建id
        jobBuildModel.setBuildId(UUID.randomUUID().toString());
        // 构建状态
        jobBuildModel.setStatus(last.getResult().name().toString());
        // 流水线id
        jobBuildModel.setJobId(jobId);
        // 流水线名称
        jobBuildModel.setJobName(jobName);
        // 持续时间
        jobBuildModel.setKeeptime(Integer.valueOf(String.valueOf(last.getDuration())));
        // 构建时间(ms)
        long timestamp = last.getTimestamp();
        if(timestamp<=0){
            jobBuildModel.setTimestamp(new Timestamp(System.currentTimeMillis()));
        }else{
            jobBuildModel.setTimestamp(new Timestamp(timestamp));
        }

        // 构建备注
        jobBuildModel.setRemake(last.getCauses().toString());
        // 构建日志
        //jobBuildModel.setLog(last.getConsoleOutputText());
        // 构建编号
        jobBuildModel.setNumber(String.valueOf(last.getNumber()));
        // 构建人
        jobBuildModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
        // 日志类型
        jobBuildModel.setLogFormat("text");
        // 触发方式
        List<BuildCause> causes = last.getCauses();
        if(null == causes || causes.size() <= 0){
            jobBuildModel.setTriggers(JenkinsConnect.JENKINS_USERNAME + TriggerType.AUTOMATIC.getName());
        }else{
            jobBuildModel.setTriggers(causes.get(0).getUserId() + TriggerType.AUTOMATIC.getName());
        }
        //jobBuildMapper.insertJobBuild(jobBuildModel);
        jobBuildMapper.insert(jobBuildModel);
        return jobBuildModel;
    }

    /**
     * 获取流水线信息
     * @param jobModel
     * @return
     */
    @Override
    public JobModel getJob(JobModel jobModel) throws IOException {
        JobModel job = jobMapper.getJob(jobModel.getJobId());
        JobBuildModel lastJob = jobBuildMapper.getLastJob(job.getJobId());
        //if(null != lastJob){
        BuildWithDetails last = jobBuildApiHelper.getJobBuildDetailInfo(job.getJobName(), "last");
        JobBuildModel jobBuildModel = insertJobBuild(last, job.getJobId(), job.getJobName());
        job.setNumber(jobBuildModel.getNumber());
        job.setStatus(jobBuildModel.getStatus());
        job.setTimestamp(jobBuildModel.getTimestamp());
        job.setTriggers(jobBuildModel.getTriggers());
        return job;
    }

    /**
     * 检查是否有job重复
     * @param jobModel
     * @return
     */
    @Override
    public boolean checkJobName(JobModel jobModel) {
        JobModel jobModelDB = jobMapper.checkJobName(jobModel.getJobName());
        if(null == jobModelDB){
            return true;
        }
        return false;
    }

    /**
     * 创建任务
     * @param jobModel
     */
    @Override
    public void ceateJob(JobModel jobModel) throws IOException {

        jobApiHelper.ceateJob(jobModel.getJobName(), jobModel.getPipeline());

        JobWithDetails job = jobApiHelper.getJob(jobModel.getJobName());

        jobModel.setJobId(UUID.randomUUID().toString());
        jobModel.setCreadTime(new Timestamp(System.currentTimeMillis()));
        jobModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
        jobModel.setDisplayName(job.getDisplayName());
        jobModel.setDescription(job.getDescription());
        jobModel.setTimestamp(null);
        jobModel.setStatus(TriggerType.NOTBUILT.getName());
        jobMapper.insert(jobModel);
    }

    /**
     * 删除流水线
     * @param jobModel
     */
    @Override
    public void deleteJob(JobModel jobModel) throws IOException {
        jobMapper.deleteById(jobModel);
        jobApiHelper.deleteJob(jobModel.getJobName());
    }

    /**
     * 收藏任务
     * @param jobModel
     */
    @Override
    public void collectJob(JobModel jobModel) {
        jobMapper.collectJob(jobModel);
    }

    /**
     * 获取当前构建中日志
     * @param jobModel
     * @return
     * @throws IOException
     */
    @Override
    public String getCurrentLog(JobModel jobModel) throws IOException {
        return jobBuildApiHelper.getBuildActiveLog(jobModel.getJobName());
    }

    /**
     *
     * @param jobModel
     * @return
     */
    @Override
    public String getJobBuildLog(JobModel jobModel) throws IOException, JobBuildLogException {
        return jobBuildApiHelper.getJobByNumberForLog(jobModel.getJobName(), Integer.valueOf(jobModel.getNumber()));
    }

    /**
     * 修改流水线
     * @param jobModel
     */
    @Override
    public void updateJob(JobModel jobModel) {
        UpdateWrapper<JobModel> qw = new UpdateWrapper<>();
        qw.eq("job_id", jobModel.getJobId());
        jobMapper.update(jobModel,qw);
    }

}
