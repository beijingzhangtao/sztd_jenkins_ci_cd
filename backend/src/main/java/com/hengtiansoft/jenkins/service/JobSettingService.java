package com.hengtiansoft.jenkins.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.model.JobSetting;

import java.util.List;

/**
 * @author zdd_x
 */
public interface JobSettingService {

    /**
     * 获取所有配置列表
     * @return
     * @param producePage
     */
    List<JobSetting> getList(Page<JobSetting> producePage);

    /**
     * 新增配置列表
     * @param jobSetting
     */
    void addSetting(JobSetting jobSetting);

    /**
     * 获取任务配置
     * @param id
     * @return
     */
    JobSetting getOne(String id);

    /**
     * 修改任务配置
     * @param jobSetting
     */
    void updateSetting(JobSetting jobSetting);

    /**
     * 删除任务配置
     * @param jobSetting
     */
    void deteleSetting(JobSetting jobSetting);

    /**
     * 返回所有配置
     * @return
     */
    List<JobSetting> getListAll();
}
