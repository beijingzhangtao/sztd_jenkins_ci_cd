package com.hengtiansoft.jenkins.apiController;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkinsApi.JobBuildApiHelper;
import com.hengtiansoft.jenkins.model.JobBuildModel;
import com.hengtiansoft.jenkins.model.JobModel;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * 任务执行类
 * @author zdd_x
 */
@RestController
@RequestMapping("/jenkins/jobBuild")
public class JobBuildController {


    @Autowired
    private JobBuildApiHelper jobBuildApiHelper;


    /**
     * 获取 Job 信息
     */
    //@RequestMapping(value = "/getJobLastBuild", method = RequestMethod.POST)
    public BaseRespResult getJobLastBuild(@RequestBody JobModel jobModel){
        Map<String, Build> resultMap = new HashMap<>();
        try {
            JobWithDetails jobLastBuild = jobBuildApiHelper.getJobLastBuild(jobModel.getJobName());
            // 获得首次编译信息
            resultMap.put("firstBuild",jobLastBuild.getFirstBuild());
            // 获得最后编译信息
            resultMap.put("lastBuild",jobLastBuild.getLastBuild());
            // 获取最后成功的编译信息
            resultMap.put("lastSuccessfulBuild",jobLastBuild.getLastSuccessfulBuild());
            // 获取最后事变的编译信息
            resultMap.put("lastFailedBuild",jobLastBuild.getLastFailedBuild());
            // 获取最后完成的编译信息
            resultMap.put("lastCompletedBuild",jobLastBuild.getLastCompletedBuild());
            // 获取最后稳定的编译信息
            resultMap.put("lastStableBuild",jobLastBuild.getLastStableBuild());
            // 获取最后不稳定的编译信息
            resultMap.put("lastUnstableBuild",jobLastBuild.getLastUnstableBuild());
            // 获取最后未成功的编译信息
            resultMap.put("lastUnsuccessfulBuild",jobLastBuild.getLastUnsuccessfulBuild());
            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 根据 Build 编号获取编译信息
     */
    @RequestMapping(value = "/getJobByNumber", method = RequestMethod.POST)
    public BaseRespResult getJobByNumber(@RequestBody JobModel jobModel, int buildNumber){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            Build build = jobBuildApiHelper.getJobByNumber(jobModel.getJobName(), buildNumber);
            resultMap.put("url",build.getUrl());
            resultMap.put("number",build.getNumber());
//            resultMap.put("testReport",build.getTestReport());
//            resultMap.put("testResult",build.getTestResult());

            //TODO  如果服务 宕机 或者为 null  查询数据库  先job表 再 job_build表
            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取全部 Job Build列表
     */
    @RequestMapping(value = "/getJobBuildListAll", method = RequestMethod.POST)
    public BaseRespResult getJobBuildListAll(@RequestBody JobModel jobModel){
        try {
            List<Build> jobBuildListAllList = jobBuildApiHelper.getJobBuildListAll(jobModel.getJobName());

            //TODO  如果服务 宕机 或者为 null  查询数据库  先job表 再 job_build表
            return BaseRespResult.successResult(jobBuildListAllList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**s
     * 获取 Job 一定范围的 Build 列表
     */
    @RequestMapping(value = "/getJobBuildListRange", method = RequestMethod.POST)
    public BaseRespResult getJobBuildListRange(@RequestBody JobModel jobModel){
//        try {
//            List<Build> jobBuildListAllList = jobBuildApiHelper.getJobBuildListRange(jobModel.getJobName(), jobModel.getFirstRange(), jobModel.getLastRange());
//
//            //TODO  如果服务 宕机 或者为 null  查询数据库  先job表 再 job_build表
//            return BaseRespResult.successResult(jobBuildListAllList);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取 Build 基本信息
     */
    @RequestMapping(value = "/getJobBuildInfo", method = RequestMethod.POST)
    public BaseRespResult getJobBuildInfo(@RequestBody JobModel jobModel){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            JobWithDetails jobBuildInfo = jobBuildApiHelper.getJobBuildInfo(jobModel.getJobName());
            Build lastBuild = jobBuildInfo.getLastBuild();
            resultMap.put("url",lastBuild.getUrl());
            resultMap.put("number",lastBuild.getNumber());
            //resultMap.put("testReport",lastBuild.getTestReport());
            //resultMap.put("testResult",lastBuild.getTestResult());

            //TODO  如果结果 或者为 null  查询数据库  先job表 再 job_build表
            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 获取 Build 详细信息
     * first            首次编译信息
     * last             最后编译信息
     * lastSuccess      最后成功的编译信息
     * lastFailed       最后事变的编译信息
     * lastCompleted    最后完成的编译信息
     * lastStable       最后稳定的编译信息
     * lastUnstable     最后不稳定的编译信息
     * lastUnsuccessful 最后未成功的编译信息
     */
    @RequestMapping(value = "/getJobBuildDetailInfo", method = RequestMethod.POST)
    public BaseRespResult getJobBuildDetailInfo(@RequestBody JobBuildModel jobBuildModel){
        Map<String, Object> resultMap = new HashMap<>();
        try {
            BuildWithDetails buildInfo = jobBuildApiHelper.getJobBuildDetailInfo(jobBuildModel.getJobName(), jobBuildModel.getState());

            //获取构建的显示名称
            resultMap.put("displayName",buildInfo.getDisplayName());
            // 获取构建的参数信息
            resultMap.put("parameters",buildInfo.getParameters());
            // 获取构建编号
            resultMap.put("number",buildInfo.getNumber());
            // 获取构建结果，如果构建未完成则会显示为null
            resultMap.put("result",buildInfo.getResult());
            // 获取执行构建的活动信息
            resultMap.put("actions",buildInfo.getActions());
            // 获取构建持续多少时间(ms)
            resultMap.put("duration",buildInfo.getDuration());
            // 获取构建开始时间戳
            resultMap.put("timestamp",buildInfo.getTimestamp());
            // 获取构建头信息，里面包含构建的用户，上游信息，时间戳等
            resultMap.put("causesList",buildInfo.getCauses());
            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取 Build Log 日志信息
     */
    @RequestMapping(value = "/getJobBuildLog", method = RequestMethod.POST)
    public BaseRespResult getJobBuildLog(@RequestBody JobBuildModel jobBuildModel){
        try {
            String jobBuildInfo = jobBuildApiHelper.getJobBuildLog(jobBuildModel.getJobName(), jobBuildModel.getLogFormat());
            return BaseRespResult.successResult(jobBuildInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取正在执行构建任务的日志信息
     */
    @RequestMapping(value = "/getBuildActiveLog", method = RequestMethod.POST)
    public BaseRespResult getBuildActiveLog(@RequestBody JobModel jobModel){
        try {
            String jobBuildInfo = jobBuildApiHelper.getBuildActiveLog(jobModel.getJobName());
            return BaseRespResult.successResult(jobBuildInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


}
