package com.hengtiansoft.jenkins.apiController;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.model.JobSetting;
import com.hengtiansoft.jenkins.service.JobSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 配置管理
 * @author zdd_x
 */
@RestController
@RequestMapping("/jenkins/jobsetting")
public class JobSettingController {


    @Autowired
    private JobSettingService jobSettingService;


    /**
     * 获取配置列表分页
     */
    @RequestMapping(value = "/getList", method = RequestMethod.POST)
    public BaseRespResult getList(@RequestBody JobSetting jobSetting){
        Page<JobSetting> producePage = new Page<>(jobSetting.getPage(),jobSetting.getRow());
        try {
            List<JobSetting> list = jobSettingService.getList(producePage);
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("data", list);
            resultMap.put("total", producePage.getTotal());
            return BaseRespResult.successResult(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 获取配置列表所有
     */
    @RequestMapping(value = "/getListAll", method = RequestMethod.POST)
    public BaseRespResult getListAll(){
        try {
            List<JobSetting> list = jobSettingService.getListAll();
            return BaseRespResult.successResult(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 获取任务配置
     */
    @RequestMapping(value = "/getOne", method = RequestMethod.POST)
    public BaseRespResult getOne(@RequestBody JobSetting jobSetting){
        try {
            JobSetting jobSettingDB = jobSettingService.getOne(jobSetting.getId());
            return BaseRespResult.successResult(jobSettingDB);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 新增配置
     */
    @RequestMapping(value = "/addSetting", method = RequestMethod.POST)
    public BaseRespResult addSetting(@RequestBody JobSetting jobSetting){

        try {
            jobSettingService.addSetting(jobSetting);
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 修改任务配置
     */
    @RequestMapping(value = "/updateSetting", method = RequestMethod.POST)
    public BaseRespResult updateSetting(@RequestBody JobSetting jobSetting){
        if(StringUtils.isEmpty(jobSetting.getId())){
            return BaseRespResult.errorResult("没有修改的任务配置");
        }
        try {
            jobSettingService.updateSetting(jobSetting);
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 删除任务配置
     */
    @RequestMapping(value = "/deteleSetting", method = RequestMethod.POST)
    public BaseRespResult deteleSetting(@RequestBody JobSetting jobSetting){
        if(StringUtils.isEmpty(jobSetting.getId())){
            return BaseRespResult.errorResult("没有修改的任务配置");
        }
        try {
            jobSettingService.deteleSetting(jobSetting);
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * jenkins 验证
     */
    @RequestMapping(value = "/jenkinsCheckImpl", method = RequestMethod.POST)
    public BaseRespResult jenkinsCheckImpl(@RequestBody JobSetting jobSetting){
        try {

            return BaseRespResult.successResult(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }




}
