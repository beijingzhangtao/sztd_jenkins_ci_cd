package com.hengtiansoft.jenkins.apiController;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.hengtiansoft.jenkins.jenkinsApi.AutoJenkinsPipeline_centos7;
import com.hengtiansoft.jenkins.jenkinsApi.JobApiHelper;
import com.hengtiansoft.jenkins.jenkinsApi.JobBuildApiHelper;
import com.hengtiansoft.jenkins.model.JobModel;
import com.hengtiansoft.jenkins.model.JobSetting;
import com.hengtiansoft.jenkins.model.JobTemplateModel;
import com.hengtiansoft.jenkins.model.JobViewModel;
import com.hengtiansoft.jenkins.service.JobService;
import com.hengtiansoft.jenkins.service.JobSettingService;
import com.hengtiansoft.jenkins.service.JobTemplateService;
import com.offbytwo.jenkins.model.BuildWithDetails;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.tmatesoft.svn.core.SVNException;

import java.io.IOException;
import java.util.*;

/**
 * 任务类
 * @author zdd_x
 */
@RestController
@RequestMapping("/jenkins/job")
public class JobController {

    @Autowired
    private JobApiHelper jobApiHelper;

    @Autowired
    private JobService jobService;

    @Autowired
    private JobBuildApiHelper jobBuildApiHelper;

    @Autowired
    private JobSettingService jobSettingService;

    @Autowired
    private JobTemplateService jobTemplateService;

    /**
     * 创建job 任务
     */
    @RequestMapping(value = "/ceateJob", method = RequestMethod.POST)
    public BaseRespResult ceateJob(@RequestBody JobModel jobModel){
        if(StringUtils.isEmpty(jobModel.getJobName())){
            return BaseRespResult.errorResult("Job 名称为空,请检查...");
        }
        try {
            //TODO 先做数据库判断，如果数据没有 走下面 <check 1>逻辑 连服务器逻辑，如果有不走，功能完成，删除注释

            //check 1 -------------------------------------------------
            //检查是否已经创建该 job
            boolean checkStatus = jobService.createdCheckJobName(jobModel.getJobName());
            if(!checkStatus){
                return BaseRespResult.errorResult(String.format("Job %s ,已创建,请勿重复创建...",jobModel.getJobName()));
            }
            //---------------------------------------------------------
            JobSetting jobSetting = jobSettingService.getOne(jobModel.getSettingId());

            String result = AutoJenkinsPipeline_centos7.extractedApi(jobSetting.getType(),
                    2,
                    2,
                    jobSetting.getSvnLocalhost(),
                    jobSetting.getFilePath(),
                    jobSetting.getProjectUrlWar(),
                    jobSetting.getProjectName(),
                    jobSetting.getSvnUrl(),
                    jobSetting.getSvnCredentialsId(),
                    jobSetting.getProjectUrlJar(),
                    jobSetting.getDockerPort(),
                    jobSetting.getProjectPort(),
                    jobSetting.getProjectVersion(),
                    jobSetting.getSshUrl(),
                    jobSetting.getSshPort(),
                    jobSetting.getSshName(),
                    jobSetting.getSshPwd(),
                    jobSetting.getDockerId(),
                    "");

            //创建job
//            jobApiHelper.ceateJob(jobModel.getJobName(), jobModel.getXml());

            //TODO  入库 job表
            jobService.insertJob(jobModel);

            return BaseRespResult.successResult(result);
        } catch (IOException | SVNException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 更新job 任务
     */
    @RequestMapping(value = "/updateJob", method = RequestMethod.POST)
    public BaseRespResult updateJob(@RequestBody JobModel jobModel){
        if(StringUtils.isEmpty(jobModel.getJobName())){
            return BaseRespResult.errorResult("Job 名称为空,请检查...");
        }

        try {
            jobApiHelper.updateJob(jobModel.getJobName(), "");
            jobService.updateJob(jobModel);
            //TODO  更新 job 表
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取 Job 基本信息
     */
    @RequestMapping(value = "/getJob", method = RequestMethod.POST)
    public BaseRespResult getJob(@RequestBody JobModel jobModel){
        Map<String, Object> result = new HashMap<>();
        try {
            JobModel jobInfo = jobService.selectJobByName(jobModel.getJobName());
            if(null != jobInfo){
                if(!StringUtils.isEmpty(jobInfo.getTemplateId())){
                    JobTemplateModel jobTemplateModel = jobTemplateService.selectOne(jobInfo.getTemplateId());
                    result.put("template", jobTemplateModel);
                }else{
                    JobTemplateModel jobTemplateModel = new JobTemplateModel();
                    result.put("template", jobTemplateModel);
                }
                result.put("name",jobInfo.getJobName());
                // 获取 Job 下一个 build 编号
//                result.put("nextBuildNumber",jobInfo.getBuildNumber());
                // 获取 Job 显示的名称
                result.put("displayName",jobInfo.getDisplayName());
                // 输出 Job 描述信息
                result.put("description",jobInfo.getDescription());

                result.put("settingId", jobInfo.getSettingId());

                return BaseRespResult.successResult(result);
            }
            JobWithDetails job = jobApiHelper.getJob(jobModel.getJobName());
            // 获取 Job 名称
            result.put("name",job.getName());
            // 获取 Job 下一个 build 编号
            result.put("nextBuildNumber",job.getNextBuildNumber());
            // 获取 Job 显示的名称
            result.put("displayName",job.getDisplayName());
            // 输出 Job 描述信息
            result.put("description",job.getDescription());
//            // 获取 Job 下游任务列表
//            result.put("downstreamProjects",job.getDownstreamProjects());
//            // 获取 Job 上游任务列表
//            result.put("upstreamProjects",job.getUpstreamProjects());

            //TODO  如果服务 宕机 或者为null  去查询数据库  job 表

            return BaseRespResult.successResult(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取所有 Job 基本信息
     */
    @RequestMapping(value = "/getJobList", method = RequestMethod.POST)
    public BaseRespResult getJobList(@RequestBody JobModel jobModel){
        int page = jobModel.getPage();
        int row = jobModel.getRow();
        try {
            Map<String, Job> jobList = jobApiHelper.getJobList();
//            Collection<Job> values = jobList.values();
//            List<JobModel> rewsult  = jobService.buildJobBuild(jobList,page,row);
            List<JobModel> resultList = new ArrayList<>();
            Collection<Job> jobCollection = jobList.values();

            if(page <= 1){
                page = 1;
            }
            page = page -1;

            int counp = 0;
            for (Job jobitem:jobCollection) {
                if(counp < (row * (page))){
                    counp ++ ;
                    continue;
                }
                jobModel = new JobModel();
                BuildWithDetails jobBuild = jobBuildApiHelper.getJobBuildDetailInfo(jobitem.getName(), "last");

                jobModel.setJobName(jobBuild.getFullDisplayName());
//                jobModel.setResult(jobBuild.getResult());
                List actions = jobBuild.getActions();
//                jobModel.setActions(actions);
                jobModel.setCreader(JenkinsConnect.JENKINS_USERNAME);
//                jobModel.setTimestamp(jobBuild.getTimestamp());
                //jobModel.setCreadTime(System.currentTimeMillis());

                resultList.add(jobModel);

                counp ++ ;
            }


            //List<JobModel> jobModels = subList(rewsult, jobModel.getPage(), jobModel.getRow());
//
//            //TODO  如果服务 宕机 或者为null  去查询数据库  job 表
            Map<String,Object> resultMap = new HashMap<>();


            resultMap.put("list",resultList);
            resultMap.put("total",jobList.size());
            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    private List<JobModel> subList(List<JobModel> list , int page, int rows) {
        List<JobModel> listSort =new ArrayList<>();
        int size=list.size();
        //截取的开始位置
        int pageStart=page<=1?0:(page-1)*rows;
        if(page <= 0){
            page = 1;
        }
        int pageEnd=page*rows;
        if(size>pageStart){
            listSort =list.subList(pageStart, pageEnd);
        }
        return listSort;
    }



    /**
     * 通过 View 名称获取 Job 列表
     */
    @RequestMapping(value = "/getJobListByView", method = RequestMethod.POST)
    public BaseRespResult getJobListByView(@RequestBody JobViewModel jobViewModel){
        try {
            Map<String, Job> jobList = jobApiHelper.getJobListByView(jobViewModel.getViewName());

            //TODO  如果服务 宕机 或者为null  去查询数据库  job_view 表
            Collection<Job> values = jobList.values();
            return BaseRespResult.successResult(values);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 执行无参数 Job build
     */
    @RequestMapping(value = "/buildJob", method = RequestMethod.POST)
    public BaseRespResult buildJob(@RequestBody JobModel jobModel){
        try {
            jobApiHelper.buildJob(jobModel.getJobName());

            //TODO  先查询 job build 信息，然后入库  job_buildinfo 表
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 执行带参数 Job build
     */
    @RequestMapping(value = "/buildParamJob", method = RequestMethod.POST)
    public BaseRespResult buildParamJob(@RequestBody JobModel jobModel){
//        try {
//            jobApiHelper.buildParamJob(jobModel.getJobName(), jobModel.getParam());
//
//            //TODO  先查询 job build 信息，然后入库  job_buildinfo 表
//            return BaseRespResult.successResult();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        return BaseRespResult.errorResult();
    }


    /**
     * 停止最后构建的 Job Build
     */
    @RequestMapping(value = "/stopLastJobBuild", method = RequestMethod.POST)
    public BaseRespResult stopLastJobBuild(@RequestBody JobModel jobModel){
        try {
            jobApiHelper.stopLastJobBuild(jobModel.getJobName());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 删除 Job
     */
    @RequestMapping(value = "/deleteJob", method = RequestMethod.POST)
    public BaseRespResult deleteJob(@RequestBody JobModel jobModel){
        try {
            jobApiHelper.deleteJob(jobModel.getJobName());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 禁用 Job
     */
    @RequestMapping(value = "/disableJob", method = RequestMethod.POST)
    public BaseRespResult disableJob(@RequestBody JobModel jobModel){
        try {
            jobApiHelper.disableJob(jobModel.getJobName());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 启用 Job
     */
    @RequestMapping(value = "/enableJob", method = RequestMethod.POST)
    public BaseRespResult enableJob(@RequestBody JobModel jobModel){
        try {
            jobApiHelper.enableJob(jobModel.getJobName());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }






}
