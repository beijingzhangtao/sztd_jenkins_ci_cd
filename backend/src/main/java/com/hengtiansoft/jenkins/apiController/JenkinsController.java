package com.hengtiansoft.jenkins.apiController;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkinsApi.JenkinsManager;
import com.hengtiansoft.jenkins.model.JenkinsModel;
import com.hengtiansoft.jenkins.model.PortIsUsing;
import com.hengtiansoft.jenkins.unit.RunShcommandUnit;
import com.offbytwo.jenkins.model.Computer;
import com.offbytwo.jenkins.model.LabelWithDetails;
import com.offbytwo.jenkins.model.Plugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * jenkins 操作类
 * @author zdd_x
 */
@RestController
@RequestMapping("/jenkins")
public class JenkinsController {



    @Autowired
    private JenkinsManager jenkinsManager;


    /**
     * 获取主机信息
     */
    @RequestMapping(value = "/getComputerInfo", method = RequestMethod.POST)
    public BaseRespResult getComputerInfo(){
        try {
            Map<String, Computer> computerInfo = jenkinsManager.getComputerInfo();
            return BaseRespResult.successResult(computerInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 重启 Jenkins
     */
    @RequestMapping(value = "/restart", method = RequestMethod.POST)
    public BaseRespResult restart(){
        try {
            jenkinsManager.restart();
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 安全重启 Jenkins
     */
    @RequestMapping(value = "/safeRestart", method = RequestMethod.POST)
    public BaseRespResult safeRestart(){
        try {
            jenkinsManager.safeRestart();
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 安全结束 Jenkins
     */
    @RequestMapping(value = "/safeExit", method = RequestMethod.POST)
    public BaseRespResult safeExit(){
        try {
            jenkinsManager.safeExit();
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 安全结束 Jenkins
     */
    @RequestMapping(value = "/close", method = RequestMethod.POST)
    public BaseRespResult close(){
        try {
            jenkinsManager.close();
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 根据 Label 查找代理节点信息
     */
    @RequestMapping(value = "/getLabelNodeInfo", method = RequestMethod.POST)
    public BaseRespResult getLabelNodeInfo(@RequestBody JenkinsModel jenkinsModel){
        try {
            Map<String, Object> resultMap = new HashMap<>();

            LabelWithDetails labelNodeInfo = jenkinsManager.getLabelNodeInfo(jenkinsModel.getLabelName());
            // 获取标签名称
            resultMap.put("name", labelNodeInfo.getName());
            // 获取 Cloud 信息
            resultMap.put("clouds", labelNodeInfo.getClouds());
            // 获取节点信息
            resultMap.put("nodeName", labelNodeInfo.getNodeName());
            // 获取关联的 Job
            resultMap.put("tiedJobs", labelNodeInfo.getTiedJobs());
            // 获取参数列表
            resultMap.put("propertiesList", labelNodeInfo.getPropertiesList());
            // 是否脱机
            resultMap.put("offline", labelNodeInfo.getOffline());
            // 获取描述信息
            resultMap.put("description", labelNodeInfo.getDescription());


            return BaseRespResult.successResult(resultMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 判断 Jenkins 是否运行
     */
    @RequestMapping(value = "/isRunning", method = RequestMethod.POST)
    public BaseRespResult isRunning(){
        try {
            boolean running = jenkinsManager.isRunning();
            return BaseRespResult.successResult(running);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 获取 Jenkins 插件信息
     */
    @RequestMapping(value = "/getPluginInfo", method = RequestMethod.POST)
    public BaseRespResult getPluginInfo(){
        List<Map<String, Object>> resultList = new ArrayList<>();
        try {
            Map<String, Object> itemMap = null;

            List<Plugin> pluginInfo = jenkinsManager.getPluginInfo();
            for (Plugin plugin:pluginInfo) {
                itemMap = new HashMap<>();
                // 插件 wiki URL 地址
                itemMap.put("url",plugin.getUrl());
                // 版本号
                itemMap.put("version",plugin.getVersion());
                // 简称
                itemMap.put("shortName",plugin.getShortName());
                // 完整名称
                itemMap.put("longName",plugin.getLongName());
                // 是否支持动态加载
                itemMap.put("supportsDynamicLoad",plugin.getSupportsDynamicLoad());
                // 插件依赖的组件
                itemMap.put("dependencies",plugin.getDependencies());
                resultList.add(itemMap);
            }

            return BaseRespResult.successResult(resultList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 判断 Jenkins 是否运行
     */
    @RequestMapping(value = "/isPortUsing", method = RequestMethod.POST)
    public BaseRespResult isPortUsing(@RequestBody PortIsUsing portIsUsing){
        try {
            if("docker".equals(portIsUsing.getType())){
                boolean portUsing = RunShcommandUnit.isPortUsing("127.0.0.1", portIsUsing.getPort());
                return BaseRespResult.successResult(portUsing);
            }else{
                Boolean portUsing = RunShcommandUnit.runByshcommand("netstat -anp |grep " + portIsUsing.getPort());
                return BaseRespResult.successResult(portUsing);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


}
