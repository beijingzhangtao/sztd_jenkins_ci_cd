package com.hengtiansoft.jenkins.apiController;

import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkinsApi.ViewApiHelper;
import com.hengtiansoft.jenkins.model.JobViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


/**
 * 视图操作类
 * @author zdd_x
 */
@RestController
@RequestMapping("/jenkins/jobView")
public class JobViewController {

    @Autowired
    private ViewApiHelper viewApiHelper;


    /**
     * 创建视图
     */
    @RequestMapping(value = "/createView", method = RequestMethod.POST)
    public BaseRespResult createView(@RequestBody JobViewModel jobViewModel){
        try {
            //TODO 先做数据库判断，如果数据没有 走下面 <check 1>逻辑，参考job那边逻辑封装 连服务器逻辑，如果有不走，功能完成，删除注释

            viewApiHelper.createView(jobViewModel.getViewName(), jobViewModel.getXml());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取视图基本信息
     */
    @RequestMapping(value = "/getView", method = RequestMethod.POST)
    public BaseRespResult getView(@RequestBody JobViewModel jobViewModel){
        try {
            String viewInfo = viewApiHelper.getView(jobViewModel.getViewName());

            //TODO  如果服务 宕机 或者为 null  查询数据库  job_view表
            return BaseRespResult.successResult(viewInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取视图基本信息xml
     */
    @RequestMapping(value = "/getViewXml", method = RequestMethod.POST)
    public BaseRespResult getViewXml(@RequestBody JobViewModel jobViewModel){
        try {
            String viewXmlInfo = viewApiHelper.getViewXml(jobViewModel.getViewName());

            //TODO  如果服务 宕机 或者为 null  查询数据库  job_view表
            return BaseRespResult.successResult(viewXmlInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 获取视图基本信息xml
     */
    @RequestMapping(value = "/getViewConfig", method = RequestMethod.POST)
    public BaseRespResult getViewConfig(@RequestBody JobViewModel jobViewModel){
        try {
            String viewConfigXml = viewApiHelper.getViewConfig(jobViewModel.getViewName());

            //TODO  如果服务 宕机 或者为 null  查询数据库  job_view表
            return BaseRespResult.successResult(viewConfigXml);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 更新视图信息
     */
    @RequestMapping(value = "/updateView", method = RequestMethod.POST)
    public BaseRespResult updateView(@RequestBody JobViewModel jobViewModel){
        try {
            viewApiHelper.updateView(jobViewModel.getViewName(), jobViewModel.getXml());

            //TODO  更新数据库  job_view 表
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 删除视图信息
     */
    @RequestMapping(value = "/deleteView", method = RequestMethod.POST)
    public BaseRespResult deleteView(@RequestBody JobViewModel jobViewModel){
        try {
            viewApiHelper.deleteView(jobViewModel.getViewName());
            return BaseRespResult.successResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


}
