package com.hengtiansoft.jenkins.authentication;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.hengtiansoft.jenkins.ResultVo.BaseRespResult;
import com.hengtiansoft.jenkins.jenkins.JenkinsConnect;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.client.JenkinsHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 认证凭证
 * @author zdd_x
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    /**
     * Jenkins 对象
     */
    private JenkinsServer jenkinsServer;

    /**
     * http 客户端对象
     */
    private JenkinsHttpClient jenkinsHttpClient;


    public static  String url = JenkinsConnect.JENKINS_URL;
    public static  String username = JenkinsConnect.JENKINS_USERNAME;
    public static  String password = JenkinsConnect.JENKINS_PASSWORD;

    /**
     * 构造方法中调用连接 Jenkins 方法
     */
    public AuthenticationController() {
        // 连接 Jenkins
        jenkinsServer = JenkinsConnect.connection();
        // 设置客户端连接 Jenkins
        jenkinsHttpClient = JenkinsConnect.getClient();
    }

    /**
     * 获取凭证信息
     */
    @RequestMapping(value = "/queryCredentialsList", method = RequestMethod.POST)
    public BaseRespResult getList() {
        try {
            List<JenkinsCredentialsInfoDTO> jenkinsCredentialsInfoDTOS = queryCredentialsList();
            return BaseRespResult.successResult(jenkinsCredentialsInfoDTOS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }

    /**
     * 创建凭证信息
     */
    @RequestMapping(value = "/createCredentials", method = RequestMethod.POST)
    public BaseRespResult create(@RequestBody JenkinsCredentialsInfoDTO jenkinsCredentialsInfoDTO) {
        try {
            String uuid = UUID.randomUUID().toString();
            createCredentials(uuid, jenkinsCredentialsInfoDTO.getUsername(), jenkinsCredentialsInfoDTO.getPassword());
            Map<String, String> resultMap = new HashMap<>(1);
            resultMap.put("credentialsId",uuid);
            return BaseRespResult.successResult(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 创建凭证信息
     */
    @RequestMapping(value = "/deleteCredentials", method = RequestMethod.POST)
    public BaseRespResult delete(@RequestBody JenkinsCredentialsInfoDTO jenkinsCredentialsInfoDTO) {
        try {
            deleteCredentials(jenkinsCredentialsInfoDTO.getId());
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 修改凭证信息
     */
    @RequestMapping(value = "/updateCredentials", method = RequestMethod.POST)
    public BaseRespResult update(@RequestBody JenkinsCredentialsInfoDTO jenkinsCredentialsInfoDTO) {
        try {
            updateCredentials(jenkinsCredentialsInfoDTO.getNewId(), jenkinsCredentialsInfoDTO.getId());
            return BaseRespResult.successResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BaseRespResult.errorResult();
    }


    /**
     * 创建凭证
     * @param credentialsId
     * @throws Exception
     */
    public void createCredentials(String credentialsId, String username, String password) throws Exception {

        List<NameValuePair> data = new ArrayList<>();
        data.add(createCredentialsParam(credentialsId, username, password));
        HttpResponse httpResponse = jenkinsHttpClient.post_form_with_result("credentials/store/system/domain/_/createCredentials", data, false);
        if (httpResponse.getStatusLine().getStatusCode() == 302) {
            System.out.println("创建凭证成功，返回状态["+ httpResponse.getStatusLine().getStatusCode() +"]");
        } else {
            throw new Exception("");
        }
    }

    /**
     * 创建凭证参数
     * @param credentialsId
     * @param userName
     * @param password
     * @return
     */
    private BasicNameValuePair createCredentialsParam(String credentialsId, String userName, String password) {
        if (StringUtils.isEmpty(credentialsId)) {
            return new BasicNameValuePair("json", "{" + "\"\": \"0\"," + "\"credentials\": " +
                    "{" + "\"scope\": \"GLOBAL\"," +
                    "\"id\": \"\"," +
                    "\"username\": \"" + userName + "\"," +
                    "\"password\": \"" + password + "\"," +
                    "\"description\": \"\"," +
                    "\"$class\": \"com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\"" +
                    "}" +
                    "}");
        } else {
            return new BasicNameValuePair("json", "{" + "\"\": \"0\"," + "\"credentials\": " +
                    "{" + "\"scope\": \"GLOBAL\"," +
                    "\"id\": \"" + credentialsId + "\"," +
                    "\"username\": \"" + userName + "\"," +
                    "\"password\": \"" + password + "\"," +
                    "\"description\": \"\"," +
                    "\"$class\": \"com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\"" +
                    "}" +
                    "}");
        }
    }


    /**
     * 查询凭证方法
     * @return
     * @throws Exception
     */
    public List<JenkinsCredentialsInfoDTO> queryCredentialsList() throws Exception {
        // 获取凭证列表 curl -X GET http://www.xxx.xxx/credentials/store/system/domain/_/api/json?depth=1 --user username:password
        String credentials = null;
        if (org.apache.commons.lang3.StringUtils.endsWith(url, "/")) {
            credentials = jenkinsHttpClient.get(url + "credentials/store/system/domain/_/api/json?depth=1");
        } else {
            credentials = jenkinsHttpClient.get(url + "/credentials/store/system/domain/_/api/json?depth=1");
        }
        if (!StringUtils.isEmpty(credentials)) {
            ObjectMapper objectMapper = new ObjectMapper();
            CredentialsStoreActionDTO credentialsStoreActionDTO = objectMapper.readValue(credentials, CredentialsStoreActionDTO.class);
            return CollectionUtils.isEmpty(credentialsStoreActionDTO.getCredentials()) == true ? new ArrayList<>() : credentialsStoreActionDTO.getCredentials();
        }
        return new ArrayList();
    }

    /**
     * 删除凭证
     * @param credentialsId
     * @throws Exception
     */
    public void deleteCredentials(String credentialsId) throws Exception {
        String cUrl = url + "credentials/store/system/domain/_/credential/" + credentialsId + "/doDelete";
        jenkinsHttpClient.post(cUrl);
    }

    /**
     * 更新凭证
     * @param newId
     * @param oldId
     * @throws Exception
     */
    public void updateCredentials(String newId, String oldId) throws
            Exception {
        String jeniknsUrl = url + "credentials/store/system/domain/_/credential/" + oldId + "/updateSubmit";
        String json = null;
        if (StringUtils.isEmpty(newId)) {
            json = "{\"stapler-class\": \"com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\", " +
                    "\"scope\": \"GLOBAL\", \"username\":\"" + username + "\", \"password\":\"" + password + "\"," +
                    "\"id\":\"\", \"description\": \"\"}";
        } else {
            json = "{\"stapler-class\": \"com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl\", " +
                    "\"scope\": \"GLOBAL\", \"username\":\"" + username + "\", \"password\":\"" + password + "\"," +
                    "\"id\":\"" + newId + "\", \"description\": \"\"}";
        }
        List<NameValuePair> data = new ArrayList<>();
        data.add(new BasicNameValuePair("json", json));
        HttpResponse httpResponse = jenkinsHttpClient.post_form_with_result(jeniknsUrl, data, false);
        if (httpResponse.getStatusLine().getStatusCode() == 302) {
            System.out.println("更新凭证成功，返回状态[{"+ httpResponse.getStatusLine().getStatusCode() +"}]");
        }
    }

}
