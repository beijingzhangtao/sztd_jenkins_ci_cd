package com.hengtiansoft.jenkins.authentication;

import java.util.List;

/**
 * @author zdd_x
 */
public class CredentialsStoreActionDTO {

    private String _class;

    private List<JenkinsCredentialsInfoDTO> credentials;

    private String description;

    private String displayName;

    private String fullDisplayName;

    private String fullName;

    private Boolean global;

    private String urlName;

    public String get_class() {
        return _class;
    }

    public void set_class(String _class) {
        this._class = _class;
    }

    public List<JenkinsCredentialsInfoDTO> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<JenkinsCredentialsInfoDTO> credentials) {
        this.credentials = credentials;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getFullDisplayName() {
        return fullDisplayName;
    }

    public void setFullDisplayName(String fullDisplayName) {
        this.fullDisplayName = fullDisplayName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getGlobal() {
        return global;
    }

    public void setGlobal(Boolean global) {
        this.global = global;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }
}
